reset
set view map
set size ratio 2
#set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0.2 "white", 0.45 "orange", 1 "red", 1.1 "black")
load "< awk '/t/ {print $0}' br.txt"


load "< awk '/hmax/ {print $0}' plandisk.dat"



set cblabel 'log({/Symbol r}) (g.cm^{-3})' rotate by 0 offset screen -0.05, screen -0.23 # 0.25
set xlabel 'R (a.u.)'
set ylabel 'h (a.u.)'

range=hmax

set yrange [-range:range]
set xrange [0:range]
set palette maxcolors 100
set label front "time = %4.3f",t," Kyears" at screen 0.58,0.11 # 0.10


#set term png enh size 1800,1500 19
set term post enh color 17 portrait
set output 'plandisk.ps'

set multiplot
set origin 0,0
set size 1,1
set logscale cb
#set cbrange [1.e-15:1.e-8]
set format cb "{%L}"
set colorbox vert user origin 0.76,0.39 size 0.05,0.4
splot 'plandisk.dat' index 1 u 2:1:(($4/$3)) w image not

#set cbrange [*:*]
set origin 0,0
set size 1,1
set contour
set cntrparam levels incremental -20,0.5,-6
unset surface
unset clabel
splot 'plandisk.dat' index 1 u 2:1:(log10(abs($4/$3))) w l lt 1 lc 7 lw 2 not

set surface
unset contour

set cbtics out
set cbtics offset 0,screen 0.08 # 0.1
set cblabel "velocity in the rOz plane (km.s^{-1})" offset screen 0.,screen 0.05 # 0.07
set xlabel ''
set ylabel ''
set format x ""
set format y ""
set format cb "%1.1f"
set origin 0,0
set size 1,1
set palette model RGB defined (0 "violet", 0.2 "blue", 0.6 "black")
set palette model RGB defined (0.1 "white", 0.2 "blue", 0.6 "green")
set colorbox horiz user origin screen 0.26,0.9 size 0.5,0.02 
unset logscale cb
#set cbrange [1.e5:1.e8]
#set cbrange [0:7]
norm=1.0 # range/60
norm=range/15
splot 'plandisk.dat' every 10:10 index 1 u 2:1:($7/$3/1000.):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not
#
#set cbrange [*:*]
#
#set cbtics in
#set cbtics nooffset 
#set cblabel 'log(-J_z)' rotate by 0 offset screen -0.08,screen 0.07
#set origin 0.521,0.521
#set size 0.4,0.4
#set format x ""
#set format y ""
#set cbrange [1.e3:*]
#set palette model RGB defined (0.2 "white", 0.45 "blue", 1 "black")
#set colorbox vert user origin 0.81,0.66 size 0.015,0.15 
#
#splot 'djdt.dat' index 1 u 2:1:(-$4/$3) w image not

unset multiplot

