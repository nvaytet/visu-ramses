#!/bin/bash
i=0
mkdir movies
rm tr.txt
mkdir movies/tr
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
for file in $dir/output*00039
#file=$dir/output_00018
do
#   echo $file
#done
    z="$(echo $file | rev |cut -d'_' -f1 | rev)"
    echo $z
    let i=$i+1
    y="$(printf "%03d" $i)"
    echo "0.5 0.5 0.5" > center.dat
    echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
    if [ $z -ne 00001 ]
    then
      ./findcenter
    fi
    ./tr
    echo "load 'plothistotrr.gp'" | gnuplot
    mv tr.ps movies/tr/'tr_'$z'.ps'
#    mv rhor.png movies/rhor/'rhor_'$y'.png'
    #mv AMBI_brho.png 'AMBI_brho_'$n'.png'
done
rm rhor.txt
rm brho.dat





