#!/bin/bash
i=0

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

if [ -d movies/moutflow2 ];then
   cd movies/moutflow2
   date=$(date '+%s')
   mv toutflow.txt "toutflow_"$date".txt"
   mv moutflow.ps "moutflow_"$date".ps"
   cd ../..
fi
 echo $VISUDIR


dir=.
 #dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#cd $dir

#rm toutflow.txt
rm angmom_*
rm angmom.res
mkdir movies
mkdir movies/moutflow2
dirmovie="movies/moutflow2"
mkdir $dirmovie

pwd
for file in $dir/output_0*0
#   file=$dir/output_00970
do 
    z="$(echo $file | rev |cut -d'_' -f1 | rev)"
    y="$(printf "%05d" $i)"
    filee="$(echo $file | rev |cut -d'/' -f1 | rev)"
    if [ -f $dirmovie'/toutflow_'$z'.txt' ]
    then
	echo file $file already processed
    else
	echo processing file $file ....
	if [ $z -gt $i ]
	then
            echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 20\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat

	    ${VISUDIR}/findcenter_sink
	     ${VISUDIR}/angmom
	     ${VISUDIR}/massoutflow
            tail -n 1 'toutflow.txt' > $dirmovie/'toutflow_'$z'.txt'

#      echo "load 'plotmoutflow.gp'" | gnuplot
#      stop
      #mv mur.ps movies/mur/'mur_'$z'.ps'
      #    mv rhor.png movies/rhor/'rhor_'$y'.png'
      #mv AMBI_brho.png 'AMBI_brho_'$n'.png'
      let i=$i+1
	fi
    fi
done

cat "toutflow.txt" >> $dirmovie/"toutflow_.txt" 

mv $dirmovie/'toutflow_.txt' $dirmovie/"toutflow.txt"



rm angmom_*
rm angmom.res
#rm angmom.dat

cd movies/moutflow2/
#rm temp
#for mutruc in mu_*.res
#do
#cat $mutruc >> temp
#done
#cat mu.res >> temp
#mv temp mu.res

echo "load '/Users/benoit/visu-ramses/plotmoutflow.gp'" | gnuplot
