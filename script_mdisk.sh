#!/bin/bash
i=0
VISUDIR=$(dirname "${BASH_SOURCE[0]}");
dir=.

if [ -d movies/mdisk ];then
   cd movies/mdisk
   date=$(date '+%s')
   cp tdisk.txt "tdisk_.txt"
   mv tdisk.txt "tdisk_"$date".txt"
   mv mdisk.ps "mdisk_"$date".ps"
   cd ../../
fi

rm tdisk.txt
rm angmom_*
rm angmom.res
dirmovie="movies/mdisk"
mkdir $dirmovie

for file in $dir/output*
   #file=$dir/output_00080
do
#   echo $file
#done
   z="$(echo $file | rev |cut -d'_' -f1 | rev)"
   y="$(printf "%05d" $i)"
   filee="$(echo $file | rev |cut -d'/' -f1 | rev)"
   if [ -f $dirmovie'/tdisk_'$z'.txt' ]
   then
      echo file $file already processed
   else
      echo processing file $file ....
      if [ $z -gt $i ]
      then
         echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
         ${VISUDIR}/findcenter
         ${VISUDIR}/angmom
         ${VISUDIR}/massdisk
         #      echo "load 'plotmdisk.gp'" | gnuplot
         #      stop
         #mv mur.ps movies/mur/'mur_'$z'.ps'
         #    mv rhor.png movies/rhor/'rhor_'$y'.png'
         #mv AMBI_brho.png 'AMBI_brho_'$n'.png'
         tail -n 1 'tdisk.txt' > $dirmovie/'tdisk_'$z'.txt'
         let i=$i+1
      fi
   fi
done

cat "tdisk.txt" >> $dirmovie/"tdisk_.txt" 

mv $dirmovie/'tdisk_.txt' $dirmovie/"tdisk.txt"



rm angmom_*
rm angmom.res
#rm angmom.dat

cd movies/mdisk/

gnuplot  ${VISUDIR}/plotmdisk.gp;
#echo "load '/gpfs/data1/jmasson/analyse_folder/plotmdisk.gp'" | gnuplot



