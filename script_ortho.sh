#!/bin/bash

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

function processdata {

    file=$1;
    z="$(echo $file | rev |cut -d'_' -f1 | rev)";
    echo "0.5 0.5 0.5" > center.dat;
    echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 3.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat;
    if [ $z -ne 00001 ] ; then
        ${VISUDIR}/findcenter;
    fi
    if $ang ; then
        if [ "$adir" == "x" ] ; then
            echo "1 0 0 1 0 0 1 0 0 1 0 0" > angmom.dat
        elif [ "$adir" == "y" ] ; then
            echo "0 1 0 0 1 0 0 1 0 0 1 0" > angmom.dat
        elif [ "$adir" == "z" ] ; then
            echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat
        else
            echo "$adir" > angmom.dat;
        fi
    else
        ${VISUDIR}/angmom;
    fi
    echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre ' $fenet ' \ncenter 0.5 0.5 0.5\nnorm ' $norm ' \nold 1' > br.dat;
    ${VISUDIR}/ortho;
    mv "plandisk_${z}.txt" plandisk.dat;
    gnuplot ${VISUDIR}/plotortho.gp;
    mv ortho.ps movies/ortho/'ortho_'$z'.ps';
    rm -rf plandisk.dat angmom.dat 'angmom_'$z'.txt' angmom.res;
    if $2 ; then
       gs movies/ortho/'ortho_'$z'.ps';
    fi

};

###################################################################

last=false;
output_selected=false;
pattern_specified=false;
fenet=1.0;
norm=200;
ang=false;
view_file=false;
while getopts "a:ef:n:o:p:x" OPTION; do
   case $OPTION in
      a)
         ang=true;
         adir=$OPTARG;
      ;;
      e)
         last=true;
      ;;
      f)
         fenet=$OPTARG;
      ;;
      n)
         norm=$OPTARG;
      ;;
      o)
         output_selected=true;
         nout=$OPTARG;
      ;;
      p)
         pattern_specified=true;
         pattern=$OPTARG;
      ;;
      x)
         view_file=true;
      ;;
   esac
done


mkdir -p movies;
mkdir -p movies/ortho;
dir=.

if $last ; then

    for file in $dir/output*; do
        echo $file;
    done
    processdata $file $view_file;

elif $output_selected ; then

    if [ $nout -lt 10 ]; then
        file="${dir}/output_0000${nout}";
    elif [ $nout -lt 100 ]; then
        file="${dir}/output_000${nout}";
    elif [ $nout -lt 1000 ]; then
        file="${dir}/output_00${nout}";
    elif [ $nout -lt 10000 ]; then
        file="${dir}/output_0${nout}";
    else
        file="${dir}/output_${nout}";
    fi
    processdata $file $view_file;

elif $pattern_specified ; then

    for file in $dir/output_$pattern ; do
        echo "file is" $file;
        processdata $file $view_file;
    done

else

    for file in $dir/output_* ; do
        echo $file;
        processdata $file $view_file;
    done

fi

