set pm3d
#set term wxt
unset label
set view map
set border 4095
set logscale cb
set palette model RGB defined ( 0 '#3366ff', 1 '#99ffcc', 2 '#339900', 3 '#66ff33', 4 '#996633', 5 '#ff9900', 6 '#ffff33' )
f(x) = x < 5./6 ? 2./3 - 4./5*x : 1 - (x-5./6)
f(x) = x <= 5./6 ? 0.6 - x*(0.6)/(5./6) :  1-(x-5./6)*(0.6)/(5./6) 
#f(x) = x <= 1./6 ? 0.6 +  (x-5./6)*(0.6)/(5./6) : x*(0.6)/(5./6)
#f(x) = 0.85-0.85*x
g(x) = x < 2./10 ? 0.75-x*(0.75-( 5./9 - 6./9*2./10))/(2./10.): x < 5./6 ? 5./9 - 6./9*x : 1 - (x-5./6)
set palette model HSV functions f(gray),1,1
set palette maxcolors 100

set format cb "10^{%L}"
#set format xy "%2.0t{/Symbol \327}10^{%L}"

#set palette model RGB defined ( 0 '#3366ff', 1 '#99ffcc', 2 '#339900', 3 '#66ff33', 4 '#996633', 5 '#ff9900', 6 '#ffff33' )
set autoscale fix
#set cbrange [1.0e-17:1e-10]
set size square
set xlabel 'u.a.' rotate by 0
load "< awk '/t/ {print $0}' cube.dat"
set label  "t= %4.3f",t," Kyears" at screen 0.45, screen 0.88
set label "5 km/s" at screen 0.28, screen 0.88 tc rgbcolor "red"
set arrow 1 from screen 0.285, screen 0.86 to screen 0.33, screen 0.86
set cblabel "Density in g/cc" rotate by -90
#splot 'cube.dat' index 1 u 2:3:4 w pm3d not, 'cubev.dat' index 1 every 200 u 2:3:1:5:6:4 w vectors lt -1 not, 'cubev.dat' index 0  u 2:3:1:5:6:4 w vectors lt 1 lw 1.5 not
#splot 'cube.dat' u 2:3:4 w pm3d not, 'cubev.dat' every 1000 u 2:3:1:5:6:4 w vectors lt -1 not
#set term postscript enhanced color
#set output 'AMBI_face_61.ps'
#rep
#set output
#set term wxt
#rep
#set term png enhanced size 1000,833 nocrop truecolor font 'Helvetica' 30
set term post enh color 17
#set output 'rhov.png'
set output 'rhov.ps'
splot 'cube.dat' index 1 u 3:2:4 w image not, 'cubev.dat' index 0 u 3:2:1:6:5:4 w vectors lt -1 not


