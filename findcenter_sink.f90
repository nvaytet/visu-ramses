program find_center_sink
implicit none
integer::i,nb_sink,ipos,isink
real(8)::vx,vy,vz,rotx,roty,rotz,lsink,acc_Rate,acc_lum,boxlen
real(8),allocatable,dimension(:)::msink,age,int_lum,Teff
real(8),allocatable,dimension(:,:):: center
character(LEN=1)::skip
character(LEN=5)::nchar
character(LEN=80)::filename
character(LEN=128)::nomfich,repository,outfich,filetype='bin'


call read_params2

ipos=INDEX(repository,'output_')
nchar=repository(ipos+7:ipos+13)


nomfich=TRIM(repository)//'/info_'//TRIM(nchar)//'.txt'
open(unit=10,file=nomfich,form='formatted',status='old')
read(10,*)
read(10,*)
read(10,*)
read(10,*)
read(10,*)
read(10,*)
read(10,*)

read(10,'(13x,E23.15)')boxlen

close(10)

nomfich=TRIM(repository)//'/sink_'//TRIM(nchar)//'.out'
open(unit=11,file=nomfich,form='formatted',status='old')
read(11,'(17x,I13)')nb_sink
close(11)
print*,'Number of sinks : ',nb_sink
allocate(center(1:nb_sink,1:3))
allocate(msink(1:nb_sink),age(1:nb_sink),int_lum(1:nb_sink),Teff(1:nb_sink))
nomfich=TRIM(repository)//'/sink_'//TRIM(nchar)//'.csv'
open(unit=12,file=nomfich,form='formatted',status='old')

do i=1,nb_sink
!!$   read(11,'(I5,2X,F9.5,3(2X,F10.7),3(2X,F7.4),2X,E13.3,3(2X,F6.3),5(2X,E11.3))')&
!!$        isink,msink(i),center(i,1),center(i,2),center(i,3), &
!!$        vx,vy,vz,rot,lsink,acc_Rate, &
!!$        acc_lum,age(i),int_lum(i),Teff(i)
   
   read(12,'(I10,16(A1,ES20.10))')&
        isink,skip,msink(i),skip,center(i,1),skip,center(i,2),skip,center(i,3),skip, &
        vx,skip,vy,skip,vz,skip,rotx,skip,roty,skip,rotz,skip,lsink,skip,acc_Rate,skip, &
        acc_lum,skip,age(i),skip,int_lum(i),skip,Teff(i)
   print*,  isink,msink(i),center(i,1),center(i,2),center(i,3), &
        vx,vy,vz,rotx,roty,rotz,lsink,acc_Rate, &
        acc_lum,age(i),int_lum(i),Teff(i) !msink(i),center(i,1)/boxlen,center(i,2)/boxlen,center(i,3)/boxlen
end do
   close(12)
   
open(unit=287,file='center.dat')
write(287,*)center(1,1)/boxlen, center(1,2)/boxlen, center(1,3)/boxlen
close(287)


deallocate(center)


contains

     subroutine read_params2

     implicit none

     character(len=4)   :: arg

     open(286,file='brho.dat')

     read(286,*)arg, repository
     read(286,*)arg, outfich 

     close(286)



     return

   end subroutine read_params2


end program find_center_sink
