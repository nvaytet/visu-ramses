#!/bin/bash

argstring1="";
argstring2="";

while getopts "el:f:o:p:" OPTION; do
   case $OPTION in
      e)
         argstring1="${argstring1} -e";
      ;;
      f)
         fenet=$OPTARG;
         argstring2="${argstring2} -f ${fenet}";
      ;;
      l)
         lmax=$OPTARG;
         argstring2="${argstring2} -l ${lmax}";
      ;;
      o)
         nout=$OPTARG;
         argstring1="${argstring1} -o ${nout}";
      ;;
      p)
         pattern=$OPTARG;
         argstring1="${argstring1} -p ${pattern}";
      ;;
   esac
done

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

${VISUDIR}/script_trho.sh ${argstring1};
${VISUDIR}/script_brho.sh ${argstring1};
${VISUDIR}/script_rhor.sh ${argstring1};
${VISUDIR}/script_coldens.sh ${argstring1} ${argstring2};
${VISUDIR}/script_rho_xy.sh ${argstring1} ${argstring2};
${VISUDIR}/script_rho_xz.sh ${argstring1} ${argstring2};
${VISUDIR}/script_diskbr.sh ${argstring1};
${VISUDIR}/script_djdt.sh ${argstring1};
${VISUDIR}/script_plandisk.sh ${argstring1};
${VISUDIR}/script_plandiskB.sh ${argstring1};
${VISUDIR}/script_tempdisk.sh ${argstring1};
