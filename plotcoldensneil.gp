#set pm3d
#set term wxt
unset label
set view map
set logscale cb
set palette model RGB defined ( 0 '#3366ff', 1 '#99ffcc', 2 '#339900', 3 '#66ff33', 4 '#996633', 5 '#ff9900', 6 '#ffff33' )
f(x) = x < 5./6 ? 2./3 - 4./5*x : 1 - (x-5./6)
f(x) = x <= 5./6 ? 0.6 - x*(0.6)/(5./6) :  1-(x-5./6)*(0.6)/(5./6) 
#f(x) = 0.85-0.85*x
g(x) = x < 2./10 ? 0.75-x*(0.75-( 5./9 - 6./9*2./10))/(2./10.): x < 5./6 ? 5./9 - 6./9*x : 1 - (x-5./6)
set palette model HSV functions f(gray),1,1
set palette maxcolors 100
set format cb "10^{%L}"
set autoscale fix
#set cbrange [*:1e-10]
set size 1,1
set xlabel 'u.a.' rotate by 0
load "< awk '/t/ {print $0}' cube.dat_1"
set label  "t= %4.3f",t," Kyears" at screen 0.65, screen 0.015
set term post enh color 17 portrait
set output 'rhov.ps'

set cblabel "Column density in g/cm^2" rotate by -90 offset screen 0.02,0

splot 'cube.dat_1' index 1 u 2:3:4 w image not
unset colorbox
unset cblabel
