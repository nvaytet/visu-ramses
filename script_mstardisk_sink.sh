#!/bin/bash
i=0
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dir=.
VISUDIR=$(dirname "${BASH_SOURCE[0]}");

if [ -d movies/mstardisk ];then
   cd movies/mstardisk
   date=$(date '+%s')
   cp tstardisk.txt "tstardisk_.txt"
   mv tstardisk.txt "tstardisk_"$date".txt"
   mv mstardisk.ps "mstardisk_"$date".ps"
   cp tdisk.txt "tdisk_.txt"
   mv tdisk.txt "tdisk_"$date".txt"
   mv mdisk.ps "mdisk_"$date".ps"
   cd ../../
fi

rm tstardisk.txt
rm tdisk.txt
rm profile.txt
rm angmom_*
rm angmom.res
dirmovie="movies/mstardisk"
mkdir $dirmovie

for file in $dir/output_0*00
   #file=$dir/output_00080
do
#   echo $file
#done
    rm profile.txt
   z="$(echo $file | rev |cut -d'_' -f1 | rev)"
   y="$(printf "%05d" $i)"
   filee="$(echo $file | rev |cut -d'/' -f1 | rev)"
   if [ -f $dirmovie'/tstardisk_'$z'.txt' ]
   then
      echo file $file already processed
   else
      echo processing file $file ....
      if [ $z -gt $i ]
      then
         echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
         ${VISUDIR}/../findcenter_sink
         ${VISUDIR}/../angmom
	 ${VISUDIR}/massstardisk
         #      echo "load 'plotmstardisk.gp'" | gnuplot
         #      stop
         #mv mur.ps movies/mur/'mur_'$z'.ps'
         #    mv rhor.png movies/rhor/'rhor_'$y'.png'
         #mv AMBI_brho.png 'AMBI_brho_'$n'.png'
         tail -n 1 'tstardisk.txt' > $dirmovie/'tstardisk_'$z'.txt'
         tail -n 1 'tdisk.txt' > $dirmovie/'tdisk_'$z'.txt'	  
	 cp 'profile.txt' $dirmovie/'profile_'$z'.txt'	  
	 let i=$i+1
      fi
   fi
done

cat "tstardisk.txt" >> $dirmovie/"tstardisk_.txt" 
mv $dirmovie/'tstardisk_.txt' $dirmovie/"tstardisk.txt"
cat "tdisk.txt" >> $dirmovie/"tdisk_.txt" 
mv $dirmovie/'tdisk_.txt' $dirmovie/"tdisk.txt"


rm angmom_*
rm angmom.res
#rm angmom.dat

cd movies/mstardisk/
#rm temp
#for mutruc in mu_*.res
#do
#cat $mutruc >> temp
#done
#cat mu.res >> temp
#mv temp mu.res

#echo "load '/home/bcommercon/visu_class0/analyse_folder/plotmstardisk.gp'" | gnuplot

#cd $dir

#rm mur.txt
#rm brho.dat


