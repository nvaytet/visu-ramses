set view map
set logscale cb
#set logscale x
set logscale y
#set palette model RGB defined (0 "white", 0.25 "orange", 0.55 "red", 1 "violet")
set palette model RGB defined (-0.00001 "black", 0 "white", 0.25 "orange", 0.55 "red", 1 "violet")

load "< awk '/t/ {print $0}' trho.txt"
#set label front "time = %4.3f",t," Kyears" at -17,20000.
set title sprintf("Time = %4.3f Kyrs",t)
set xlabel 'log({/Symbol r}) (g.cc^{-1})'
set ylabel 'T (K)'
set yrange [5:10000]
set xrange [-21:-2]
set cbrange [0.9:100]
set palette maxcolors 100

#set term png enh size 1800,1500 19
set term post enh color 17
#set output 'trho.png'
set output 'trho.ps'
#splot 'trho.txt' index 1 u 1:2:3 w pm3d not
splot 'trho.txt' index 1 u 1:2:3 w p pt 7 ps 0.12 palette not, 'rhoT_centre.dat' u (log10($2)):3:(0.9) w l lt 1 lc -1 lw 3 not
unset label
#set term wxt

