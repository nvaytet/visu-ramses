module ramses_lite
                               !  Ramses variables
  !___________________________________                           ___________________
                                      ! in module amr_parameters
  integer,parameter :: dp=kind(1.0D0)             ! real*8
  integer,parameter :: qdp=kind(1.0_8)            ! real*8
  integer,parameter :: MAXBOUND=100                                ! Boundary conditions parameters
  integer,parameter :: nvector=2000                                ! Size of vector sweeps
  integer           :: nboundary=0
  real(dp)          :: omega_b=0.0D0             ! Omega Baryon
  real(dp)          :: omega_m=1.0D0             ! Omega Matter
  real(dp)          :: omega_l=0.0D0             ! Omega Lambda
  real(dp)          :: omega_k=0.0D0             ! Omega Curvature
  real(dp)          :: h0     =1.0D0             ! Hubble constant in km/s/Mpc
  real(dp)          :: aexp   =1.0D0             ! Current expansion factor
                                        ! Mesh parameters
  integer           :: ngridmax =0               ! Maximum number of grids
  integer           :: levelmin =1               ! Full refinement up to levelmin
  integer           :: nlevelmax=1               ! Maximum number of level
  integer           :: nx=1 , ny=1 , nz=1        ! Number of coarse cells in each dimension
  integer           :: ndim                      ! originally a parameter in ramses
  integer           :: twotondim                 ! originally a parameter in ramses
  integer           :: threetondim               ! originally a parameter in ramses
  integer           :: twondim                   ! originally a parameter in ramses
  real(dp)          :: boxlen = 1.0D0            ! Box length along x direction
  character(len=128):: ordering='hilbert'
  !____________________________________                        ____________
                                       ! in module amr_commons
  integer           :: nstep_coarse=0            ! Coarse step
  integer           :: ncoarse                   ! nx.ny.nz
  integer           :: ngrid_current             ! Actual number of octs
  integer           :: ncpu , ndomain , myid=1 , overload=1
  real(dp)          :: t=0.0D0                                    ! Time variable
  real(qdp),allocatable,dimension(:):: hilbert_key                ! Hilbert key
  real(qdp),allocatable,dimension(:):: bound_key , bound_key2
  real(qdp)                         :: order_all_min , order_all_max
  integer,allocatable,dimension(:,:):: headl                 ! Pointers for each level linked list
  integer,allocatable,dimension(:,:):: taill
  integer,allocatable,dimension(:,:):: numbl
  integer,allocatable,dimension(:,:):: numbtot
  integer,allocatable,dimension(:,:):: headb                 ! Pointers for each level boundary linked list
  integer,allocatable,dimension(:,:):: tailb
  integer,allocatable,dimension(:,:):: numbb
  integer :: headf , tailf , numbf , used_mem , used_mem_tot   ! Pointers for free memory grid linked list
                                                ! Tree arrays
  real(dp),allocatable,dimension(:,:):: xg      ! grids position
  integer ,allocatable,dimension(:,:):: nbor    ! neighboring father cells
  integer ,allocatable,dimension(:)  :: father  ! father cell
  integer ,allocatable,dimension(:)  :: next    ! next grid in list
  integer ,allocatable,dimension(:)  :: prev    ! previous grid in list
  integer ,allocatable,dimension(:)  :: son     ! sons grids
  integer ,allocatable,dimension(:)  :: flag1   ! flag for refine
  integer ,allocatable,dimension(:)  :: flag2   ! flag for expansion
                                                                     ! Global indexing
  integer ,allocatable,dimension(:)  :: cpu_map  ! domain decomposition
  integer ,allocatable,dimension(:)  :: cpu_map2 ! new domain decomposition
  !____________________________________                        ____________
                                       ! in module hydro_parameters
  integer                            ::nvar     ! Number of independant variables
  real(dp)                           ::gamma    ! Hydro solver parameters
  !____________________________________                        ____________
                                       ! in module hydro_commons
  real(dp),allocatable,dimension(:,:)::uold,unew ! State vector and its update
end module ramses_lite
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
program amrhydro2ucd     ! lire les 3 fichiers ramses ( info, amr, hydro ) de chaque processeur successivement
  use ramses_lite
  USE LIB_VTK_IO
  implicit none
  integer                                 :: bucket(0:8,0:7) , diag_id     , face_id        ,                        &
                                             i , ii  , i_dumy  , ic , icell     , icell2    , icpu , i_fichier , idim  ,  &
                                             i_face       , igrid , ilevel , ind    ,                                &
                                             i_draw_cell  , i_ghost_cell   , i_ok_cell , ipos , is        , iskip ,  &
                                             ix , iy , iz , i_plan         , i_raff    , iv   , i_2d      , i_3d  ,  &
                                             j  , jj ,                                                               &
                                             k  , kk ,                                                               &
                                             max_draw_cell    , max_ok_cell  , max_ghost_cell ,                      &
                                             nb_hexahedron=0  , nb_square=0  , nb_trapeze=0 , nb_triangle=0  ,       &
                                             nb_split_nbor=0  , nb_hexa(7)=0 , nb_hexa_0=0  ,                        &
                                             ncache           , ncell       , ncell2   , n_dim_vtk , n_dim_hexalist ,&
                                             perp_id          , split_nbor(27)=0    
  integer , dimension(:)    , allocatable :: cpu_list , ind_drawcell , ind_okcell , ind_ghostcell ,  indvtk , order
  integer , dimension(:,:)  , allocatable :: ind_cell2          , hexa_2_dum2 , &
                                             nb_draw_cell       , nb_ok_cell  , &       
                                             nbors_father_cells , nbors_father_grids , nb_ghost_cell
  integer , dimension(:,:,:), allocatable :: hexa_list
  integer , dimension(nvector) , save     :: ind_cell 
  integer ,                      save     :: face_sibl_id(4,6) , quad_data(8,6,2) , sibl_id(2,8,6,2)
  integer , dimension(8)                  :: cubevertex_id  =         (/5,6,9,8,14,15,18,17/)
  integer , dimension(8,8)                :: cubevertex_sib_id , test_cubevertex_id 
  integer                                 :: vtk_quadrangle_id(3)= (/0,9,12/)
  integer                                 :: vtk_trapeze_id(3)   = (/0,9,9/)    ! tres provisoire (faux pour ndim=3)
  integer                                 :: vtk_triangle_id(3)  = (/0,5,14/)
  integer                                 :: count , count_old , count_rate , count_max
  real(dp),dimension(1:3)             :: xbound              = (/0d0,0d0,0d0/)
  real(dp),dimension(1:8,1:3)         :: xc , x_prov
  real(dp)                            :: dl , dl1 , dl2 , dl3 , dx  
  character(len=5)                        :: nchar   , ncharcpu
  character(len=128)                      :: nomfich , repository 
  character(len=128),dimension(:),allocatable :: file_list
  logical,dimension(:)  ,allocatable      :: cpu_read
  logical                                 :: quadrangle_ok , ok 
!                for VTK_IO Library
  integer(I1P) , dimension(:) , allocatable :: ghost_node       , vtk_cell_type 
  integer(I4P)                              :: E_IO             , nb_vtk_node    , nb_vtk_cell 
  integer(I4P) , save                       :: vtk_offset_count
  integer(I4P) , dimension(:) , allocatable :: vtk_connectivity , vtk_offset
  real(R4P)    , dimension(:) , allocatable :: dens_vtk_node    , press_vtk_node , level_vtk_node , &
                                                  x_vtk_node    ,     y_vtk_node ,     z_vtk_node , &
                                                 vx_vtk_node    ,    vy_vtk_node ,    vz_vtk_node , &
                                                 bx_vtk_node    ,    by_vtk_node ,    bz_vtk_node 
  interface 
    function QuickSort(InList) result(OutList)
     INTEGER,DIMENSION(:)              :: InList
     INTEGER,DIMENSION(size(InList,1)) :: OutList
    end function QuickSort
  end interface 


  CALL read_args
                                             ! check that first ramses files exist
  ipos    = INDEX(repository,'output_')
  nchar   = repository(ipos+7:ipos+13)
  inquire( file= TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out00001' , exist=ok ) 
  if ( .not. ok ) then ; print *,TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out00001'//' not found.' ; stop ; endif
  inquire( file=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'    , exist=ok )  
  if ( .not. ok ) then ; print *,TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'//' not found.'   ; stop ; endif

  nomfich = TRIM(repository)//'/info_'//TRIM(nchar)//'.txt'          
  CALL read_info ( nomfich )                                      !  read info file

  nomfich = TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'   !  read the head of the amr file
  CALL read_amr_header ( nomfich )                                !  to get  nx,ny,nz,ncoarse,nbound,twodim  etc
 
  nomfich = TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out00001' !  read the head of the first hydro file
  CALL read_hydro_header ( nomfich )                              !  to get nvar  etc
                                                     
  ncell   = ncoarse + twotondim*ngridmax
  xbound  = (/dble(nx/2),dble(ny/2),dble(nz/2)/)

  allocate (      file_list(1:ncpu) )
  allocate (       cpu_list(1:ncpu) )
  allocate (       cpu_read(1:ncpu) )               ; cpu_read      = .false.
  allocate (     nb_ok_cell(1:ncpu,1:nlevelmax) )   ; nb_ok_cell    = 0
  allocate (   nb_draw_cell(1:ncpu,1:nlevelmax) )   ; nb_draw_cell  = 0
  allocate (  nb_ghost_cell(1:ncpu,1:nlevelmax) )   ; nb_ghost_cell = 0

  allocate ( ind_cell2         (1:nvector,1:twondim) )
  allocate ( nbors_father_cells(1:nvector,1:threetondim) )
  allocate ( nbors_father_grids(1:nvector,1:twotondim) )

  allocate ( indvtk(ncell) ) ; indvtk = -1

  CALL alloc_ramses_lite
 
  write ( 6 , "(' ')" )
  write ( 6 , "(' time = ',1pe10.3,5x,'nstep_coarse = ',i6)" ) t ,  nstep_coarse 
  write ( 6 , "(' ndim = ',i2,3x,'ncpu = ',i2  )" ) ndim  ,   ncpu  
  write ( 6 , "(' nboundary  = ',i2,3x,'nx = ',i1,3x,'ny = ',i1,3x,'nz = ',i1,3x,'ncoarse = ',i2 )" )&
                  nboundary     ,       nx          , ny          , nz          , ncoarse   
  write ( 6 , "(' levelmin = ',i2, 3x, 'levelmax = ',i2 )" ) levelmin  , nlevelmax  
  write ( 6 , "(' max grid par processeur = ',i9,3x, 'max cell par processeur = ',i9 )" ) ngridmax , ncell
  write ( 6 , "(' ')" )

                   
  CALL init_search                      !  utilitaires pour la recherche des cellules VTK
                                        !  de raccordement entre les differents niveaux de raffinement
  test_cubevertex_id(:,1) = (/14,15,18,17,23,24,27,26/)  ;  cubevertex_sib_id(:,1) = (/ 8, 7, 5, 6, 4, 3, 1, 2/)
  test_cubevertex_id(:,2) = (/14,13,10,11, 5, 4, 1, 2/)  ;  cubevertex_sib_id(:,2) = (/ 1, 2, 4, 3, 5, 6, 8, 7/)
  test_cubevertex_id(:,3) = (/14,17,16,13,23,26,25,22/)  ;  cubevertex_sib_id(:,3) = (/ 7, 5, 6, 8, 3, 1, 2, 4/)
  test_cubevertex_id(:,4) = (/14,11,12,15, 5, 2, 3, 6/)  ;  cubevertex_sib_id(:,4) = (/ 2, 4, 3, 1, 6, 8, 7, 5/)
  test_cubevertex_id(:,5) = (/14,15,18,17, 5, 6, 9, 8/)  ;  cubevertex_sib_id(:,5) = (/ 4, 3, 1, 2, 8, 7, 5, 6/)
  test_cubevertex_id(:,6) = (/14,13,10,11,23,22,19,20/)  ;  cubevertex_sib_id(:,6) = (/ 5, 6, 8, 7, 1, 2, 4, 3/)
  test_cubevertex_id(:,7) = (/14,17,16,13, 5, 8, 7, 4/)  ;  cubevertex_sib_id(:,7) = (/ 3, 1, 2, 4, 7, 5, 6, 8/)
  test_cubevertex_id(:,8) = (/14,11,12,15,23,20,21,24/)  ;  cubevertex_sib_id(:,8) = (/ 6, 8, 7, 5, 2, 4, 3, 1/)

!  write ( 6 , "(a)") 'triangle ou pyramide'
!  do i_face=1,twondim                                      ! face_sibl_id(:,i_face)
!     write ( 6 , "('i_face =',i2,3x,'cell_rank =',i2,3x    , 'face_sibl_ids =',4(i2,2x))" )&
!                    i_face         , quad_data(2,i_face,1) , (face_sibl_id(i,i_face), i=1,2**(ndim-1) )
!  end do
!  write ( 6 , "(:,a)") 'trapeze ou wedge'
!  do i_face=1,twondim
!     write ( 6 , "('i_face =',i2,2x)" ) i_face
!     do i_plan=1,ndim-1
!        write ( 6 , "(14x,'i_plan =',i2,2x)" ) i_plan
!        do k=1,4
!           write ( 6 , "(28x,'quad_rank =',i2,2x         ,'sibl_id =',2(i2,2x))") &
!                              quad_data(k,i_face,i_plan) , ( sibl_id(kk,k,i_face,i_plan) , kk=1,ndim-1)
!        end do
!     end do
!  end do

  do i_fichier = 1 , ncpu
     CALL reset_ramses_lite 
     CALL title ( i_fichier , ncharcpu )

     nomfich = TRIM(repository)//'/amr_'//TRIM(nchar)//'.out'//TRIM(ncharcpu)
     CALL read_amr ( nomfich )                                    !  read amr file

     nomfich = TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out'//TRIM(ncharcpu)
     CALL read_hydro ( nomfich )                                  !  read hydro file

     do ilevel = levelmin , nlevelmax                          ! compute number of ramses cells to draw
        nb_ok_cell(  i_fichier,ilevel) = 0
        nb_draw_cell(:        ,ilevel) = 0
        do icpu=1,ncpu                                   ! compute number of ok_cells (and ghost_cells if multi cpu runs)
           if  ( headl(icpu,ilevel) /= 0 ) then
              igrid = headl(icpu,ilevel)
              do while  ( igrid /= 0 )
                 do ind=1,twotondim
                    icell = igrid + ncoarse + (ind-1)*ngridmax
                    if ( son(icell) == 0 ) then
                          nb_draw_cell(icpu,ilevel)  = nb_draw_cell(icpu,ilevel) + 1
                    end if               ! son(icell) == 0
                 end do                 ! ind
                 igrid = next(igrid)
              end do                  ! while igrid /= 0
           end if 
        end do     ! icpu
        nb_ok_cell(i_fichier,ilevel) =  nb_draw_cell(i_fichier,ilevel)
     end do                      ! ilevel

     max_draw_cell  = sum( nb_draw_cell              )
     max_ok_cell    = sum( nb_draw_cell(i_fichier,:) )
     max_ghost_cell = max_draw_cell - max_ok_cell

     write (6 , "(' ')" )
     write (6 , "('i_cpu',15x,'l =' ,14(i2,8x))" )            (                      ilevel  , ilevel=1,nlevelmax )
     write (6 , "(i2,5x,'nb_ok_cel=',14(i8,2x))" ) i_fichier ,( nb_ok_cell(i_fichier,ilevel) , ilevel=1,nlevelmax )
     write (6 , "(7x,'nb_ok_cel pour ce fichier',i8)" )              sum(  nb_ok_cell(i_fichier,:))    
     write (6 , "(7x,'nb_tot ',i8,2x,'nb_ok ',i8,2x,'nb_ghost ',i8)" ) max_draw_cell , max_ok_cell , max_ghost_cell
    
     allocate ( ind_okcell   (max_ok_cell)    ) ; ind_okcell    = 0  ! allocate list of ramses cells to draw
     allocate ( ind_ghostcell(max_ghost_cell) ) ; ind_ghostcell = 0  ! allocate list of ramses ghost cells to draw processors interfaces
     allocate ( ind_drawcell (max_draw_cell)  ) ; ind_drawcell  = 0  ! allocate list of ramses ghost cells to draw 
     allocate ( dens_vtk_node(max_draw_cell)    , press_vtk_node(max_draw_cell) , level_vtk_node(max_draw_cell) ,&
                   x_vtk_node(max_draw_cell)    ,     y_vtk_node(max_draw_cell) ,     z_vtk_node(max_draw_cell) ,&
                  vx_vtk_node(max_draw_cell)    ,    vy_vtk_node(max_draw_cell) ,    vz_vtk_node(max_draw_cell) ,&
                  bx_vtk_node(max_draw_cell)    ,    by_vtk_node(max_draw_cell) ,    bz_vtk_node(max_draw_cell) ,&
                   ghost_node(max_draw_cell)                                                                      )
                dens_vtk_node(:) = 0.           ; press_vtk_node(:) = 0.        ; level_vtk_node(:) = 0.                          
                   x_vtk_node(:) = 0.           ;     y_vtk_node(:) = 0.        ;     z_vtk_node(:) = 0. 
                  vx_vtk_node(:) = 0.           ;    vy_vtk_node(:) = 0.        ;    vz_vtk_node(:) = 0.
                  bx_vtk_node(:) = 0.           ;    by_vtk_node(:) = 0.        ;    bz_vtk_node(:) = 0.
                   ghost_node(:) = 0
                       indvtk(:) =-1

     i_ok_cell    = 0
     i_draw_cell  = 0
     i_ghost_cell = 0
     do ilevel = levelmin , nlevelmax               !  gather list of ramses cell to draw
        do icpu=1,ncpu                              ! look for all ghost unsplitted cells
           if ( headl(icpu,ilevel) /= 0 ) then
              dx  = 0.5**ilevel
              do ind=1,twotondim
                 iz = (ind-1)/4           ;   xc(ind,3) = (dble(iz)-0.5D0)*dx      ! Geometry
                 iy = (ind-1-4*iz)/2      ;   xc(ind,2) = (dble(iy)-0.5D0)*dx
                 ix = (ind-1-2*iy-4*iz)   ;   xc(ind,1) = (dble(ix)-0.5D0)*dx
              end do
              igrid = headl(icpu,ilevel)
              do while  ( igrid /= 0 )
                 do ind=1,twotondim
                    icell = igrid + ncoarse + (ind-1)*ngridmax
                    if ( son(icell) == 0 ) then
                                     i_draw_cell = i_draw_cell + 1
                       ind_drawcell(i_draw_cell) = icell
                                    flag2(icell) = 2
                       if ( icpu == i_fichier ) then
                                     i_ok_cell = i_ok_cell + 1
                         ind_okcell(i_ok_cell) = icell
                                  flag2(icell) = 1
                       end if
                       if ( icpu /= i_fichier ) ghost_node(i_draw_cell) = 1
                                  x_vtk_node(i_draw_cell) = ( xg(igrid,1)+xc(ind,1) - xbound(1) )  ! Compute cell center
                                  y_vtk_node(i_draw_cell) = ( xg(igrid,2)+xc(ind,2) - xbound(2) )  !  xg is read in amr
                       if(ndim>2) z_vtk_node(i_draw_cell) = ( xg(igrid,3)+xc(ind,3) - xbound(3) )
                                 vx_vtk_node(i_draw_cell) = uold(ind_drawcell(i_draw_cell),2)
                                 vy_vtk_node(i_draw_cell) = uold(ind_drawcell(i_draw_cell),3)
                                 bx_vtk_node(i_draw_cell) = 0.5*(uold(ind_drawcell(i_draw_cell),6)&
                                      +uold(ind_drawcell(i_draw_cell),nvar+1))
                                 by_vtk_node(i_draw_cell) = 0.5*(uold(ind_drawcell(i_draw_cell),7)&
                                      +uold(ind_drawcell(i_draw_cell),nvar+2))
                      if(ndim>2)vz_vtk_node(i_draw_cell) = uold(ind_drawcell(i_draw_cell),4)
                      if(ndim>2)bz_vtk_node(i_draw_cell) = 0.5*(uold(ind_drawcell(i_draw_cell),8)&
                           +uold(ind_drawcell(i_draw_cell),nvar+3))

                               dens_vtk_node(i_draw_cell) = uold(ind_drawcell(i_draw_cell),1)
                              press_vtk_node(i_draw_cell) = uold(ind_drawcell(i_draw_cell),ndim+2)
!11janv12                              level_vtk_node(i_draw_cell) = real(ilevel)
!29fev12                              level_vtk_node(i_draw_cell) = real(i_fichier)
                              level_vtk_node(i_draw_cell) = real(100*i_fichier) + real(ilevel)
                    end if               ! son(icell) == 0
                 end do                 ! ind
                 igrid = next(igrid)
              end do                  ! while igrid /= 0
           end if 
        end do ! icpu

     end do                      ! ilevel

      do  k=1,max_draw_cell             ! position of ok_cells in vtk list
         indvtk(ind_drawcell(k)) = k-1      ! vtk indices start at zero not one !!!
      end do

                                         ! allocate vtk memory
                                         ! A default de connaitre precisement le nombre de vtk_cell
                                         ! on dimensionne (largement) a 1.1*nombre de draw_cell
     n_dim_vtk      = INT( 1.2 * max_draw_cell )
     n_dim_hexalist = 8 * ( n_dim_vtk**(2./3.) )
     allocate ( vtk_cell_type   ( n_dim_vtk           ),&
                vtk_connectivity( n_dim_vtk*twotondim ),&
                vtk_offset      ( n_dim_vtk           ),&
                hexa_list       ( 8,n_dim_hexalist,7  )  )

     vtk_cell_type(:)    = 0
     vtk_connectivity(:) = 0
     vtk_offset(:)       = 0
     hexa_list(:,:,:)    = 0
     nb_vtk_cell         = 0
     vtk_offset_count    = 0
     nb_hexa_0           = 0
     nb_square           = 0
     nb_triangle         = 0
     nb_trapeze          = 0
     nb_hexahedron       = 0
 
     do ilevel = levelmin , nlevelmax
        dx     = 0.5**ilevel
        ncache = nb_ok_cell(i_fichier,ilevel) ; iskip=0 ; dl=0.0 ;  dl1=0.0 ; dl2=0.0 ; dl3=0.0 ; x_prov=0
        do  k=levelmin,ilevel
            iskip = iskip + nb_ok_cell(i_fichier,k-1)
        end do
        do icell = 1 , ncache , nvector
           ncell2 = MIN(nvector,ncache-icell+1)
           do ic=1,ncell2
              ind_cell(ic) = ind_okcell(icell-1+iskip+ic)
           end do 


           call get3cubefather ( ind_cell , nbors_father_cells , nbors_father_grids , ncell2 , ilevel )


           do icell2=1,ncell2
              quadrangle_ok = .true.             ! store the VTK quadrangle (3D => hexaedreon) if 4 (3D => 8) nbors OK 
              do is=1,twotondim                                    ! check that neighbours of the list are ok_cells
                            kk = nbors_father_cells(icell2,cubevertex_id(is))
                 if ( flag2(kk) == 0 ) then 
                               quadrangle_ok = .false. 
                 else
                               x_prov(is,1) = x_vtk_node( indvtk(kk) + 1 )    ! vtk indices start at zero not one !!!
                               x_prov(is,2) = y_vtk_node( indvtk(kk) + 1 )
                    if(ndim>2) x_prov(is,3) = z_vtk_node( indvtk(kk) + 1 )
                 end if
              end do
              if ( quadrangle_ok ) then                   ! exclude giant cells (periodic bondary conditions)
                 do idim=1,ndim
                    do is=1,twotondim-1
                       if ( ABS( x_prov(is+1,idim)-x_prov(is,idim) ) > 1.5*dx ) quadrangle_ok = .false.
                    end do
                 end do
              end if 
              if ( quadrangle_ok ) then 
                               nb_hexa_0    = nb_hexa_0 + 1
                               nb_square    = nb_square + 1
                               nb_vtk_cell  = nb_vtk_cell + 1
                 vtk_cell_type(nb_vtk_cell) = vtk_quadrangle_id(ndim)
                 do i=1,twotondim
                    vtk_connectivity(vtk_offset_count+i) = indvtk(nbors_father_cells(icell2,cubevertex_id(i)))
                 end do
                 vtk_offset_count        = vtk_offset_count + twotondim
                 vtk_offset(nb_vtk_cell) = vtk_offset_count
              end if                                                              ! end VTK quadrangle 
                              

              if ( ilevel == nlevelmax ) cycle    !  raccords avec niveau superieur inutile pour ilevel = nlevelmax

              nb_split_nbor = 0
              do k=1,threetondim             ! flag to one reffined neigbours (else zero)
                 split_nbor(k) = 0
                 if (     son(nbors_father_cells(icell2,k)) /= 0         &
                     .and.   (nbors_father_cells(icell2,k)) >  ncoarse  )then
                       split_nbor(k) = 1
                    nb_split_nbor    = nb_split_nbor + 1  
                 end if    
              end do
              if ( nb_split_nbor == 0 ) cycle     !  no reffined neigbour for cell icell2
                              
                                                         !   recherche des cellules triangulaires ou pyramidales
               do i_face = 1 , twondim    ! boucle sur les faces de la cellule centrale
                  face_id = quad_data(2,i_face,1) 
                  if ( split_nbor(face_id) == 1 ) then           ! si cellule (face) raffinee, utiliser les jumeaux
                                      nb_triangle  = nb_triangle + 1
                                      nb_vtk_cell  = nb_vtk_cell + 1
                        vtk_cell_type(nb_vtk_cell) = vtk_triangle_id(ndim)
                     do k = 1 , 2**(ndim-1)
                        vtk_connectivity(vtk_offset_count+k) = indvtk(  son( nbors_father_cells(icell2,face_id)) &
                                                                      + ncoarse + (face_sibl_id(k,i_face)-1)*ngridmax )
                     end do
                     vtk_connectivity(vtk_offset_count+1+2**(ndim-1)) = indvtk(ind_cell(icell2))
                                      vtk_offset_count                = vtk_offset_count +  1 + 2**(ndim-1)
                                      vtk_offset(nb_vtk_cell)         = vtk_offset_count
                  end if
                  do i_plan=1,ndim-1           ! trapezes (ou wedges) entre les triangles (ou pyramides)
                         if (     (                       split_nbor(quad_data(2,i_face,i_plan))  == 1 ) &
                            .and. (                       split_nbor(quad_data(3,i_face,i_plan))  == 1 ) &
                            .and. ( indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan))) /=-1 )  ) then
               ! exclude giant cells (periodic bondary conditions)  vtk indices start at zero not one !!!
                                       x_prov(1,1) = x_vtk_node( indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan))) + 1 )
                                       x_prov(1,2) = y_vtk_node( indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan))) + 1 )
                            if(ndim>2) x_prov(1,3) = z_vtk_node( indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan))) + 1 )
                                       x_prov(2,1) = x_vtk_node( indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan))) + 1 )
                                       x_prov(2,2) = y_vtk_node( indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan))) + 1 )
                            if(ndim>2) x_prov(2,3) = z_vtk_node( indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan))) + 1 )
                            ok = .true.               
                            do idim=1,ndim
                              if ( ABS( x_prov(2,idim)-x_prov(1,idim) ) > 1.5*dx ) ok = .false.
                            end do
                            if (.not. ok ) cycle
                                                     nb_trapeze  = nb_trapeze  + 1
                                                     nb_vtk_cell = nb_vtk_cell + 1
                            if ( ndim == 2 ) then
                                         vtk_cell_type(nb_vtk_cell) = vtk_trapeze_id(ndim)
                               vtk_connectivity(vtk_offset_count+1) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan))) 
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax)   
                               vtk_connectivity(vtk_offset_count+3) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,3,i_face,i_plan)-1)*ngridmax) 
                               vtk_connectivity(vtk_offset_count+4) = indvtk(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) 
                               vtk_offset_count                     = vtk_offset_count + 4
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else if ( ndim == 3 ) then
                                         vtk_cell_type(nb_vtk_cell) = 13
                               vtk_connectivity(vtk_offset_count+1) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan))) 
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax) 
                               vtk_connectivity(vtk_offset_count+3) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,2,i_face,i_plan)-1)*ngridmax) 
                               vtk_connectivity(vtk_offset_count+4) = indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan))) 
                               vtk_connectivity(vtk_offset_count+5) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,3,i_face,i_plan)-1)*ngridmax) 
                               vtk_connectivity(vtk_offset_count+6) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,3,i_face,i_plan)-1)*ngridmax) 
                               vtk_offset_count                     = vtk_offset_count + 6
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else
                               stop 'trapezes ndim pas prevu'
                            end if
                         end if
                         if (     ( split_nbor(quad_data(2,i_face,i_plan)) == 1 ) &
                            .and. ( split_nbor(quad_data(3,i_face,i_plan)) == 1 ) &
                            .and. ( split_nbor(quad_data(4,i_face,i_plan)) == 1 )  ) then
                            if ( ndim == 2 ) then
                                                        nb_trapeze  = nb_trapeze  + 1
                                                       nb_vtk_cell  = nb_vtk_cell + 1
                                         vtk_cell_type(nb_vtk_cell) = vtk_trapeze_id(ndim)
                               vtk_connectivity(vtk_offset_count+1) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan)))
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+3) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,3,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+4) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,4,i_face,i_plan)-1)*ngridmax)
                               vtk_offset_count                     = vtk_offset_count + 4
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else if ( ndim == 3 ) then                                 ! une pyramide et un coin
                                                       nb_triangle  = nb_triangle + 1
                                                       nb_vtk_cell  = nb_vtk_cell + 1
                                         vtk_cell_type(nb_vtk_cell) = 14
                               vtk_connectivity(vtk_offset_count+1) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+3) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,4,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+4) = indvtk( son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,4,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+5) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan)))
                               vtk_offset_count                     = vtk_offset_count + 5
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                                                        nb_trapeze  = nb_trapeze  + 1
                                                       nb_vtk_cell  = nb_vtk_cell + 1
                                         vtk_cell_type(nb_vtk_cell) = 13
                               vtk_connectivity(vtk_offset_count+1) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,3,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+3) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,4,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+4) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+5) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,3,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+6) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,4,i_face,i_plan)-1)*ngridmax)
                               vtk_offset_count                     = vtk_offset_count + 6
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else
                               stop 'sortant ndim pas prevu'
                            end if
                         end if
                         if (     ( split_nbor(quad_data(2,i_face,i_plan)) == 0 ) &
                            .and. ( split_nbor(quad_data(3,i_face,i_plan)) == 1 ) &
                            .and. ( split_nbor(quad_data(4,i_face,i_plan)) == 0 )  ) then
                                                     nb_trapeze  = nb_trapeze  + 1
                                                     nb_vtk_cell = nb_vtk_cell + 1
                            if ( ndim == 2 ) then
                                         vtk_cell_type(nb_vtk_cell) = vtk_trapeze_id(ndim)
                               vtk_connectivity(vtk_offset_count+1) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan)))
                               vtk_connectivity(vtk_offset_count+2) = indvtk(nbors_father_cells(icell2,quad_data(2,i_face,i_plan)))
                               vtk_connectivity(vtk_offset_count+3) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,3,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+4) = indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan)))
                               vtk_offset_count                     = vtk_offset_count + 4
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else if ( ndim == 3 ) then
                                         vtk_cell_type(nb_vtk_cell) = 14
                               vtk_connectivity(vtk_offset_count+1) = indvtk(nbors_father_cells(icell2,quad_data(2,i_face,i_plan)))
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,3,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+3) = indvtk(nbors_father_cells(icell2,quad_data(4,i_face,i_plan)))
                               vtk_connectivity(vtk_offset_count+4) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(3,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,3,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+5) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan)))
                               vtk_offset_count                     = vtk_offset_count + 5
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else
                               stop 'rentrant ndim pas prevu'
                            end if
                         end if
                         if (     ( split_nbor(quad_data(2,i_face,i_plan)) == 1 ) &
                            .and. ( split_nbor(quad_data(3,i_face,i_plan)) == 0 ) &
                            .and. ( split_nbor(quad_data(4,i_face,i_plan)) == 1 )  ) then
                                                     nb_triangle = nb_triangle + 1
                                                     nb_vtk_cell = nb_vtk_cell + 1
                            if ( ndim == 2 ) then
                                         vtk_cell_type(nb_vtk_cell) = vtk_triangle_id(ndim)
                               vtk_connectivity(vtk_offset_count+1) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan)))
                               vtk_connectivity(vtk_offset_count+2) = indvtk( son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+3) = indvtk(nbors_father_cells(icell2,quad_data(3,i_face,i_plan)))
                               vtk_offset_count                     = vtk_offset_count + 3
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else if ( ndim == 3 ) then
                                         vtk_cell_type(nb_vtk_cell) = vtk_triangle_id(ndim)
                               vtk_connectivity(vtk_offset_count+1) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+2) = indvtk(son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(1,4,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+3) = indvtk( son(nbors_father_cells(icell2,&
                                    quad_data(4,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,4,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+4) = indvtk( son(nbors_father_cells(icell2,&
                                    quad_data(2,i_face,i_plan))) &
                                                                             + ncoarse + (sibl_id(2,2,i_face,i_plan)-1)*ngridmax)
                               vtk_connectivity(vtk_offset_count+5) = indvtk(nbors_father_cells(icell2,quad_data(1,i_face,i_plan)))
                               vtk_offset_count                     = vtk_offset_count + 5
                               vtk_offset(nb_vtk_cell)              = vtk_offset_count
                            else
                               stop 'isme ndim pas prevu'
                            end if
                         end if
                  end do      ! i_plan
               end do       ! i_face

!            recherche des hexaedres aux intefaces entre ilevel et ilevel+1
!            pour le carre 3x3 
               if ( ndim == 3 ) then           ! seulement en 3D
                         do k=1,8      ! k boucle sur les huit sous cubes pour les hexahedres a n raffines
                            ok = .false.
                            nb_split_nbor = 0
                            do kk=1,8    ! verifie que au moins un des huit points est raffine
                               if ( split_nbor(test_cubevertex_id(kk,k)) == 1 )  nb_split_nbor = nb_split_nbor + 1
                            end do
                            if ( nb_split_nbor == 0 ) cycle
                            ok = .true.
                            do kk=1,8     ! sortir si un des point non raffine n'est pas un node vtk
                               if ( ( indvtk(nbors_father_cells(icell2,test_cubevertex_id(kk,k))) == -1 ) .and. &
                                  (                         split_nbor(test_cubevertex_id(kk,k))  ==  0 )         ) ok = .false.
                            end do
                            if ( ok ) then
                               nb_hexa(nb_split_nbor) = nb_hexa(nb_split_nbor) + 1
                               do kk=1,8
                                  if ( split_nbor(test_cubevertex_id(kk,k)) == 0 ) then
                                       hexa_list(kk,nb_hexa(nb_split_nbor),nb_split_nbor) = &
                                            indvtk(nbors_father_cells(icell2,test_cubevertex_id(kk,k)))
                                  else
                                       hexa_list(kk,nb_hexa(nb_split_nbor),nb_split_nbor) = &
                                            indvtk( son(nbors_father_cells(icell2,test_cubevertex_id(kk,k))) &
                                            + ncoarse + ( cubevertex_sib_id(kk,k)-1 )*ngridmax )
                                  end if
                               end do
                            end if
                         end do   ! k boucle sur les huit sous cubes
               end if   ! ndim = 3

           end do        ! icell2
        end do          ! icell
!provisoire
         if ( ilevel == nlevelmax ) cycle    !  raccords avec niveau superieur inutile pour ilevel = nlevelmax
!         write ( 6 , "('ilevel =',i6 ,5x,'n_dim_hexalist =',i8,5x,'nb_hexa_0_7 =', 8(i7,2x))" ) ilevel , n_dim_hexalist , nb_hexa_0 , nb_hexa(1:7)
         do i_raff=1,7
            if ( nb_hexa(i_raff) > n_dim_hexalist ) write &
                 ( 6 , "('nb_hexa(',i1,') > n_dim_hexalist ',i8)" ) i_raff , nb_hexa(i_raff)
            allocate ( hexa_2_dum2(8,nb_hexa(i_raff)) , order(nb_hexa(i_raff)) )
                       hexa_2_dum2(:,:)=0             ; order(:)=0

!            call system_clock ( count , count_rate , count_max )
!            write ( 6 , "(4(i,5x))" ) count , count-count_old ,count_rate , count_max
!            count_old = count

            do kk=1,nb_hexa(i_raff)                                       ! copie triee des coins des hexahedres
               hexa_2_dum2(:,kk) = quicksort ( hexa_list(:,kk,i_raff) )
                       order(kk) = 0
            end do

!            call system_clock ( count , count_rate , count_max )
!            write ( 6 , "(4(i,5x))" ) count , count-count_old ,count_rate , count_max
!            count_old = count

            do kk=1,nb_hexa(i_raff)                                        ! marquage des doublons
               if ( order(kk) > 0 ) cycle
               i_dumy = 0
               do jj=kk,nb_hexa(i_raff)
                  if ( (jj==kk) .or. (order(jj)>0) ) cycle
                  if (      (hexa_2_dum2(1,jj) == hexa_2_dum2(1,kk)) &
                       .and.(hexa_2_dum2(2,jj) == hexa_2_dum2(2,kk)) &
                       .and.(hexa_2_dum2(3,jj) == hexa_2_dum2(3,kk)) &
                       .and.(hexa_2_dum2(4,jj) == hexa_2_dum2(4,kk)) &
                       .and.(hexa_2_dum2(5,jj) == hexa_2_dum2(5,kk)) &
                       .and.(hexa_2_dum2(6,jj) == hexa_2_dum2(6,kk)) &
                       .and.(hexa_2_dum2(7,jj) == hexa_2_dum2(7,kk)) &
                       .and.(hexa_2_dum2(8,jj) == hexa_2_dum2(8,kk)) )  then 
                      i_dumy    = i_dumy  + 1
                      order(jj) = i_dumy
                  end if
!!!!!                    if ( i_dumy >= (8 - i_raff) ) exit
               end do
            end do

!            call system_clock ( count , count_rate , count_max )
!            write ( 6 , "(4(i,5x))" ) count , count-count_old ,count_rate , count_max
!            count_old = count


            do kk=1,nb_hexa(i_raff)                                       ! sortie VTK sans les doublons
               if ( order(kk) > 0 ) cycle
                                                   nb_hexahedron  = nb_hexahedron + 1
                                                     nb_vtk_cell  = nb_vtk_cell + 1
                                       vtk_cell_type(nb_vtk_cell) = vtk_quadrangle_id(ndim)
                  do k=1,8 ; vtk_connectivity(vtk_offset_count+k) = hexa_list(k,kk,i_raff) ; end do
                                              vtk_offset_count    = vtk_offset_count + 8
                                          vtk_offset(nb_vtk_cell) = vtk_offset_count
            end do

            bucket(:,i_raff)=0
            do kk=1,nb_hexa(i_raff)                ! statistiques provisoires sur les doublons
               select case ( order(kk) )
                   case ( 0 )   ; bucket(0,i_raff)=bucket(0,i_raff)+1
                   case ( 1 )   ; bucket(1,i_raff)=bucket(1,i_raff)+1
                   case ( 2 )   ; bucket(2,i_raff)=bucket(2,i_raff)+1
                   case ( 3 )   ; bucket(3,i_raff)=bucket(3,i_raff)+1
                   case ( 4 )   ; bucket(4,i_raff)=bucket(4,i_raff)+1
                   case ( 5 )   ; bucket(5,i_raff)=bucket(5,i_raff)+1
                   case ( 6 )   ; bucket(6,i_raff)=bucket(6,i_raff)+1
                   case ( 7 )   ; bucket(7,i_raff)=bucket(7,i_raff)+1
                   case default ; bucket(8,i_raff)=bucket(8,i_raff)+1
               end select
            end do
!            write (6 , "('level =',i2,5x,'nb et type de doublons pour i_raff =',i2,3x,9(i8,2x))" ) ilevel , i_raff , bucket(0:8,i_raff)

            deallocate ( order , hexa_2_dum2 )
         end do  ! i_raff

         nb_hexa(:) = 0
         nb_hexa_0  = 0
!provisoire
     end do            ! ilevel

     if ( nb_vtk_cell > n_dim_vtk ) then
        write ( 6 , "('nb_vtk_cell=',i8,3x,'nb_dim_vtk=',i8)" ) nb_vtk_cell , n_dim_vtk
        stop 'nb_vtk_cell > nb_dim_vtk'
     else
        write ( 6 , "('nb_vtk_cell=',i8,3x,'nb_dim_vtk =',i8)" ) nb_vtk_cell , n_dim_vtk
        write ( 6 , "('nb_square  =',i8,3x,'nb_triangle=',i8,3x,'nb_trapeze=',i8,3x,'nb_hexaedrons=',i8)" )&
                       nb_square ,          nb_triangle ,        nb_trapeze ,        nb_hexahedron 
     end if
     nb_vtk_node = max_draw_cell
!                                                                sortie fichier des vtk_cells
     nomfich = TRIM(repository)//TRIM('/UCD_RAMSES_'//nchar//'_'//ncharcpu//'.vtu')
     write ( 6 , "('ecriture du fichier ',a)" ) nomfich
     file_list(i_fichier) = TRIM('UCD_RAMSES_'//nchar//'_'//ncharcpu//'.vtu')
     E_IO = VTK_INI_XML ( output_format =    'BINARY'     , &
                          filename      =  TRIM(nomfich)  , &
                          mesh_topology = 'UnstructuredGrid')

     E_IO = VTK_GEO_XML ( NN = nb_vtk_node , NC = nb_vtk_cell , &
                          X=x_vtk_node     , Y=y_vtk_node     , Z=z_vtk_node)
     
     E_IO = VTK_CON_XML ( NC        = nb_vtk_cell      , &
                          connect   = vtk_connectivity , &
                          offset    = vtk_offset       , &
                          cell_type = vtk_cell_type       )

     E_IO = VTK_DAT_XML ( var_location     = 'node', &
                          var_block_action = 'OPEN'   )

     E_IO = VTK_VAR_XML ( NC_NN   = nb_vtk_node  , &
                          varname = 'densite'    , &
                          var     = dens_vtk_node    )

     E_IO = VTK_VAR_XML ( NC_NN   = nb_vtk_node  , &
                          varname = 'Vitesse'    , &
                          varx    = vx_vtk_node  , &
                          vary    = vy_vtk_node  , &
                          varz    = vz_vtk_node     )

     E_IO = VTK_VAR_XML ( NC_NN   = nb_vtk_node  , &
                          varname = 'Bfield'    , &
                          varx    = bx_vtk_node  , &
                          vary    = by_vtk_node  , &
                          varz    = bz_vtk_node     )

     E_IO = VTK_VAR_XML ( NC_NN   = nb_vtk_node  , &
                          varname = 'Pression'   , &
                          var     = press_vtk_node  )

     E_IO = VTK_VAR_XML ( NC_NN   = nb_vtk_node  , &
                          varname = 'Cent_Proc+Rlevel'    , &
                          var     = level_vtk_node  )

     ! include visit ghostnodes in .vtu file
     if ( ncpu > 1 )                                 &
     E_IO = VTK_VAR_XML ( NC_NN   = nb_vtk_node    , &
                          varname = 'avtGhostNodes', &
                          var     =     ghost_node    )

     E_IO = VTK_DAT_XML ( var_location     = 'node' ,&
                          var_block_action = 'CLOSE'  )
     E_IO = VTK_GEO_XML()
     E_IO = VTK_END_XML()

     deallocate ( ind_okcell    , ind_drawcell     , ind_ghostcell    , hexa_list )
     deallocate ( vtk_cell_type , vtk_connectivity , vtk_offset   )
     deallocate ( dens_vtk_node , press_vtk_node   , level_vtk_node ,&
                     x_vtk_node ,     y_vtk_node   ,     z_vtk_node ,&
                    bx_vtk_node ,    by_vtk_node   ,    bz_vtk_node ,&
                    vx_vtk_node ,    vy_vtk_node   ,    vz_vtk_node   )
     deallocate (    ghost_node )

  end do    ! i_fichier

    write ( 6 , "('ecriture du fichier multiblocks ',a)" ) TRIM(repository)//TRIM('/UCD_RAMSES_'//nchar//'.visit')
    open  ( 14 , file=TRIM(repository)//TRIM('/UCD_RAMSES_'//nchar//'.visit') , form='formatted')
    write ( 14 , "('!NBLOCKS ',i4)" ) ncpu
    write ( 14 ,"(a)" ) (TRIM(file_list(k)),k=1,ncpu)
    close ( 14 )

     write (6 , "( 1x,'* SUMMARY *',4x,'l = ', 14(i2,8x))" ) ( ilevel   , ilevel=1,nlevelmax )
  if ( ncpu > 1) then
     write (6 , "( 1x,'nb grid tot  '         ,14(i8,2x))" ) ( numbtot(1,ilevel) , ilevel=1,nlevelmax )
     write (6 , "( 1x,'nb min grid  '         ,14(i8,2x))" ) ( numbtot(2,ilevel) , ilevel=1,nlevelmax )
     write (6 , "( 1x,'nb max grid  '         ,14(i8,2x))" ) ( numbtot(3,ilevel) , ilevel=1,nlevelmax )
     write (6 , "( 1x,'nb grid moyen'         ,14(i8,2x))" ) ( numbtot(4,ilevel) , ilevel=1,nlevelmax )
  end if 
     write (6 , "( 1x,'nb_ok_cel    '         ,14(i8,2x))" )   sum(nb_ok_cell,1)
     write (6 , "( 1x,'nb_ok_cel  total   '    ,i8)"       )   sum ( nb_ok_cell )

  CALL dealloc_ramses_lite

  deallocate ( file_list , cpu_list , cpu_read , nb_ok_cell , nb_draw_cell )
  deallocate ( ind_cell2 , nbors_father_cells , nbors_father_grids )

  contains
!-----------------------------------------------------------------------------------
  subroutine read_args
    implicit none
    integer           :: i,iargc,n
    character(len=4)  :: opt ; character(len=128) ::arg
    n = iargc()
    if (n < 2) then
       print *, 'usage: amrhydro2ucd   -inp  input_dir'
       print *, 'ex: amrhydro2ucd  -inp output_00001 '
       stop
    end if
    do i = 1,n,2
       CALL getarg(i,opt)
       if (i == n) then
          print '("option ",a2," has no argument")', opt
          stop 2
       end if
       CALL getarg(i+1,arg)
       select case (opt)
          case ('-inp')  ;  repository = trim(arg)
          case default   ;  print '("unknown option ",a2," ignored")', opt
       end select
    end do
  end subroutine read_args
!-----------------------------------------------------------------------------------
subroutine init_search
   implicit none
   integer                              :: ii , imin , imax , i_save , is_2 , is_3 , is_4 ,&
                                           jmin , jmax , kmin   ,  kmax  , sibl_dumy(4) 
   integer                              :: diag_coord(3)   ,  prod_vect(3,6,2) , v_test(3,8)    ,&
                                           f_sibling_id(2) , d_sibling_id(2)   , p_sibling_id(2) 
   integer , dimension( 3, 6) , save    :: face_coord     = reshape ((/-1, 0, 0,&
                                                                       +1, 0, 0,&
                                                                        0,-1, 0,&
                                                                        0,+1, 0,&
                                                                        0, 0,-1,&
                                                                        0, 0,+1/) , shape=(/3,6/) )
   integer , dimension( 3, 6,2) , save  :: perpface_coord = reshape ((/ 0,-1, 0,&
                                                                        0, 1, 0,&
                                                                        1, 0, 0,&
                                                                       -1, 0, 0,&
                                                                       -1, 0, 0,&
                                                                        1, 0, 0,&
                                                                        0, 0, 1,&
                                                                        0, 0,-1,&
                                                                        0, 0,-1,&
                                                                        0, 0, 1,&
                                                                        0, 1, 0,&
                                                                        0,-1, 0/) , shape=(/3,6,2/) )
   i_2d =  0  ; if (ndim>1) i_2d = 1
   i_3d =  0  ; if (ndim>2) i_3d = 1
   imin = -1  ; imax = +1
   jmin = -1  ; jmax = -1 ; if(ndim>1) jmax = +1
   kmin = -1  ; kmax = -1 ; if(ndim>2) kmax = +1
   do i_face = 1 ,twondim            ! boucle sur les faces de la cellule centrale
      ii = 0
      do k=kmin,kmax,2
         do j=jmin,jmax,2
            do i=imin,imax,2
                 if (    ( i == -face_coord(1,i_face) ) &
                     .or.( j == -face_coord(2,i_face) ) &
                     .or.( k == -face_coord(3,i_face) ) ) then
                               ii  = ii + 1
                     sibl_dumy(ii) = ( i + 2*j + 4*k + 7 )/2+1
                  end if
            end do
         end do
      end do
      i_save  = sibl_dumy(4) ; sibl_dumy(4) = sibl_dumy(3) ; sibl_dumy(3) = i_save  ! swap last ids for VTK specs
      face_sibl_id(:,i_face) = sibl_dumy(:)
      do i_plan=1,ndim-1
         prod_vect(1,i_face,i_plan)=  face_coord(2,i_face)*perpface_coord(3,i_face,i_plan)&
                                    - face_coord(3,i_face)*perpface_coord(2,i_face,i_plan)
         prod_vect(2,i_face,i_plan)=  face_coord(3,i_face)*perpface_coord(1,i_face,i_plan)&
                                    - face_coord(1,i_face)*perpface_coord(3,i_face,i_plan)
         prod_vect(3,i_face,i_plan)=  face_coord(1,i_face)*perpface_coord(2,i_face,i_plan)&
                                 - face_coord(2,i_face)*perpface_coord(1,i_face,i_plan)
         v_test(:,1) =                              0
         v_test(:,2) =                              face_coord(:,i_face)
         v_test(:,3) =                              face_coord(:,i_face)+ perpface_coord(:,i_face,i_plan)
         v_test(:,4) =                                                    perpface_coord(:,i_face,i_plan)
         if ( ndim > 2 ) then
         v_test(:,5) = prod_vect(:,i_face,i_plan) 
         v_test(:,6) = prod_vect(:,i_face,i_plan) + face_coord(:,i_face)
         v_test(:,7) = prod_vect(:,i_face,i_plan) + face_coord(:,i_face)+ perpface_coord(:,i_face,i_plan)
         v_test(:,8) = prod_vect(:,i_face,i_plan) +                       perpface_coord(:,i_face,i_plan)
         end if
         do iv=1,twotondim
            quad_data(iv,i_face,i_plan) =         ( v_test(1,iv)+2 )& 
                                         + i_2d*3*( v_test(2,iv)+1 )&
                                         + i_3d*9*( v_test(3,iv)+1 )
         end do
         face_id = quad_data(2,i_face,i_plan)                               ; is_2 = 0
         diag_id = quad_data(3,i_face,i_plan)  ;  diag_coord(:)=v_test(:,3) ; is_3 = 0
         perp_id = quad_data(4,i_face,i_plan)                               ; is_4 = 0
         do k=kmin,kmax,2
            do j=jmin,jmax,2
               do i=imin,imax,2
                   if ((     ( i ==    -face_coord(1,i_face)        )   &
                        .or. ( j ==    -face_coord(2,i_face)        )   &
                        .or. ( k ==    -face_coord(3,i_face)        ))  &
                                    .and.                               &
                       (     ( i == perpface_coord(1,i_face,i_plan) )   &
                        .or. ( j == perpface_coord(2,i_face,i_plan) )   &
                        .or. ( k == perpface_coord(3,i_face,i_plan) )) ) then
                                    is_2  = is_2 + 1
                       f_sibling_id(is_2) = ( i + 2*j + 4*k + 7 )/2+1
                   end if
                   if ((     ( i == -diag_coord(1) )   &
                        .and.( j == -diag_coord(2) ) ) &
                                   .or.                &
                       (     ( i == -diag_coord(1) )   &
                        .and.( k == -diag_coord(3) ) ) &
                                   .or.                &
                       (     ( j == -diag_coord(2) )   & 
                        .and.( k == -diag_coord(3) ) )) then
                                    is_3  = is_3 + 1
                       d_sibling_id(is_3) = ( i + 2*j + 4*k + 7 )/2+1
                   end if
                   if ((     ( i == -perpface_coord(1,i_face,i_plan) )   &
                        .or. ( j == -perpface_coord(2,i_face,i_plan) )   &
                        .or. ( k == -perpface_coord(3,i_face,i_plan) ))  &
                                    .and.                                &
                       (     ( i ==      face_coord(1,i_face)        )   &
                        .or. ( j ==      face_coord(2,i_face)        )   &
                        .or. ( k ==      face_coord(3,i_face)        )) ) then
                                    is_4  = is_4 + 1
                       p_sibling_id(is_4) = ( i + 2*j + 4*k + 7 )/2+1
                   end if
               end do
            end do
         end do
         sibl_id(:,1,i_face,i_plan)=0
         sibl_id(:,2,i_face,i_plan)=f_sibling_id(:)
         sibl_id(:,3,i_face,i_plan)=d_sibling_id(:)
         sibl_id(:,4,i_face,i_plan)=p_sibling_id(:)
      end do     ! i_plan
   end do         ! i_face
end subroutine init_search
!-----------------------------------------------------------------------------------
end program amrhydro2ucd
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine read_info ( nomfich )
  use ramses_lite
  implicit none
  integer              :: i     , impi
  real                 :: scale , scale_l , scale_d , scale_t 
  character(LEN=128)   :: nomfich 
    open ( unit=10 , file=TRIM(nomfich) , form='formatted' , status='old' )
    read ( 10 , '(13x,I11)')ncpu
    read ( 10 , '(13x,I11)')ndim
    read ( 10 , '(13x,I11)')levelmin
    read ( 10 , '(13x,I11)')nlevelmax
    read ( 10 , '(13x,I11)')ngridmax
    read ( 10 , '(13x,I11)')nstep_coarse
    read ( 10 , * ) 
    read ( 10 , '(13x,E23.15)')scale
    read ( 10 , '(13x,E23.15)')t
    read ( 10 , '(13x,E23.15)')aexp
    read ( 10 , '(13x,E23.15)')h0
    read ( 10 , '(13x,E23.15)')omega_m
    read ( 10 , '(13x,E23.15)')omega_l
    read ( 10 , '(13x,E23.15)')omega_k
    read ( 10 , '(13x,E23.15)')omega_b
    read ( 10 , '(13x,E23.15)')scale_l
    read ( 10 , '(13x,E23.15)')scale_d
    read ( 10 , '(13x,E23.15)')scale_t
!!$    read ( 10 , * ) ! mu_gas
!!$    read ( 10 , * ) ! ngrp
    read ( 10 , * )
    read ( 10 , '(14x,A80)')ordering
    read ( 10 , * )
    if ( TRIM(ordering).eq.'hilbert' ) then
       write(*,*) 'got to here 2'
       allocate ( bound_key(0:ncpu) )
       do impi=1,ncpu
          read ( 10 ,'(I8,1X,E23.15,1X,E23.15)') i , bound_key(impi-1) , bound_key(impi)
       end do
    endif
    close(10)
end subroutine read_info
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine read_amr_header(nomfich)
  use ramses_lite 
  implicit none
  character(LEN=128)   :: nomfich 
    open  ( unit=11 , file=nomfich , status='old' , form='unformatted' )
    read  ( 11 ) ncpu
    read  ( 11 ) ndim
    read  ( 11 ) nx , ny , nz
    read  ( 11 ) nlevelmax
    read  ( 11 ) ngridmax
    read  ( 11 ) nboundary
    read  ( 11 ) ngrid_current
    read  ( 11 ) boxlen
    close ( 11 )
    twotondim = 2**ndim   ; twondim=2*ndim ; threetondim=3**ndim 
    ncoarse   = nx*ny*nz 
end subroutine read_amr_header
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine read_hydro_header(nomfich)
  use ramses_lite , only : nvar
  implicit none
  character(LEN=128)   :: nomfich 
  integer              :: ncpu2
       open ( unit = 12 , file=nomfich , status='old' , form='unformatted' )
              read ( 12 ) ncpu2
              read ( 12 ) nvar
             close ( 12 )
end subroutine read_hydro_header
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine read_hydro(nomfich)
  use ramses_lite
  implicit none
  character(LEN=128)   :: nomfich 
!ng_28_avril_20011  integer              :: ncell , ncache , iskip , igrid , i , ilevel , ind , ivar
  integer              :: ncache , iskip , igrid , i , ilevel , ind , ivar
  integer              :: ilevel2 , numbl2 , ibound , istart 
!  integer              :: ncpu2 , ndim2 , nlevelmax2 , nboundary2
  integer ,dimension(:),allocatable::ind_grid
  real(dp),dimension(:),allocatable::xx
!  real(dp)::gamma2	
       open ( unit = 12 , file=nomfich , status='old' , form='unformatted' )
              read ( 12 ) ncpu
              read ( 12 ) nvar
              read ( 12 ) ndim
              read ( 12 ) nlevelmax
              read ( 12 ) nboundary
              read ( 12 ) gamma
              !print*,nvar
              !stop
              do ilevel=1,nlevelmax
                 do ibound=1,nboundary+ncpu
                    if(ibound<=ncpu)then
                       ncache=numbl(ibound,ilevel)
                       istart=headl(ibound,ilevel)
                    else
                       ncache=numbb(ibound-ncpu,ilevel)
                       istart=headb(ibound-ncpu,ilevel)
                    end if
                    read ( 12 ) ilevel2
                    read ( 12 ) numbl2
                    if(numbl2.ne.ncache)then
                       write(*,*)'File hydro.tmp is not compatible'
                       write(*,*)'Found   =',numbl2,' for level ',ilevel2
                       write(*,*)'Expected=',ncache,' for level ',ilevel
                    end if
                    if(ncache>0)then
                       allocate(ind_grid(1:ncache))
                       allocate(xx(1:ncache))
                       ! Loop over level grids
                       igrid=istart
                       do i=1,ncache
                          ind_grid(i)=igrid
                          igrid=next(igrid)
                       end do
                       ! Loop over cells
                       do ind=1,twotondim
                          iskip=ncoarse+(ind-1)*ngridmax
                          ! Loop over conservative variables
                          do ivar=1,nvar
                             read ( 12 ) xx
                             if(ivar==1)then                              ! densite
                                do i=1,ncache
                                   uold(ind_grid(i)+iskip,1)=xx(i)
                                end do
                             else if(ivar>=2.and.ivar<=4)then        ! vitesse
                                do i=1,ncache
!                                   uold(ind_grid(i)+iskip,ivar)=xx(i)*uold(ind_grid(i)+iskip,1)
!NG_4juillet2011 
                                   uold(ind_grid(i)+iskip,ivar)=xx(i)
                                end do
                             else if(ivar>=5.and.ivar<=7)then
                                do i=1,ncache
!                                   xx(i)=xx(i)/(gamma-1d0)
!                                   xx(i)=xx(i)+0.5d0*uold(ind_grid(i)+iskip,2)**2/uold(ind_grid(i)+iskip,1)
!                if ( ndim > 1 )    xx(i)=xx(i)+0.5d0*uold(ind_grid(i)+iskip,3)**2/uold(ind_grid(i)+iskip,1)
!                if ( ndim > 2 )    xx(i)=xx(i)+0.5d0*uold(ind_grid(i)+iskip,4)**2/uold(ind_grid(i)+iskip,1)
                                   uold(ind_grid(i)+iskip,ivar+1)=xx(i) ! champ magnetique left
                                end do
                             else if(ivar>=8.and.ivar<=10)then
                                do i=1,ncache
                                   uold(ind_grid(i)+iskip,nvar+ivar-7)=xx(i) ! champ magnetique right
                                end do
                            else if(ivar==11)then           ! pression
                                do i=1,ncache
                                    uold(ind_grid(i)+iskip,5)=xx(i)
                                end do
                            end if
                                !do i=1,ncache
                                   !uold(ind_grid(i)+iskip,ivar)=xx(i)*uold(ind_grid(i)+iskip,1)
                                !end do
                          end do
                       end do
                       deallocate(ind_grid,xx)
                    end if
                 end do
              end do
             close ( 12 )
end subroutine read_hydro
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine read_amr ( nomfich )
  use ramses_lite
  implicit none
  integer                          :: i , ibound , idim , ilevel , ind , iskip , ncache
  integer,dimension(:),allocatable ::ind_grid , iig , pos , grid
  real(dp),dimension(:),allocatable::xxg
  character(LEN=128)   :: nomfich 
       open ( unit = 11 , file=nomfich , status='old' , form='unformatted' )
              read ( 11 ) ncpu
              write(*,*) 'ncpu',ncpu
              read ( 11 ) ndim
              read ( 11 ) nx , ny , nz
              read ( 11 ) nlevelmax
              read ( 11 ) ngridmax
              read ( 11 ) nboundary
              read ( 11 ) ngrid_current
              read ( 11 ) boxlen
  do i=1,11 ; read ( 11 ) ; end do                      ! skip time variables
              read ( 11 ) headl(1:ncpu,1:nlevelmax)     ! Read levels variables
              read ( 11 ) taill(1:ncpu,1:nlevelmax)
              read ( 11 ) numbl(1:ncpu,1:nlevelmax)
              read ( 11 ) numbtot(1:10,1:nlevelmax)    
  if ( nboundary>0 ) then                               ! Read boundary linked list
              read ( 11 ) headb(1:nboundary,1:nlevelmax)
              read ( 11 ) tailb(1:nboundary,1:nlevelmax)
              read ( 11 ) numbb(1:nboundary,1:nlevelmax)
  endif
              read ( 11 ) headf,tailf,numbf,used_mem,used_mem_tot  ! free memory in ramses
              read ( 11 ) ordering                                 ! ordering    in ramses
              write(*,*) 'ordering',ordering
  if( TRIM(ordering).eq.'bisection' ) then
    do i=1,5 ;read ( 11 ) ; end do
  else
              write(*,*) 'got to here 1'
              read ( 11 )  bound_key(0:ncpu)        ! bound_key(0:ndomain)
  endif
              read ( 11 )     son(1:ncoarse)        ! Read coarse level
              read ( 11 )   flag1(1:ncoarse)
              read ( 11 ) cpu_map(1:ncoarse)
     do ilevel=1,nlevelmax                           ! Read fine levels
        do ibound=1,nboundary+ncpu
           if(ibound<=ncpu)then
              ncache=numbl(ibound,ilevel)
           else
              ncache=numbb(ibound-ncpu,ilevel)
           end if
           if(ncache>0)then
              allocate ( ind_grid(1:ncache) , xxg (1:ncache) , iig(1:ncache) )
              allocate ( pos     (1:ncache) , grid(1:ncache) )
              read ( 11 ) ind_grid                                   ! Read grid index
              read ( 11 ) iig                                        ! Read next index
              do i=1,ncache ; next(ind_grid(i))=iig(i) ; end do
              read ( 11 ) iig                                        ! Read prev index
              do i=1,ncache ; prev(ind_grid(i))=iig(i) ; end do              
              do idim=1,ndim                                         ! Read grid center
                 read ( 11 ) xxg
                 do i=1,ncache ; xg(ind_grid(i),idim) = xxg(i) ; end do
              end do
              read ( 11 ) iig                                        ! Read father index
              do i=1,ncache ; father(ind_grid(i)) = iig(i) ; end do
              do ind=1,twondim                                       ! Read nbor index
                 read ( 11 ) iig
                 do i=1,ncache ; nbor(ind_grid(i),ind) = iig(i) ; end do
              end do
              do ind=1,twotondim                                     ! Read son index
                 iskip = ncoarse+(ind-1)*ngridmax
                 read ( 11 ) iig
                 do i=1,ncache ; son(ind_grid(i)+iskip) = iig(i) ; end do
              end do
              do ind=1,twotondim                                     ! Read cpu map
                 iskip = ncoarse+(ind-1)*ngridmax
                 read ( 11 ) iig
                 do i=1,ncache ; cpu_map(ind_grid(i)+iskip) = iig(i) ; end do
              end do
              do ind=1,twotondim                                     ! Read refinement map
                 iskip = ncoarse+(ind-1)*ngridmax
                 read ( 11 ) iig
                 do i=1,ncache ; flag1(ind_grid(i)+iskip) = iig(i) ; end do
              end do
              deallocate ( xxg, iig, pos, grid, ind_grid )
           end if
        end do
     end do
             close ( 11 )
     headf       = ngrid_current + 1
     tailf       = ngridmax
     numbf       = ngridmax - ngrid_current
     prev(headf) = 0
     next(tailf) = 0
end subroutine read_amr
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine title ( n , nchar )
  implicit none
  integer     :: n
  character(len=5) :: nchar
  character(len=1) :: nchar1
  character(len=2) :: nchar2
  character(len=3) :: nchar3
  character(len=4) :: nchar4
  character(len=5) :: nchar5
      if ( n .ge. 10000) then ; write(nchar5,'(i5)')n ;  nchar = nchar5
  elseif ( n .ge.  1000) then ; write(nchar4,'(i4)')n ;  nchar = '0'//nchar4
  elseif ( n .ge.   100) then ; write(nchar3,'(i3)')n ;  nchar = '00'//nchar3
  elseif ( n .ge.    10) then ; write(nchar2,'(i2)')n ;  nchar = '000'//nchar2
  else                        ; write(nchar1,'(i1)')n ;  nchar = '0000'//nchar1
  endif
end subroutine title
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine alloc_ramses_lite 
  use ramses_lite
  implicit none
  integer  :: i , ncell
  ncell = ncoarse + twotondim*ngridmax
                                                                 ! Allocate linked list for each level
  allocate ( headl(    1:ncpu,1:nlevelmax) ) ; headl   = 0          ! Head grid in the level
  allocate ( taill(    1:ncpu,1:nlevelmax) ) ; taill   = 0          ! Tail grid in the level
  allocate ( numbl(    1:ncpu,1:nlevelmax) ) ; numbl   = 0          ! Number of grids in the level
  allocate ( numbtot(    1:10,1:nlevelmax) ) ; numbtot = 0          ! Total number of grids in the level
  allocate ( headb(1:MAXBOUND,1:nlevelmax) ) ; headb   = 0          ! Allocate physical boundary for each level
  allocate ( tailb(1:MAXBOUND,1:nlevelmax) ) ; tailb   = 0
  allocate ( numbb(1:MAXBOUND,1:nlevelmax) ) ; numbb   = 0
                                                                 ! Allocate AMR cell-based arrays
  allocate ( flag1      (0:ncell) )          ; flag1=0              ! Note: starting from 0
  allocate ( flag2      (0:ncell) )          ; flag2=0;             ! Note: starting from 0
  allocate ( son        (1:ncell) )          ; son=0                ! Son index
                                                                    ! Allocate MPI cell-based arrays
  allocate ( cpu_map    (1:ncell) )          ;  cpu_map=0;          ! Cpu map
  allocate ( cpu_map2   (1:ncell) )          ;  cpu_map2=0          ! New cpu map for load balance
  allocate ( hilbert_key(1:ncell) )          ;  hilbert_key=0.0d0   ! Ordering key
                                                                 ! Allocate grid center coordinates
  allocate ( xg(1:ngridmax,1:ndim) )         ;  xg=0.0D0
                                                                 ! Allocate tree arrays
  allocate ( father(1:ngridmax) )            ; father = 0
  allocate ( nbor  (1:ngridmax,1:twondim) )  ; nbor   = 0
  allocate ( next  (1:ngridmax) )            ; next   = 0  
  allocate ( prev  (1:ngridmax) )            ; prev   = 0
                                                                  ! Initialize AMR grid linked list
  do i=1,ngridmax-1 ; next(i)=i+1 ; end do
  do i=2,ngridmax   ; prev(i)=i-1 ; end do
  headf       = 1                                                 ! Pointer to first grid in free memory
  tailf       = ngridmax                                          ! Pointer to last grid in free memory
  prev(headf) = 0; next(tailf)=0
  numbf       = ngridmax                                          ! Number of grids in free memory
  used_mem    = ngridmax-numbf
                                                                  ! in hydro commons
  allocate ( uold(1:ncell,1:nvar+3) ) ; uold = 0.0d0
  allocate ( unew(1:ncell,1:nvar+3) ) ; unew = 0.0d0
end subroutine alloc_ramses_lite
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine reset_ramses_lite 
  use ramses_lite
  implicit none
  integer  :: i , ncell
   ncell   = ncoarse + twotondim*ngridmax
                   ! reset linked list for each level
   headl   = 0          ! Head grid in the level
   taill   = 0          ! Tail grid in the level
   numbl   = 0          ! Number of grids in the level
   numbtot = 0          ! Total number of grids in the level
   headb   = 0          ! Allocate physical boundary for each level
   tailb   = 0
   numbb   = 0 
                    ! reset AMR cell-based arrays
   flag1   = 0              ! Note: starting from 0
   flag2   = 0              ! Note: starting from 0
   son     = 0              ! Son index
                    ! reset grid center coordinates
   xg      = 0.0D0
                   ! reset tree arrays
   father  = 0
   nbor    = 0
   next    = 0                                          
   prev    = 0   
                                            ! Initialize AMR grid linked list
   do i=1,ngridmax-1 ; next(i)=i+1 ; end do
   do i=2,ngridmax   ; prev(i)=i-1 ; end do
   headf       = 1                            ! Pointer to first grid in free memory
   tailf       = ngridmax                     ! Pointer to last grid in free memory
   prev(headf) = 0; next(tailf)=0
   numbf       = ngridmax                     ! Number of grids in free memory
   used_mem    = ngridmax-numbf
                                           ! in hydro commons
   uold = 0.0d0
   unew = 0.0d0
end subroutine reset_ramses_lite                                        
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine dealloc_ramses_lite 
  use ramses_lite
  implicit none
  deallocate ( headl    , taill   , numbl , numbtot ) 
  deallocate ( headb    , tailb   , numbb ) 
  deallocate ( flag1    , flag2   , son )
  deallocate ( cpu_map  , cpu_map2  )
  deallocate ( hilbert_key )
  deallocate ( xg , father , nbor , next , prev )
                                                                  ! in hydro commons
  deallocate ( uold , unew ) 
end subroutine dealloc_ramses_lite
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
recursive  function QuickSort(InList) result(OutList)
    INTEGER,DIMENSION(:) :: InList
    INTEGER,DIMENSION(size(InList,1)) :: OutList
    INTEGER,DIMENSION(size(InList,1)) :: SupList, OrderedSupList, InfList, OrderedInfList
    INTEGER :: pivot
    INTEGER :: i,j, InfListSize, SupListSize
 
    ! S'il ne reste qu'un �l�ment dans la liste, on arr�te la r�cursion
    if(size(InList,1) < 2) then
       OutList(1) = Inlist(1)
    else
       ! Le pivot sera le premier �l�ment de la liste
       pivot = InList(1)
 
       ! On trie la liste
       InfListSize = 0
       SupListSize = 0
       do i = 2, size(InList,1)
          if(InList(i) < Pivot) then
             InfListSize = InfListSize + 1
             InfList(InfListSize) = InList(i)
          elseif(InList(i) >= Pivot) then
             SupListSize = SupListSize + 1
             SupList(SupListSize) = InList(i)
          end if
       enddo
 
       ! On recompose la liste
       if(InfListSize < 1) then
          OrderedSupList = QuickSort(SupList(1:SupListSize))
          OutList = (/ Pivot, (OrderedSupList(j), j=1,SupListSize) /)
       elseif(SupListSize < 1) then
          OrderedInfList = QuickSort(InfList(1:InfListSize))
          OutList = (/ (OrderedInfList(j), j=1,InfListSize), Pivot /)
       else
          OrderedInfList = QuickSort(InfList(1:InfListSize))
          OrderedSupList = QuickSort(SupList(1:SupListSize))
 
          OutList = (/ (OrderedInfList(j), j=1,InfListSize), Pivot, (OrderedSupList(j), j=1,SupListSize) /)
       endif
    end if
end function QuickSort
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine get3cubefather ( ind_cell_father , nbors_father_cells , &
     &                      nbors_father_grids , ncell , ilevel )

  use ramses_lite , only : nx,ny,nz, ncoarse , ndim     , ngridmax   ,&
                           nvector , twondim ,twotondim , threetondim 
  implicit none
  integer                                    :: ncell , ilevel
  integer,dimension(1:nvector)               :: ind_cell_father
  integer,dimension(1:nvector,1:threetondim) :: nbors_father_cells
  integer,dimension(1:nvector,1:twotondim)   :: nbors_father_grids
  !------------------------------------------------------------------
  ! This subroutine determines the 3^ndim neighboring father cells 
  ! of the input father cell. According to the refinement rule, 
  ! they should be present anytime.
  !------------------------------------------------------------------
  integer                                :: i    , j     , nxny    , i1    , j1    , k1    , ind , iok
  integer                                ::i1min , i1max , j1min   , j1max , k1min , k1max , ind_father
  integer,dimension(1:nvector),save      ::ix    , iy    , iz      , iix   , iiy   , iiz
  integer,dimension(1:nvector),save      ::pos   , ind_grid_father , ind_grid_ok
!ng  integer,dimension(1:nvector,1:threetondim),save::nbors_father_ok
!ng  integer,dimension(1:nvector,1:twotondim),save::nbors_grids_ok
  integer,dimension(1:nvector,1:27),save :: nbors_father_ok
  integer,dimension(1:nvector,1:8) ,save :: nbors_grids_ok
  logical::oups

  nxny=nx*ny

  if(ilevel==1)then  ! Easy...

     oups=.false.
     do i=1,ncell
        if(ind_cell_father(i)>ncoarse)oups = .true.
     end do
     if(oups)then
        write(*,*)'get3cubefather'
        write(*,*)'oupsssss !'
!        call clean_stop
         stop ' originally clean_stop'
     endif

     do i=1,ncell
        iz(i)=(ind_cell_father(i)-1)/nxny
     end do
     do i=1,ncell
        iy(i)=(ind_cell_father(i)-1-iz(i)*nxny)/nx
     end do
     do i=1,ncell
        ix(i)=(ind_cell_father(i)-1-iy(i)*nx-iz(i)*nxny)
     end do

     i1min=0  ;            i1max=0
               if(ndim > 0)i1max=2
     j1min=0  ;            j1max=0
               if(ndim > 1)j1max=2
     k1min=0  ;            k1max=0
               if(ndim > 2)k1max=2

     ! Loop over 3^ndim neighboring father cells
     do k1=k1min,k1max
        iiz=iz
        if(ndim > 2)then
           do i=1,ncell
              iiz(i)=iz(i)+k1-1
              if(iiz(i) < 0   )iiz(i)=nz-1
              if(iiz(i) > nz-1)iiz(i)=0
           end do
        end if
        do j1=j1min,j1max
           iiy=iy
           if(ndim > 1)then
              do i=1,ncell
                 iiy(i)=iy(i)+j1-1
                 if(iiy(i) < 0   )iiy(i)=ny-1
                 if(iiy(i) > ny-1)iiy(i)=0
              end do
           end if
           do i1=i1min,i1max
              iix=ix
              if(ndim > 0)then
                 do i=1,ncell
                    iix(i)=ix(i)+i1-1
                    if(iix(i) < 0   )iix(i)=nx-1
                    if(iix(i) > nx-1)iix(i)=0
                 end do
              end if
              ind_father=1+i1+3*j1+9*k1
              do i=1,ncell
                 nbors_father_cells(i,ind_father)=  1         &
                                                  + iix(i)    &
                                                  + iiy(i)*nx &
                                                  + iiz(i)*nxny
              end do
           end do
        end do
     end do

     i1min=0  ;            i1max=0
               if(ndim > 0)i1max=1
     j1min=0  ;            j1max=0
               if(ndim > 1)j1max=1
     k1min=0  ;            k1max=0
               if(ndim > 2)k1max=1

     ! Loop over 2^ndim neighboring father grids
     do k1=k1min,k1max
        iiz=iz
        if(ndim > 2)then
           do i=1,ncell
              iiz(i)=iz(i)+2*k1-1
              if(iiz(i) < 0   )iiz(i)=nz-1
              if(iiz(i) > nz-1)iiz(i)=0
           end do
        end if
        do j1=j1min,j1max
           iiy=iy
           if(ndim > 1)then
              do i=1,ncell
                 iiy(i)=iy(i)+2*j1-1
                 if(iiy(i) < 0   )iiy(i)=ny-1
                 if(iiy(i) > ny-1)iiy(i)=0
              end do
           end if
           do i1=i1min,i1max
              iix=ix
              if(ndim > 0)then
                 do i=1,ncell
                    iix(i)=ix(i)+2*i1-1
                    if(iix(i) < 0   )iix(i)=nx-1
                    if(iix(i) > nx-1)iix(i)=0
                 end do
              end if
              ind_father=1+i1+2*j1+4*k1
              do i=1,ncell
                 nbors_father_grids(i,ind_father)=   1                &
                                                  + (iix(i)/2)        &
                                                  + (iiy(i)/2)*(nx/2) &
                                                  + (iiz(i)/2)*(nxny/4)
              end do
           end do
        end do
     end do

  else    ! else, more complicated...
     
     ! Get father cell position in the grid
     do i=1,ncell
        pos(i)=(ind_cell_father(i)-ncoarse-1)/ngridmax+1
     end do
     ! Get father grid
     do i=1,ncell
        ind_grid_father(i)=ind_cell_father(i)-ncoarse-(pos(i)-1)*ngridmax
     end do

     ! Loop over position
     do ind=1,twotondim

        ! Select father cells that sit at position ind
        iok=0
        do i=1,ncell
           if(pos(i)==ind)then
              iok=iok+1
              ind_grid_ok(iok)=ind_grid_father(i)
           end if
        end do

        if(iok>0)&
        & call get3cubepos ( ind_grid_ok , ind , nbors_father_ok ,&
                                                 nbors_grids_ok  , iok )

        ! Store neighboring father cells for selected cells
        do j=1,threetondim
           iok=0
           do i=1,ncell
              if(pos(i)==ind)then
                 iok=iok+1
                 nbors_father_cells(i,j)=nbors_father_ok(iok,j)
              end if
           end do
        end do

        ! Store neighboring father grids for selected cells
        do j=1,twotondim
           iok=0
           do i=1,ncell
              if(pos(i)==ind)then
                 iok=iok+1
                 nbors_father_grids(i,j)=nbors_grids_ok(iok,j)
              end if
           end do
        end do

     end do

  end if

end subroutine get3cubefather
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine get3cubepos ( ind_grid , ind , nbors_father_cells , nbors_father_grids , ng )

  use ramses_lite , only : nx,ny,nz, ncoarse , ndim      , ngridmax    ,&
                           nvector , twondim , twotondim , threetondim , son , nbor
  implicit none
  integer::ng,ind
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector,1:threetondim)::nbors_father_cells
  integer,dimension(1:nvector,1:twotondim)::nbors_father_grids
  !--------------------------------------------------------------------
  ! This subroutine determines the 3^ndim neighboring father cells 
  ! of the input cell at position ind in grid ind_grid. According to 
  ! the refinements rules and since the input cell is refined, 
  ! they should be present anytime.
  !--------------------------------------------------------------------
  integer                              :: i     , j     , iskip
  integer                              :: ii    , iimin , iimax
  integer                              :: jj    , jjmin , jjmax
  integer                              :: kk    , kkmin , kkmax
  integer                              :: icell , igrid , inbor
  integer,dimension(1:8)               :: iii=(/1,2,1,2,1,2,1,2/)
  integer,dimension(1:8)               :: jjj=(/3,3,4,4,3,3,4,4/)
  integer,dimension(1:8)               :: kkk=(/5,5,5,5,6,6,6,6/)
  integer,dimension(1:27,1:8,1:3)      :: lll , mmm
  integer,dimension(1:nvector),save    :: ind_grid1 , ind_grid2 , ind_grid3
!integer,dimension(1:nvector,1:twotondim),save::nbors_grids
  integer,dimension(1:nvector,1:8),save:: nbors_grids
  
  call getindices3cube(lll,mmm)

  iimin=0  ;  iimax=0 ;  if (ndim>0) iimax=1
  jjmin=0  ;  jjmax=0 ;  if (ndim>1) jjmax=1
  kkmin=0  ;  kkmax=0 ;  if (ndim>2) kkmax=1

  do kk=kkmin,kkmax
     do i=1,ng
        ind_grid1(i) = ind_grid(i)
     end do
     if(kk>0)then
        inbor = kkk(ind)
        do i=1,ng
           ind_grid1(i) = son(nbor(ind_grid(i),inbor))
        end do
     end if

     do jj=jjmin,jjmax
        do i=1,ng
           ind_grid2(i) = ind_grid1(i)
        end do
        if(jj>0)then
           inbor = jjj(ind)
           do i=1,ng
              ind_grid2(i) = son(nbor(ind_grid1(i),inbor))
           end do
        end if
 
        do ii=iimin,iimax
           do i=1,ng
              ind_grid3(i) = ind_grid2(i)
           end do
           if(ii>0)then
              inbor = iii(ind)
              do i=1,ng
                 ind_grid3(i) = son(nbor(ind_grid2(i),inbor))
              end do
           end if

           inbor = 1 + ii + 2*jj + 4*kk
           do i=1,ng
              nbors_grids(i,inbor) = ind_grid3(i)
           end do     

        end do
     end do
  end do     

  do j=1,twotondim
     do i=1,ng
        nbors_father_grids(i,j) = nbors_grids(i,j)
     end do
  end do

  do j=1,threetondim
     igrid = lll(j,ind,ndim)
     icell = mmm(j,ind,ndim)
     iskip = ncoarse + (icell-1)*ngridmax
     do i=1,ng
        nbors_father_cells(i,j) = iskip + nbors_grids(i,igrid)
     end do
  end do

end subroutine get3cubepos
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
subroutine getindices3cube(lll,mmm)
  implicit none
  integer,dimension(1:27,1:8,1:3)::lll,mmm
  
  lll=0; mmm=0
  ! -> ndim=1
  ! @ind =1
  lll(1:3,1,1)=(/2,1,1/)
  mmm(1:3,1,1)=(/2,1,2/)
  ! @ind =2
  lll(1:3,2,1)=(/1,1,2/)
  mmm(1:3,2,1)=(/1,2,1/)

  ! -> ndim=2
  ! @ind =1
  lll(1:9,1,2)=(/4,3,3,2,1,1,2,1,1/)
  mmm(1:9,1,2)=(/4,3,4,2,1,2,4,3,4/)
  ! @ind =2
  lll(1:9,2,2)=(/3,3,4,1,1,2,1,1,2/)
  mmm(1:9,2,2)=(/3,4,3,1,2,1,3,4,3/)
  ! @ind =3
  lll(1:9,3,2)=(/2,1,1,2,1,1,4,3,3/)
  mmm(1:9,3,2)=(/2,1,2,4,3,4,2,1,2/)
  ! @ind =4
  lll(1:9,4,2)=(/1,1,2,1,1,2,3,3,4/)
  mmm(1:9,4,2)=(/1,2,1,3,4,3,1,2,1/)

  ! -> ndim= 3
  ! @ind = 1
  lll(1:27,1,3)=(/8,7,7,6,5,5,6,5,5,4,3,3,2,1,1,2,1,1,4,3,3,2,1,1,2,1,1/)
  mmm(1:27,1,3)=(/8,7,8,6,5,6,8,7,8,4,3,4,2,1,2,4,3,4,8,7,8,6,5,6,8,7,8/)
  ! @ind = 2
  lll(1:27,2,3)=(/7,7,8,5,5,6,5,5,6,3,3,4,1,1,2,1,1,2,3,3,4,1,1,2,1,1,2/)
  mmm(1:27,2,3)=(/7,8,7,5,6,5,7,8,7,3,4,3,1,2,1,3,4,3,7,8,7,5,6,5,7,8,7/)
  ! @ind = 3
  lll(1:27,3,3)=(/6,5,5,6,5,5,8,7,7,2,1,1,2,1,1,4,3,3,2,1,1,2,1,1,4,3,3/)
  mmm(1:27,3,3)=(/6,5,6,8,7,8,6,5,6,2,1,2,4,3,4,2,1,2,6,5,6,8,7,8,6,5,6/)
  ! @ind = 4
  lll(1:27,4,3)=(/5,5,6,5,5,6,7,7,8,1,1,2,1,1,2,3,3,4,1,1,2,1,1,2,3,3,4/)
  mmm(1:27,4,3)=(/5,6,5,7,8,7,5,6,5,1,2,1,3,4,3,1,2,1,5,6,5,7,8,7,5,6,5/)
  ! @ind = 5
  lll(1:27,5,3)=(/4,3,3,2,1,1,2,1,1,4,3,3,2,1,1,2,1,1,8,7,7,6,5,5,6,5,5/)
  mmm(1:27,5,3)=(/4,3,4,2,1,2,4,3,4,8,7,8,6,5,6,8,7,8,4,3,4,2,1,2,4,3,4/)
  ! @ind = 6
  lll(1:27,6,3)=(/3,3,4,1,1,2,1,1,2,3,3,4,1,1,2,1,1,2,7,7,8,5,5,6,5,5,6/)
  mmm(1:27,6,3)=(/3,4,3,1,2,1,3,4,3,7,8,7,5,6,5,7,8,7,3,4,3,1,2,1,3,4,3/)
  ! @ind = 7
  lll(1:27,7,3)=(/2,1,1,2,1,1,4,3,3,2,1,1,2,1,1,4,3,3,6,5,5,6,5,5,8,7,7/)
  mmm(1:27,7,3)=(/2,1,2,4,3,4,2,1,2,6,5,6,8,7,8,6,5,6,2,1,2,4,3,4,2,1,2/)
  ! @ind = 8
  lll(1:27,8,3)=(/1,1,2,1,1,2,3,3,4,1,1,2,1,1,2,3,3,4,5,5,6,5,5,6,7,7,8/)
  mmm(1:27,8,3)=(/1,2,1,3,4,3,1,2,1,5,6,5,7,8,7,5,6,5,1,2,1,3,4,3,1,2,1/)

end subroutine getindices3cube
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
