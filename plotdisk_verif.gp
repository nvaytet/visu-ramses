reset
set view map
#set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0.2 "white", 0.45 "orange", 1 "red", 1.1 "black")
load "< awk '/t/ {print $0}' br.txt"


load "< awk '/hmax/ {print $0}' plandisk_plan.dat"
load "< awk '/rmax/ {print $0}' plandisk_plan.dat"

#set ytics ('-100' -100, '-50' -50, '0' 0,'50' 50, '100' 100) offset screen 0.01,0. 
#set xtics ('-100' -100, '-50' -50, '0' 0,'50' 50, '100' 100) offset screen 0.0,0.01

#set ytics ('-60' -60, '-40' -40, '-20' -20, '0' 0, '20' 20, '40' 40, '60' 60) offset screen 0.01,0. 
#set xtics ('0' 0, '20' 20, '40' 40, '60' 60, '80' 80, '100' 100, '130' 130) offset screen 0.0,0.01 
#set ytics ('-1000' -1000,'-500' -500,'-250' -250,'-100' -100,'0' 0,'100' 100,'250' 250, '500' 500, '1000' 1000) offset screen 0.01,0. 
#set xtics ('0' 0, '250' 250, '500' 500, '750' 750,  '1000' 1000, '1300' 1300) offset screen 0.0,0.01 
set ytics ('-120' -120, '-80' -80, '-40' -40, '0' 0, '40' 40, '80' 80, '120' 120) offset screen 0.01,0. 
set xtics ('0' 0, '50' 50, '100' 100, '150' 150, '200' 200) offset screen 0.0,0.01 

set cbtics in
#set cbtics ('-14' 1.e-14,'-13' 1.e-13,'-12' 1.e-12)
#set cbtics offset screen -0.081,screen 0. textcolor rgb "blue" 
set cblabel 'log({/Symbol r}) (g.cm^{-3})' rotate by 90 offset screen 0.0, screen 0.1
set xlabel 'R (a.u.)'offset screen 0.0, 0.02 
set ylabel 'h (a.u.)'offset screen 0.02, 0.0 

set cbtics offset screen -0.0,screen 0.005
range=1.5*rmax

maxv=4

#range=160 # fixe le rayon max

set yrange [-range/2:range/2]
set xrange [0:range]
set palette maxcolors 100
#set label front "time = %4.3f",t," Kyears" at screen 0.58,0.11 # 0.10


#set term png enh size 1800,1500 19
set term post enh color 16 portrait
set output 'plandisk.ps'

set multiplot
set grid noxtics noytics noztics front


set size ratio 1
set lmargin at screen 0.05
#set tmargin at screen 0.95
set bmargin at screen 0.355
set rmargin at screen 0.50

#set origin 0,0
#set size 1,1
set logscale cb
set cbrange [5.e-15:1.e-11]
set format cb "{%L}"
#set colorbox vert user origin 0.58,0.20 size 0.06,0.23
set colorbox horiz user origin 0.55,0.85 size 0.3,0.02
splot 'plandisk_plan.dat' index 1 u 2:1:(($4/$3)) w image not

set cbrange [0:*]
#set origin 0,0
#set size 1,1
set contour
set cntrparam levels incremental -20,1,-6
unset surface
unset clabel
splot 'plandisk_plan.dat' index 1 u 2:1:(log10(abs($4/$3))) w l lt 1 lc 7 lw 2 not

set surface
unset contour

set cbtics in
set cbtics ('0' 0, '1' 1,'2' 2,'3' 3,'4' 4)
set cbtics offset screen -0.0,screen 0.005
set cblabel "velocity (km.s^{-1})" rotate by 90 offset screen -0.,screen 0.1
set xlabel ''
set ylabel ''
set format x ""
set format y ""
set format cb "%1.0f"
set origin 0,0
set size 1,1
set palette model RGB defined (0. "white", 1.0 "black",1.01 "blue", 4. "green")
set colorbox horiz user origin 0.1,0.85 size 0.3,0.02
unset logscale cb
norm=1.0 # range/60
norm=range/25
set cbrange [0:maxv]

set label 'a' at graph 0.07, graph 0.91 front textcolor rgb "black" font ",35"

splot 'plandisk_plan.dat' every 10:10 index 1 u 2:1:($7/$3/1000.):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not








reset

range2=range

set yrange [-range2:range2]
set xrange [-range2:range2]

set palette model RGB defined (0.2 "white", 0.45 "orange", 1 "red", 1.1 "black")
set palette maxcolors 100

set size ratio 1
set view map

set xlabel 'R (a.u.)' offset screen 0.0, 0.02 

#set tmargin at screen 0.95
set bmargin at screen 0.355

set lmargin at screen 0.50
#set rmargin at screen 0.99

set grid noxtics noy2tics noytics noztics front

unset colorbox
#unset ytics
#set y2tics
#set y2tics mirror
#set ytics ('' 0,'' 50)
#set ytics ('-100' -100, '-50' -50, '0' 0,'50' 50, '100' 100) offset screen 0.48,0. left
#set xtics ('-100' -100, '-50' -50, '0' 0,'50' 50, '100' 100) offset screen 0.0,0.01

#set ytics ('-60' -60, '-40' -40, '-20' -20, '0' 0, '20' 20, '40' 40, '60' 60) offset screen 0.48,0. left
#set xtics ('-20' -20, '0' 0, '20' 20, '40' 40) offset screen 0.0,0.01 
set xtics ('-1000' -1000,'-500' -500,'0' 0, '500' 500, '1000' 1000) offset screen 0.0,0.01 
set ytics ('-1000' -1000,'-500' -500,'0' 0,  '500' 500, '1000' 1000) offset screen 0.48,0. left
# AMBI_MU2_B01
set xtics ('-200' -200,'-100' -100,'0' 0, '100' 100, '200' 200) offset screen 0.0,0.01 
set ytics ('-200' -200,'-100' -100,'0' 0,  '100' 100, '200' 200) offset screen 0.48,0. left

set format y ""

#set origin 0,0
#set size ratio 1
set logscale cb
set cbrange [5.e-15:1.e-11]
set format cb "{%L}"
splot 'plandisk_ortho.dat' index 1 u 2:1:(($4/$3)) w image not

set cbrange [0:*]
set contour
set cntrparam levels incremental -20,1,-6
unset surface
unset clabel
splot 'plandisk_ortho.dat' index 1 u 2:1:(log10(abs($4/$3))) w l lt 1 lc 7 lw 2 not

set surface
unset contour

set xlabel ''
set ylabel ''
set format x ""
set format y ""
set format cb "%1.1f"
set origin 0,0
set size 1,1
set palette model RGB defined (0. "white", 1.0 "black",1.01 "blue", 4. "green")
unset logscale cb
set cbrange [0:maxv]
norm=1.0 # range/60
norm=range/10.


set label 'b' at graph 0.07, graph 0.91 front textcolor rgb "black" font ",35"

splot 'plandisk_ortho.dat' every 5:5 index 1 u 2:1:($7/$3/1000.):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not

unset multiplot

