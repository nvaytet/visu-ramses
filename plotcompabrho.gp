reset
#set pm3d
set view map
#set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (-1 "blue", -0.2 "green", 0 "white", 0.3 "orange", 0.5 "red", 1 "violet")

load "< awk '/t/ {print $0}' brho1.txt"
set label front "time = %4.3f",t," Kyears" at 1.e-17,1.
load "< awk '/t/ {print $0}' brho2.txt"
set label front "time = %4.3f",t," Kyears" at 1.e-17,0.8
set xlabel '{/Symbol r} (g.cc^{-1})'
set ylabel 'B (G)'
set xrange [-18:-9]
set yrange [-5:1]
#set cbrange [-150:150]
set cbrange [-2:2]
#set cblabel 'B_{AD}/B_{MHD}' rotate by -90
set cblabel 'B_{2}/B_{1}' rotate by -90

#set term png enh size 1800,1500 19
#set term gif enh size 1800,1500 19
set term post enh color 19
set output 'comp_brho.ps'
#splot 'brho.txt' w pm3d not
splot 'rescompabrho.txt' w p pt 7 ps 0.12 palette not
unset label
#set term wxt

