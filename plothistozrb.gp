reset
set view map
set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0 "white", 0.35 "orange", 0.65 "violet", 1 "red")

#set size 0.5,1


load "< awk '/t/ {print $0}' br.txt"
#set label front "time = %4.3f",t," Kyears" at 7,40
set title sprintf("Time = %4.3f Kyrs",t)
set cblabel 'B (G)'
set xlabel 'R (a.u.)'
set ylabel 'h (a.u.)'
#set yrange [-50:50]
#set xrange [0:50]
set cbrange [*:50]
set palette maxcolors 100


#set term png enh size 1800,1500 19
set term post enh color 17
set output 'zrb.ps'
set title '||B||'
#set cbrange [0:0.2]
#splot 'zrb.txt' index 1 u 2:1:($4/$3) w p pt 5 ps 0.6 palette not
splot 'zrb.txt' index 1 u 2:1:($4/$3) w image not
set output 'zrb_z.ps'
#set cbrange [0:0.1]
set title 'B_z'
#splot 'zrb.txt' index 1 u 2:1:($5/$3) w p pt 5 ps 0.6 palette not
splot 'zrb.txt' index 1 u 2:1:($5/$3) w image not
set output 'zrb_r.ps'
set title 'B_r'
#set cbrange [0:0.05]
#splot 'zrb.txt' index 1 u 2:1:($6/$3) w p pt 5 ps 0.6 palette not
splot 'zrb.txt' index 1 u 2:1:($6/$3) w image not
set output 'zrb_t.ps'
#set cbrange [0:0.1]
set title 'B_{/Symbol q}'
#splot 'zrb.txt' index 1 u 2:1:($7/$3) w p pt 5 ps 0.6 palette not
splot 'zrb.txt' index 1 u 2:1:($7/$3) w image not
unset label
#set term wxt



