#!/bin/bash
i=0

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

mkdir movies
#mkdir movies/plandiskB_fc
#mkdir movies/plandiskB_sc
dirmovie="movies/plandiskB_bt"
mkdir $dirmovie
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dir=.
for file in $dir/output_00730
   #for file in $dir/output_00009
   #file=$dir/output_00018
do
#      echo $file
#   done
   z="$(echo $file | rev |cut -d'_' -f1 | rev)"
   #echo $z
#   if [ -f $dirmovie'/plandiskB_'$z'.ps' ]
#   then
#      echo file $file already processed
#   else
      echo processing file $file ....
      let i=$i+1
      y="$(printf "%03d" $i)"
      filee="$(echo $file | rev |cut -d'/' -f1 | rev)"
      echo "0.5 0.5 0.5" > center.dat
      echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 3.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
      if [ $z -ne 00001 ]
      then
          ${VISUDIR}/findcenter_sink
      fi
      ${VISUDIR}/angmom
      #   echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 13.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > br.dat
      #   /gpfs/data1/jmasson/analyse_folder/plandiskB
      ##   echo "load '/gpfs/data1/jmasson/analyse_folder/plothistodiskbr.gp'" | gnuplot
      #   mv "plandisk_${z}.txt" plandisk.dat
      #   echo "load '/gpfs/data1/jmasson/analyse_folder/plotplandiskB.gp'" | gnuplot
      #   mv plandiskB.ps movies/plandiskB_sc/'plandiskB_'$z'.ps'
      #   echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 8.5\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > br.dat
      #   /gpfs/data1/jmasson/analyse_folder/plandiskB
      #   echo "load '/gpfs/data1/jmasson/analyse_folder/plothistodiskbr.gp'" | gnuplot
      #   mv "plandisk_${z}.txt" plandisk.dat
      #   echo "load '/gpfs/data1/jmasson/analyse_folder/plotplandiskB.gp'" | gnuplot
      #   mv plandiskB.ps movies/plandiskB_fc/'plandiskB_'$z'.ps'
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 7.9\ncenter 0.5 0.5 0.5\nnorm 500\nold 1' > br.dat
      ${VISUDIR}/plandisqueB_bt
#      /gpfs/data1/jmasson/analyse_folder/plandiskBdetailsetbt
      #   echo "load '/gpfs/data1/jmasson/analyse_folder/plothistodiskbr.gp'" | gnuplot
      mv "plandisk_${z}.txt" plandisk.dat
      gnuplot ${VISUDIR}/plotplandiskB_bt.gp;
      mv plandiskB.ps $dirmovie'/plandiskB_bt'$z'.ps'
#      rm -rf plandisk.dat angmom.dat 'angmom_'$z'.txt' angmom.res 
#      rm -rf angmom.dat 'angmom_'$z'.txt' angmom.res 
#   fi
#   gs $dirmovie'/plandiskB_'$z'.ps'
done
#rm brho.txt
#rm brho.dat





