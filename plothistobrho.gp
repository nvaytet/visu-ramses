set view map
set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0 "white", 0.25 "orange", 0.55 "red", 1 "violet")

load "< awk '/t/ {print $0}' brho.txt"
#set label front "time = %4.3f",t," Kyears" at -17,2.1
set title sprintf("Time = %4.3f Kyrs",t)
set xlabel 'log({/Symbol r}) (g.cc^{-1})'
set ylabel 'log(B) (G)'
# set yrange [-6:5]
# set xrange [-19:-2]
set cbrange [1:100]
set palette maxcolors 100

#set term png enh size 1800,1500 19
set term post enh color 17
#set output 'brho.png'
set output 'brho.ps'
#splot 'brho.txt' index 1 u 1:2:3 w pm3d not
splot 'brho.txt' index 1 u 1:2:3 w p pt 7 ps 0.12 palette not
unset label
#set term wxt

