set view map
set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0 "white", 0.25 "orange", 0.55 "red", 1 "violet")

load "< awk '/t/ {print $0}' br.txt"
#set label front "time = %4.3f",t," Kyears" at -17,2.1
set title sprintf("Time = %4.3f Kyrs",t)
set ylabel 'log(B) (G)'
set xlabel 'log(R) (a.u.)'
set xrange [0:3]
set yrange [-3:1]
set cbrange [1:100]
set palette maxcolors 100


#set term png enh size 1800,1500 19
set term post enh color 17
#set output 'br.png'
set output 'br.ps'
#splot 'br.txt' index 1 u 1:2:3 w pm3d not
splot 'br.txt' index 1 u 1:2:3 w p pt 7 ps 0.22 palette not
unset label
#set term wxt

