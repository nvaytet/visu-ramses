set logscale y
set xlabel 'Time (kyears)'
set ylabel 'Outflow Mass'
set term post enh color 17
set output 'moutflow.ps'
plot 'toutflow.txt' u 1:2 w l lw 3 tit 'Mass of outflow'
