#!/bin/bash
i=1

mkdir movies
#mkdir movies/plandisk_sc
dirmovie="movies/disk_verif"
mkdir $dirmovie
#cp ../sc_analyse/plotplandisk_* .
#dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dir=.

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

for file in $dir/output_*
   #for file in $dir/output_00065
   #file=$dir/output_00018
do
#      echo $file
#   done
   z="$(echo $file | rev |cut -d'_' -f1 | rev)"
   filee="$(echo $file | rev |cut -d'/' -f1 | rev)"
   if [ -f $dirmovie'/disk_'$z'.ps' ]
   then
      echo file $file already processed
   else
      echo processing file $file ....
      #   let i=$i+1
      #   y="$(printf "%03d" $i)"
      echo "0.5 0.5 0.5" > center.dat
      echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 3.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
      if [ $z -ne 00001 ]
      then
         ${VISUDIR}/findcenter
      fi
      ${VISUDIR}/angmom
      #   echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 13.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > br.dat
      #   ./plandisk
      ##   echo "load 'plothistodiskbr.gp'" | gnuplot
      #   mv "plandisk_${z}.txt" plandisk.dat
      #   echo "load 'plotplandisk_sc.gp'" | gnuplot
      #   mv plandisk.ps movies/plandisk_sc/'plandisk_'$z'.ps'
      #   echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 8.5\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > br.dat
      #   ./plandisk
      ##   echo "load 'plothistodiskbr.gp'" | gnuplot
      #   mv "plandisk_${z}.txt" plandisk.dat
      #   echo "load 'plotplandisk_fc.gp'" | gnuplot
      #   mv plandisk.ps movies/plandisk_fc/'plandisk_'$z'.ps'
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 6.5\ncenter 0.5 0.5 0.5\nnorm 600\nold 1' > br.dat
      ${VISUDIR}/plandiskverif
      mv "plandisk_${z}.txt" plandisk_plan.dat
#      echo "load '/home/bcommercon/visu_class0/analyse_folder/plotplandisk_verif.gp'" | gnuplot
      #   echo "load 'plotplandisk_out.gp'" | gnuplot
      #   mv plandisk.ps movies/plandisk_out/'plandisk_'$z'.ps'
#      mv plandisk.ps $dirmovie/'plandisk_'$z'.ps'
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 7.1\ncenter 0.5 0.5 0.5\nnorm 20\nold 1' > br.dat
      ${VISUDIR}/planorthoverif
      #   echo "load '/home/bcommercon/visu_class0/analyse_folder/plothistodiskbr.gp'" | gnuplot
      mv "plandisk_${z}.txt" plandisk_ortho.dat
#      echo "load '/home/bcommercon/visu_class0/analyse_folder/plotdisk_verif.gp'" | gnuplot
      gnuplot ${VISUDIR}/plotdisk_verif.gp;
      mv plandisk.ps $dirmovie'/disk_'$z'.ps'
#      rm -rf plandisk.dat angmom.dat 'angmom_'$z'.txt' angmom.res 
   fi
#gs $dirmovie/'plandisk_'$z'.ps'
done
#rm brho.txt
#rm brho.dat

