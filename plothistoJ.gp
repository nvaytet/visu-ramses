reset
set size ratio 2
set view map
#set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0.2 "white", 0.45 "orange", 1 "red")
load "< awk '/t/ {print $0}' J_F.txt"


load "< awk '/hmax/ {print $0}' djdt.dat"




set cblabel 'log(|J_z|)' rotate by 0 offset screen -0.05, screen -0.2 # -0.25
set xlabel 'R (a.u.)'
set ylabel 'h (a.u.)'

# For fenetre 3, range = 150, for 0.1Msun
range=hmax

set yrange [-range:range]
set xrange [0:range]
set palette maxcolors 100
set label front "time = %4.3f",t," Kyears" at screen 0.58,0.11 # 0.1


#set term png enh size 1800,1500 19
set term post enh color 17 portrait
set output 'zrJ.ps'



set multiplot
set origin 0,0
set size 1,1
set logscale cb
#set cbrange [1.e3:*]
set format cb "{%L}"
set colorbox vert user origin 0.76,0.22 size 0.02,0.37
splot 'djdt.dat' index 1 u 2:1:(abs($4/$3)) w image not

set origin 0,0
set size 1,1
set contour
set cntrparam levels incremental 8,2,30
unset surface
unset clabel
splot 'djdt.dat' index 1 u 2:1:(log(abs($4/$3))) w l lt 1 lc 7 lw 2 not

set surface
unset contour

set cbtics out
set cbtics offset 0,screen 0.08 #  0.1
set cblabel "Angular momentum flux for the Lorentz force (log)" offset screen 0.,screen 0.05 # 0.07
set xlabel ''
set ylabel ''
set format x ""
set format y ""
set origin 0,0
set size 1,1
set palette model RGB defined (0 "violet", 0.2 "blue", 0.6 "black")
set palette model RGB defined (0.2 "blue", 0.6 "green")
set colorbox horiz user origin 0.26,0.9 size 0.6,0.02 

#set cbrange [1.e5:1.e8]
norm=range/15
splot 'djdt.dat' every 10:10 index 1 u 2:1:($7/$3):($5/$7*norm):($6/$7*norm):(0) w vectors palette not

set cbrange [*:*]

set cbtics in
set cbtics nooffset 
set cblabel 'log(-J_z)' rotate by 0 offset screen -0.04,screen -0.11 # 0.07
#set origin 0.46,0.523
set origin 0.623,0.523
set size 0.4,0.4
set format x ""
set format y ""
set cbrange [1.e3:*]
set palette model RGB defined (0.2 "white", 0.45 "blue", 1 "black")
set colorbox vert user origin 0.86,0.44 size 0.015,0.15 

splot 'djdt.dat' index 1 u 2:1:(-$4/$3) w image not

unset multiplot

