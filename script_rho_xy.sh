#!/bin/bash

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

function processdata {

   file=$1;
   lmax=$2;
   fenet=$3;
   z="$(echo $file | cut -d'_' -f2)";
   rm center.dat;
   echo "0.5 0.5 0.5" > center.dat;
   echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 1''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax ' $lmax '\nfile vtk\ntype 17\nfenetre' $fenet '\ncenter 0.5 0.5 0.5\nnorm 500000\nold 0' > input.dat;
#    if [ $z -ne 00001 ] && [ $fenet -ne 1 ]; then
#        echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat;
#        ${VISUDIR}/findcenter;
#    fi
   ${VISUDIR}/amrleg;
   gnuplot ${VISUDIR}/plotv.gp;
   mv rhov.ps movies/rhov_xy/'rhov_'$z'_xy.ps';

};

###############################################################

last=false;
output_selected=false;
pattern_specified=false;
lmax=10;
fenet=3.0;
while getopts "el:f:o:p:" OPTION; do
   case $OPTION in
      e)
         last=true;
      ;;
      f)
         fenet=$OPTARG;
      ;;
      l)
         lmax=$OPTARG;
      ;;
      o)
         output_selected=true;
         nout=$OPTARG;
      ;;
      p)
         pattern_specified=true;
         pattern=$OPTARG;
      ;;
   esac
done

mkdir -p movies;
mkdir -p movies/rhov_xy;
dir=.

if $last ; then

    for file in $dir/output*; do
        echo $file;
    done
    processdata $file $lmax $fenet;

elif $output_selected ; then

    if [ $nout -lt 10 ]; then
        file="${dir}/output_0000${nout}";
    elif [ $nout -lt 100 ]; then
        file="${dir}/output_000${nout}";
    elif [ $nout -lt 1000 ]; then
        file="${dir}/output_00${nout}";
    elif [ $nout -lt 10000 ]; then
        file="${dir}/output_0${nout}";
    else
        file="${dir}/output_${nout}";
    fi
    processdata $file $lmax $fenet;

elif $pattern_specified ; then

    for file in $dir/output_$pattern ; do
        echo "file is" $file;
        processdata $file $lmax $fenet;
    done

else

    for file in $dir/output_* ; do
        echo $file;
        processdata $file $lmax $fenet;
    done

fi
