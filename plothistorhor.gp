set view map
set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0 "white", 0.25 "orange", 0.55 "red", 1 "violet")

load "< awk '/t/ {print $0}' rhor.txt"
#set label front "time = %4.3f",t," Kyears" at 0,-4
set title sprintf("Time = %4.3f Kyrs",t)
set ylabel 'log({/Symbol r}) (g.cc^{-1})'
set xlabel 'log(R) (a.u.)'
set xrange [-3:4]
set yrange [-19:-2]
set cbrange [1:500]
set palette maxcolors 100

#set term png enh size 1800,1500 19
set term post enh color 17
#set output 'rhor.png'
set output 'rhor.ps'
splot 'rhor.txt' index 1 u 2:1:3 w p pt 7 ps 0.12 palette not
unset label
#set term wxt

