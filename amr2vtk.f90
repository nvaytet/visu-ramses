!-----------------------------------------------------------------------------
! Modified amr2cell program to create 3D VTK files from RAMSES binary outputs.
! Neil Vaytet (ENS Lyon) - 09/2014
! You can use MPI, compiling with mpif90 -DMPI -cpp -o amr2vtk amr2vtk.f90.
! This will have each thread process one seperate cpu output.
! Usage: ./amr2vtk -inp output_00001
! The vtu and pvtu files are stored inside the output directory.
!-----------------------------------------------------------------------------
program amr2vtk

  implicit none
  
#ifdef MPI
  include "mpif.h"
  integer :: ierror,world
#endif
  integer :: nproc,rank

  integer, parameter :: dp = 8
  integer::ndim,n,i,j,k,twotondim,ncoarse,type=0,domax=0
  integer::ivar,nvar,ncpu,ncpuh,lmax=0,nboundary,ngrid_current
  integer::nx,ny,nz,ilevel,idim,jdim,kdim,icell
  integer::nlevelmax,ilevel1,ngrid1
  integer::nlevelmaxs,nlevel,iout
  integer::ind,ipos,ngrida,ngridh,ilevela,ilevelh
  integer::ngridmax,nstep_coarse,icpu,ncpu_read
  integer::nhx,nhy,ihx,ihy,ivar1,ivar2
  real(dp)::gamma,smallr,smallc,gammah
  real(dp)::boxlen,boxlen2
  real(dp)::t,aexp,hexp,t2,aexp2,hexp2
  real(dp)::omega_m,omega_l,omega_k,omega_b
  real(dp)::scale_l,scale_d,scale_t
  real(dp)::omega_m2,omega_l2,omega_k2,omega_b2

  integer::nx_sample=0,ny_sample=0,nz_sample=0
  integer::ngrid,imin,imax,jmin,jmax,kmin,kmax
  integer::ncpu2,npart2,ndim2,nlevelmax2,nstep_coarse2
  integer::nx2,ny2,nz2,ngridmax2,nvarh,ndimh,nlevelmaxh
  integer::nx_full,ny_full,nz_full,lmin,levelmin
  integer::ix,iy,iz,ndom,impi,bit_length,maxdom
  integer,dimension(1:8)::idom,jdom,kdom,cpu_min,cpu_max
  real(dp),dimension(1:8)::bounding_min,bounding_max
  real(dp)::dkey,dmax,dummy
  real(dp), dimension(1) :: order_min
  real(dp)::xmin=0,xmax=1,ymin=0,ymax=1,zmin=0,zmax=1
  real(dp)::xxmin,xxmax,yymin,yymax,zzmin,zzmax,dx,dx2
  real(dp),dimension(:,:),allocatable::x,xg
  real(dp),dimension(:,:,:),allocatable::var
  real(dp),dimension(:)  ,allocatable::rho
  logical,dimension(:)  ,allocatable::ref
  integer,dimension(:)  ,allocatable::isp
  integer,dimension(:,:),allocatable::son,ngridfile,ngridlevel,ngridbound
  real(dp),dimension(1:8,1:3)::xc
  real(dp),dimension(1:3)::xbound=(/0d0,0d0,0d0/)
  character(LEN=5)::nchar,ncharcpu
  character(LEN=80)::ordering
  character(LEN=128)::nomfich,repository,outfich,filetype='bin'
  logical::ok,ok_part,ok_cell
  real(dp),dimension(:),allocatable::bound_key
  logical,dimension(:),allocatable::cpu_read
  integer,dimension(:),allocatable::cpu_list
  character(LEN=1)::proj='z'

  type level
     integer::ilevel
     integer::ngrid
     integer::imin
     integer::imax
     integer::jmin
     integer::jmax
     integer::kmin
     integer::kmax
  end type level

  type(level),dimension(1:100)::grid

  ! Temporary space for reading labels from the info file.
  character(LEN=128)::temp_label
  
  ! VTK variables
  logical :: quick_and_dirty = .true.
  integer :: nvtkmax         = 300000
  integer :: nvarmax         = 4
  integer :: nverts_tot      = 27
  integer :: nmaxlevels      = 30
  logical :: single_file     = .false.
  
  integer(8), dimension(:,:  ), allocatable :: xyz
  integer   , dimension(:,:,:), allocatable :: vtkvertices
  integer   , dimension(:,:  ), allocatable :: cell_nvert,cell_types
  integer   , dimension(:    ), allocatable :: old_vertices
  integer   , dimension(:    ), allocatable :: ncells_level
  logical   , dimension(:,:,:), allocatable :: split_face
  logical   , dimension(:,:  ), allocatable :: split_cell  
  logical   , dimension(:    ), allocatable :: is_center,is_corner,is_edge,is_face
  real(dp)  , dimension(:    ), allocatable :: vtkPointDataCount
  real(dp)  , dimension(:,:  ), allocatable :: vtkPointData,corner_shift
  
  integer                        :: ii,jj,kk,ll,ncells,ip,npoints,ntetra,iskip,nskip
  integer                        :: ioff0,ioff1,ioff2,ioff3,ioff4,ioff5,ioff6,ioff7
  integer                        :: nbytes_xyz,nbytes_rhoc,nbytes_rhop,nbytes_cellc
  integer                        :: nbytes_cellt,nbytes_cello,nvertices,vert_offset_count
  integer                        :: nbytes_velp,nbytes_magp,nbytes_prep
  integer                        :: iicell,jjcell,iivert,jjvert,nrealpoints,ncells_backup
  integer                        :: ntempcells,npoints_extra,nverts,npointsmax
  integer                        :: kstart,kincr,funit1,funit2,funit3,funit4
  logical                        :: this_is_a_new_point,vertex_not_found
  integer(8)                     :: ix1,iy1,iz1
  character (len=500)            :: string
  character (len=128)            :: vtkfile1,vtkfile2,pvtkfile
  character (len=9  )            :: offset
  character (len=2  )            :: prec_string
  character (len=1  ), parameter :: end_rec = char(10)    ! end-character for binary-record finalize
  real(dp)                       :: scaling,weight

#ifdef MPI
  if(single_file)then
     write(*,*) 'Cannot create single file with MPI'
     stop
  endif
  call mpi_init(ierror)
  world = mpi_comm_world
  call mpi_comm_size(world,nproc,ierror)
  call mpi_comm_rank(world,rank,ierror)
  call mpi_barrier(world,ierror)
#else
  nproc = 1 ; rank = 0
#endif
  
  if(quick_and_dirty)then
     nverts = 8
  else
     nverts = nverts_tot
  endif
  
  call allocate_arrays
  call read_params

  write(prec_string,'(i2)') dp*8
  
  !-----------------------------------------------
  ! Lecture du fichier hydro au format RAMSES
  !-----------------------------------------------
  ipos=INDEX(repository,'output_')
  nchar=repository(ipos+7:ipos+13)
  nomfich=TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out00001'
  inquire(file=nomfich, exist=ok) ! verify input file 
  if ( .not. ok ) then
     print *,TRIM(nomfich)//' not found.'
     stop
  endif
  nomfich=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'
  inquire(file=nomfich, exist=ok) ! verify input file 
  if ( .not. ok ) then
     print *,TRIM(nomfich)//' not found.'
     stop
  endif

  funit1 = 10+rank
  
  nomfich=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'
  open(unit=funit1,file=nomfich,status='old',form='unformatted')
  read(funit1)ncpu
  read(funit1)ndim
  read(funit1)nx,ny,nz
  read(funit1)nlevelmax
  read(funit1)ngridmax
  read(funit1)nboundary
  read(funit1)ngrid_current
  read(funit1)boxlen
  close(funit1)
  twotondim=2**ndim
  xbound=(/dble(nx/2),dble(ny/2),dble(nz/2)/)

  allocate(ngridfile(1:ncpu+nboundary,1:nlevelmax))
  allocate(ngridlevel(1:ncpu,1:nlevelmax))
  if(nboundary>0)allocate(ngridbound(1:nboundary,1:nlevelmax))

  if(ndim==2)then
     write(*,*)'Output file contains 2D data'
     write(*,*)'Aborting'
     stop
  endif

  nomfich=TRIM(repository)//'/info_'//TRIM(nchar)//'.txt'
  open(unit=funit1,file=nomfich,form='formatted',status='old')
  read(funit1,*)
  read(funit1,*)
  read(funit1,'(A13,I11)')temp_label,levelmin
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)

  read(funit1,*)
  read(funit1,'(A13,E23.15)')temp_label,t
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*)
  read(funit1,*) ! mu_gas
  read(funit1,*) ! ngrp

  read(funit1,'(A14,A80)')temp_label,ordering
  if(rank==0) write(*,'(XA14,A20)')temp_label,TRIM(ordering)
  read(funit1,*)
  allocate(cpu_list(1:ncpu))
  if(TRIM(ordering).eq.'hilbert')then
     allocate(bound_key(0:ncpu))
     allocate(cpu_read(1:ncpu))
     cpu_read=.false.
     do impi=1,ncpu
        read(funit1,'(I8,1X,E23.15,1X,E23.15)')i,bound_key(impi-1),bound_key(impi)
     end do
  endif
  close(funit1)

  !-----------------------
  ! Map parameters
  !-----------------------
  if(lmax==0)then
     lmax=nlevelmax
  endif
  if(rank==0)then
     write(*,*)'time=',t
     write(*,*)'Working resolution =',2**lmax
  endif
  xxmin=xmin ; xxmax=xmax
  yymin=ymin ; yymax=ymax
  zzmin=zmin ; zzmax=zmax

  if(TRIM(ordering).eq.'hilbert')then

     dmax=max(xxmax-xxmin,yymax-yymin,zzmax-zzmin)
     do ilevel=1,lmax
        dx=0.5_dp**ilevel
        if(dx.lt.dmax)exit
     end do
     lmin=ilevel
     bit_length=lmin-1
     maxdom=2**bit_length
     imin=0; imax=0; jmin=0; jmax=0; kmin=0; kmax=0
     if(bit_length>0)then
        imin=int(xxmin*dble(maxdom))
        imax=imin+1
        jmin=int(yymin*dble(maxdom))
        jmax=jmin+1
        kmin=int(zzmin*dble(maxdom))
        kmax=kmin+1
     endif

     dkey=(dble(2**(nlevelmax+1)/dble(maxdom)))**ndim
     ndom=1
     if(bit_length>0)ndom=8
     idom(1)=imin; idom(2)=imax
     idom(3)=imin; idom(4)=imax
     idom(5)=imin; idom(6)=imax
     idom(7)=imin; idom(8)=imax
     jdom(1)=jmin; jdom(2)=jmin
     jdom(3)=jmax; jdom(4)=jmax
     jdom(5)=jmin; jdom(6)=jmin
     jdom(7)=jmax; jdom(8)=jmax
     kdom(1)=kmin; kdom(2)=kmin
     kdom(3)=kmin; kdom(4)=kmin
     kdom(5)=kmax; kdom(6)=kmax
     kdom(7)=kmax; kdom(8)=kmax
     
     do i=1,ndom
        if(bit_length>0)then
           call hilbert3d(idom(i),jdom(i),kdom(i),order_min,bit_length,1)
        else
           order_min=0.0d0
        endif
        bounding_min(i)=(order_min(1))*dkey
        bounding_max(i)=(order_min(1)+1.0D0)*dkey
     end do
     
     cpu_min=0; cpu_max=0
     do impi=1,ncpu
        do i=1,ndom
           if (   bound_key(impi-1).le.bounding_min(i).and.&
                & bound_key(impi  ).gt.bounding_min(i))then
              cpu_min(i)=impi
           endif
           if (   bound_key(impi-1).lt.bounding_max(i).and.&
                & bound_key(impi  ).ge.bounding_max(i))then
              cpu_max(i)=impi
           endif
        end do
     end do
     
     ncpu_read=0
     do i=1,ndom
        do j=cpu_min(i),cpu_max(i)
           if(.not. cpu_read(j))then
              ncpu_read=ncpu_read+1
              cpu_list(ncpu_read)=j
              cpu_read(j)=.true.
           endif
        enddo
     enddo
  else
     ncpu_read=ncpu
     do j=1,ncpu
        cpu_list(j)=j
     end do
  end  if

  !-----------------------------
  ! Compute hierarchy
  !-----------------------------
  do ilevel=1,lmax
     nx_full=2**ilevel
     ny_full=2**ilevel
     nz_full=2**ilevel
     imin=int(xxmin*dble(nx_full))+1
     imax=int(xxmax*dble(nx_full))+1
     jmin=int(yymin*dble(ny_full))+1
     jmax=int(yymax*dble(ny_full))+1
     kmin=int(zzmin*dble(nz_full))+1
     kmax=int(zzmax*dble(nz_full))+1
     grid(ilevel)%imin=imin
     grid(ilevel)%imax=imax
     grid(ilevel)%jmin=jmin
     grid(ilevel)%jmax=jmax
     grid(ilevel)%kmin=kmin
     grid(ilevel)%kmax=kmax
  end do

  if((.not.single_file) .and. (rank == 0))then
     ! Open parallel VTK file
     pvtkfile = trim(repository)//'/'//trim(repository)//'.pvtu'
     funit4 = 500
     open (funit4,file=trim(pvtkfile),form='formatted')
     write(funit4,'(a)') '<?xml version="1.0"?>'
     write(funit4,'(a)') '<VTKFile type="PUnstructuredGrid" version="0.1" byte_order="LittleEndian">'
     write(funit4,'(a)') '  <PUnstructuredGrid GhostLevel="0">'
     write(funit4,'(a)') '    <PPoints>'
     write(funit4,'(a)') '      <PDataArray type="Float'//prec_string//'" Name="Coordinates" NumberOfComponents="3" />'
     write(funit4,'(a)') '    </PPoints>'
!      write(funit4,'(a)') '    <PCellData>'
!      write(funit4,'(a)') '      <PDataArray type="Float'//prec_string//'" Name="Density Cells" NumberOfComponents="1"/>'
!      write(funit4,'(a)') '    </PCellData>'
     write(funit4,'(a)') '    <PPointData>'
     write(funit4,'(a)') '      <PDataArray type="Float'//prec_string//'" Name="Density" NumberOfComponents="1"/>'
     write(funit4,'(a)') '    </PPointData>'
     do icpu = 1,ncpu_read
        call generate_vtk_filename(repository,icpu,vtkfile1,vtkfile2)
        write(funit4,'(a)') '    <Piece Source="'//trim(vtkfile2)//'" />'
     enddo
     write(funit4,'(a)') '  </PUnstructuredGrid>'
     write(funit4,'(a)') '</VTKFile>'
     close(funit4)
  endif
  
  !-----------------------------------------------
  ! Compute projected variables
  !----------------------------------------------
  
  ! Scaling factor to convert between real coordinate and integer position
  scaling = 2.0_dp**lmax
  
  ! Useful array to go through cell vertices efficiently
  corner_shift(1, 1) = -1.0_dp ; corner_shift(2, 1) = -1.0_dp ; corner_shift(3, 1) = -1.0_dp
  corner_shift(1, 2) =  1.0_dp ; corner_shift(2, 2) = -1.0_dp ; corner_shift(3, 2) = -1.0_dp
  corner_shift(1, 3) =  1.0_dp ; corner_shift(2, 3) =  1.0_dp ; corner_shift(3, 3) = -1.0_dp
  corner_shift(1, 4) = -1.0_dp ; corner_shift(2, 4) =  1.0_dp ; corner_shift(3, 4) = -1.0_dp
  corner_shift(1, 5) = -1.0_dp ; corner_shift(2, 5) = -1.0_dp ; corner_shift(3, 5) =  1.0_dp
  corner_shift(1, 6) =  1.0_dp ; corner_shift(2, 6) = -1.0_dp ; corner_shift(3, 6) =  1.0_dp
  corner_shift(1, 7) =  1.0_dp ; corner_shift(2, 7) =  1.0_dp ; corner_shift(3, 7) =  1.0_dp
  corner_shift(1, 8) = -1.0_dp ; corner_shift(2, 8) =  1.0_dp ; corner_shift(3, 8) =  1.0_dp
  corner_shift(1, 9) =  0.0_dp ; corner_shift(2, 9) = -1.0_dp ; corner_shift(3, 9) = -1.0_dp
  corner_shift(1,10) =  1.0_dp ; corner_shift(2,10) =  0.0_dp ; corner_shift(3,10) = -1.0_dp
  corner_shift(1,11) =  0.0_dp ; corner_shift(2,11) =  1.0_dp ; corner_shift(3,11) = -1.0_dp
  corner_shift(1,12) = -1.0_dp ; corner_shift(2,12) =  0.0_dp ; corner_shift(3,12) = -1.0_dp
  corner_shift(1,13) =  0.0_dp ; corner_shift(2,13) = -1.0_dp ; corner_shift(3,13) =  1.0_dp
  corner_shift(1,14) =  1.0_dp ; corner_shift(2,14) =  0.0_dp ; corner_shift(3,14) =  1.0_dp
  corner_shift(1,15) =  0.0_dp ; corner_shift(2,15) =  1.0_dp ; corner_shift(3,15) =  1.0_dp
  corner_shift(1,16) = -1.0_dp ; corner_shift(2,16) =  0.0_dp ; corner_shift(3,16) =  1.0_dp
  corner_shift(1,17) = -1.0_dp ; corner_shift(2,17) = -1.0_dp ; corner_shift(3,17) =  0.0_dp
  corner_shift(1,18) =  1.0_dp ; corner_shift(2,18) = -1.0_dp ; corner_shift(3,18) =  0.0_dp
  corner_shift(1,19) =  1.0_dp ; corner_shift(2,19) =  1.0_dp ; corner_shift(3,19) =  0.0_dp
  corner_shift(1,20) = -1.0_dp ; corner_shift(2,20) =  1.0_dp ; corner_shift(3,20) =  0.0_dp
  corner_shift(1,21) =  0.0_dp ; corner_shift(2,21) = -1.0_dp ; corner_shift(3,21) =  0.0_dp
  corner_shift(1,22) =  1.0_dp ; corner_shift(2,22) =  0.0_dp ; corner_shift(3,22) =  0.0_dp
  corner_shift(1,23) =  0.0_dp ; corner_shift(2,23) =  1.0_dp ; corner_shift(3,23) =  0.0_dp
  corner_shift(1,24) = -1.0_dp ; corner_shift(2,24) =  0.0_dp ; corner_shift(3,24) =  0.0_dp
  corner_shift(1,25) =  0.0_dp ; corner_shift(2,25) =  0.0_dp ; corner_shift(3,25) = -1.0_dp
  corner_shift(1,26) =  0.0_dp ; corner_shift(2,26) =  0.0_dp ; corner_shift(3,26) =  1.0_dp
  corner_shift(1,27) =  0.0_dp ; corner_shift(2,27) =  0.0_dp ; corner_shift(3,27) =  0.0_dp
  
  npoints = 0 ; ncells_level = 0 ; ncells = 0
  vtkPointData = 0.0_dp ; vtkPointDataCount = 0.0_dp
  cell_types  = 12 ; cell_nvert = 8
  split_face = .false.
  split_cell = .false.
  is_center  = .false.
  is_corner  = .false.
  is_edge    = .false.
  is_face    = .false.
  
  kstart = rank+1
  kincr  = nproc
  
  ! Loop over processor files
  do k = kstart,ncpu_read,kincr
  
     if(.not.single_file)then
        npoints = 0 ; ncells_level = 0 ; ncells = 0
        vtkPointData = 0.0_dp ; vtkPointDataCount = 0.0_dp
        cell_types  = 12 ; cell_nvert = 8
        split_face = .false.
        split_cell = .false.
        is_center  = .false.
        is_corner  = .false.
        is_edge    = .false.
        is_face    = .false.
     endif
     
     icpu=cpu_list(k)
     call title(icpu,ncharcpu)

     ! Open AMR file and skip header
     nomfich=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out'//TRIM(ncharcpu)
     funit1 = 10+rank
     open(unit=funit1,file=nomfich,status='old',form='unformatted')
     write(*,*)'Processing file '//TRIM(nomfich)
     do i=1,21
        read(funit1)
     end do
     ! Read grid numbers
     read(funit1)ngridlevel
     ngridfile(1:ncpu,1:nlevelmax)=ngridlevel
     read(funit1)
     if(nboundary>0)then
        do i=1,2
           read(funit1)
        end do
        read(funit1)ngridbound
        ngridfile(ncpu+1:ncpu+nboundary,1:nlevelmax)=ngridbound
     endif
     read(funit1)
! ROM: comment the single follwing line for old stuff
     read(funit1)
     if(TRIM(ordering).eq.'bisection')then
        do i=1,5
           read(funit1)
        end do
     else
        read(funit1)
     endif
     read(funit1)
     read(funit1)
     read(funit1)

     ! Open HYDRO file and skip header
     nomfich=TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out'//TRIM(ncharcpu)
     funit2 = 100+rank
     open(unit=funit2,file=nomfich,status='old',form='unformatted')
     read(funit2)
     read(funit2)nvarh
     read(funit2)
     read(funit2)
     read(funit2)
     read(funit2)

     ! Loop over levels
     do ilevel=1,lmax
     
!         ncells_level = 0

        ! Geometry
        dx=0.5_dp**ilevel
        dx2=0.5_dp*dx
        nx_full=2**ilevel
        ny_full=2**ilevel
        nz_full=2**ilevel
        do ind=1,twotondim
           iz=(ind-1)/4
           iy=(ind-1-4*iz)/2
           ix=(ind-1-2*iy-4*iz)
           xc(ind,1)=(dble(ix)-0.5D0)*dx
           xc(ind,2)=(dble(iy)-0.5D0)*dx
           xc(ind,3)=(dble(iz)-0.5D0)*dx
        end do

        ! Allocate work arrays
        ngrida=ngridfile(icpu,ilevel)
        grid(ilevel)%ngrid=ngrida
        if(ngrida>0)then
           allocate(xg(1:ngrida,1:ndim))
           allocate(son(1:ngrida,1:twotondim))
           allocate(var(1:ngrida,1:twotondim,1:nvarh))
           allocate(x  (1:ngrida,1:ndim))
           allocate(rho(1:ngrida))
           allocate(ref(1:ngrida))
        endif

        ! Loop over domains
        do j=1,nboundary+ncpu

           ! Read AMR data
           if(ngridfile(j,ilevel)>0)then
              read(funit1) ! Skip grid index
              read(funit1) ! Skip next index
              read(funit1) ! Skip prev index
              ! Read grid center
              do idim=1,ndim
                 if(j.eq.icpu)then
                    read(funit1)xg(:,idim)
                 else
                    read(funit1)
                 endif
              end do
              read(funit1) ! Skip father index
              do ind=1,2*ndim
                 read(funit1) ! Skip nbor index
              end do
              ! Read son index
              do ind=1,twotondim
                 if(j.eq.icpu)then
                    read(funit1)son(:,ind)
                 else
                    read(funit1)
                 end if
              end do
              ! Skip cpu map
              do ind=1,twotondim
                 read(funit1)
              end do
              ! Skip refinement map
              do ind=1,twotondim
                 read(funit1)
              end do
           endif

           ! Read HYDRO data
           read(funit2)
           read(funit2)
           if(ngridfile(j,ilevel)>0)then
              ! Read hydro variables
              do ind=1,twotondim
                 do ivar=1,nvarh
                    if(j.eq.icpu)then
                       read(funit2)var(:,ind,ivar)
                    else
                       read(funit2)
                    end if
                 end do
              end do
           end if
        end do

        ! Compute map
        if(ngrida>0)then

           ! Loop over cells
           do ind=1,twotondim

              ! Compute cell center
              do i=1,ngrida
                 x(i,1)=(xg(i,1)+xc(ind,1)-xbound(1))
                 x(i,2)=(xg(i,2)+xc(ind,2)-xbound(2))
                 x(i,3)=(xg(i,3)+xc(ind,3)-xbound(3))
              end do
              ! Check if cell is refined
              do i=1,ngrida
                 ref(i)=son(i,ind)>0.and.ilevel<lmax
              end do
              ! Store data cube
              do i=1,ngrida
                 ok_cell= .not.ref(i).and. &
                      & (x(i,1)+dx2)>=xmin.and.&
                      & (x(i,2)+dx2)>=ymin.and.&
                      & (x(i,3)+dx2)>=zmin.and.&
                      & (x(i,1)-dx2)<=xmax.and.&
                      & (x(i,2)-dx2)<=ymax.and.&
                      & (x(i,3)-dx2)<=zmax
                 if(ok_cell)then

                    ncells = ncells + 1
                    ncells_level(ilevel) = ncells_level(ilevel) + 1

                    ! Go through 8/27 vertices for unstructured grid
                    do kk = 1,nverts
                    
                       ix1 = nint((x(i,1)+corner_shift(1,kk)*0.5_dp*dx)*scaling,8)
                       iy1 = nint((x(i,2)+corner_shift(2,kk)*0.5_dp*dx)*scaling,8)
                       iz1 = nint((x(i,3)+corner_shift(3,kk)*0.5_dp*dx)*scaling,8)
                       this_is_a_new_point = .true.
                       
                       ! Check if point already exists other cells
                       
                       ! Search through corner vertices of current and previous levels
                       if(ncells > 1)then
                          vertex_not_found = .true.
                          ll = ilevel
                          do while(vertex_not_found .and. (ll > levelmin-1))
!                           do ll = max(ilevel-1,1),ilevel
                             jj = ncells_level(ll)-1
                             do while(vertex_not_found .and. (jj > 0))
!                              do jj = ncells_level(ll)-1,1,-1 ! start from the end as neighbour cells are more likely to be at the end
                                ii = 1
                                do while(vertex_not_found .and. (ii < nverts+1))
!                                 do ii = 1,nverts
                                   if( (ix1 == xyz(1,vtkvertices(ii,ll,jj)+1)) .and. &
                                       (iy1 == xyz(2,vtkvertices(ii,ll,jj)+1)) .and. &
                                       (iz1 == xyz(3,vtkvertices(ii,ll,jj)+1)) ) then
                                      vtkvertices(kk,ilevel,ncells_level(ilevel)) = vtkvertices(ii,ll,jj)
                                      this_is_a_new_point = .false.
                                      vertex_not_found = .false.
                                      select case(kk)
                                      case(1:8)
                                         is_corner(vtkvertices(ii,ll,jj)+1) = .true.
                                      case(9:20)
                                         is_edge(vtkvertices(ii,ll,jj)+1) = .true.
                                      case(21:26)
                                         is_face(vtkvertices(ii,ll,jj)+1) = .true.
                                      case(27)
                                         is_center(vtkvertices(ii,ll,jj)+1) = .true.
                                      end select
                                      if(is_corner(vtkvertices(ii,ll,jj)+1).and.is_face(vtkvertices(ii,ll,jj)+1))then
                                         split_cell(ll,jj) = .true.
                                         ! Now identify face to be split
                                         select case(ii)
                                         case(25)
                                            split_face(1,ll,jj) = .true.
                                         case(26)
                                            split_face(2,ll,jj) = .true.
                                         case(21)
                                            split_face(3,ll,jj) = .true.
                                         case(22)
                                            split_face(4,ll,jj) = .true.
                                         case(23)
                                            split_face(5,ll,jj) = .true.
                                         case(24)
                                            split_face(6,ll,jj) = .true.
                                         end select
!                                       elseif(is_corner(vtkvertices(ii,ll,jj)+1).and.is_edge(vtkvertices(ii,ll,jj)+1))then
!                                          split_cell(ll,jj) = .true.
!                                          ! Now identify face to be split
!                                          select case(ii)
!                                          case( 9)
!                                             split_face(1,ll,jj) = .true.
!                                             split_face(3,ll,jj) = .true.
!                                          case(10)
!                                             split_face(1,ll,jj) = .true.
!                                             split_face(4,ll,jj) = .true.
!                                          case(11)
!                                             split_face(1,ll,jj) = .true.
!                                             split_face(5,ll,jj) = .true.
!                                          case(12)
!                                             split_face(1,ll,jj) = .true.
!                                             split_face(6,ll,jj) = .true.
!                                          case(13)
!                                             split_face(2,ll,jj) = .true.
!                                             split_face(3,ll,jj) = .true.
!                                          case(14)
!                                             split_face(2,ll,jj) = .true.
!                                             split_face(4,ll,jj) = .true.
!                                          case(15)
!                                             split_face(2,ll,jj) = .true.
!                                             split_face(5,ll,jj) = .true.
!                                          case(16)
!                                             split_face(2,ll,jj) = .true.
!                                             split_face(6,ll,jj) = .true.
!                                          case(17)
!                                             split_face(3,ll,jj) = .true.
!                                             split_face(6,ll,jj) = .true.
!                                          case(18)
!                                             split_face(3,ll,jj) = .true.
!                                             split_face(4,ll,jj) = .true.
!                                          case(19)
!                                             split_face(4,ll,jj) = .true.
!                                             split_face(5,ll,jj) = .true.
!                                          case(20)
!                                             split_face(5,ll,jj) = .true.
!                                             split_face(6,ll,jj) = .true.
!                                          end select
                                      endif
                                   endif
                                   ii = ii + 1
                                enddo
                                jj = jj - 1
                             enddo
                             ll = ll - 1
                          enddo
!                           
                       endif ! end if ncells > 1 searching through corner vertices of current and previous levels
                          
                       ! Add new point if it does not exist
                       if(this_is_a_new_point)then                    
                          npoints = npoints + 1
                          vtkvertices(kk,ilevel,ncells_level(ilevel)) = npoints - 1
                          xyz(1,npoints) = ix1
                          xyz(2,npoints) = iy1
                          xyz(3,npoints) = iz1
                       endif
                       
                       select case(kk)
                       case(1:8)
                          is_corner(vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = .true.
                          weight = 1.0_dp / (sqrt(3.0_dp)*dx)
                       case(9:20)
                          is_edge(vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = .true.
                          weight = 1.0_dp / (sqrt(2.0_dp)*dx)
                       case(21:26)
                          is_face(vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = .true.
                          weight = 1.0_dp / dx
                       case(27)
                          is_center(vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = .true.
                          weight = 1.0_dp
                       end select
                       
                       ! Density
                       vtkPointData(1,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = &
                       vtkPointData(1,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) + weight*var(i,ind,1)
                       
                       ! Velocity
                       vtkPointData(2,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = &
                       vtkPointData(2,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) + &
                       weight*sqrt(var(i,ind,2)**2+var(i,ind,3)**2+var(i,ind,4)**2)
                       
                       ! Magnetic field
                       vtkPointData(3,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = &
                       vtkPointData(3,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) + &
                       weight*sqrt((0.5_dp*(var(i,ind,5)+var(i,ind, 8)))**2 + &
                                   (0.5_dp*(var(i,ind,3)+var(i,ind, 9)))**2 + &
                                   (0.5_dp*(var(i,ind,4)+var(i,ind,10)))**2)
                       
                       ! Pressure
                       vtkPointData(4,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = &
                       vtkPointData(4,vtkvertices(kk,ilevel,ncells_level(ilevel))+1) + weight*var(i,ind,11)
                       
                       vtkPointDataCount(vtkvertices(kk,ilevel,ncells_level(ilevel))+1) = &
                       vtkPointDataCount(vtkvertices(kk,ilevel,ncells_level(ilevel))+1) + weight
                       
                    enddo ! end loop over cell corner vertices
                    
                 end if
              end do

           end do
           ! End loop over cell

           deallocate(xg,son,var,ref,rho,x)
        endif
        
        write(*,'(a,i2,a,i7,a,i9,a,i9)') 'File '//TRIM(nomfich)//': level ',ilevel,' Added ',ncells_level(ilevel),&
                                        &' new cells. Total:',sum(ncells_level),'. Points: ',npoints

     end do
     ! End loop over levels

     close(funit1)
     close(funit2)
     
     if(.not.quick_and_dirty)then
     
        ncells_backup = sum(ncells_level)
     
        ! Connect level boundaries in a nice way

        do ll = 1,lmax
        
           ntempcells = ncells_level(ll)
        
           do jj = 1,ncells_level(ll)
         
              if(split_cell(ll,jj))then
              
                 ! Make backup copy of cell vertices
                 old_vertices(1:nverts_tot) = vtkvertices(1:nverts_tot,ll,jj)
                 
                 ! Redefine old cell to first pyramid
                 if(split_face(1,ll,jj))then
                    vtkvertices(1,ll,jj) = old_vertices(       1)
                    vtkvertices(2,ll,jj) = vtkvertices ( 9,ll,jj)
                    vtkvertices(3,ll,jj) = vtkvertices (25,ll,jj)
                    vtkvertices(4,ll,jj) = vtkvertices (12,ll,jj)
                    vtkvertices(5,ll,jj) = old_vertices(      27)
                    cell_nvert(ll,jj) = 5
                    cell_types(ll,jj) = 14
                    ! Now create 3 new cells to complete the face
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices ( 9,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       2)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (10,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (25,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (10,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       3)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (11,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (25,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (11,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       4)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (12,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (25,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 else
                    vtkvertices(1:4,ll,jj) = old_vertices(1:4)
                    vtkvertices(  5,ll,jj) = old_vertices( 27)
                    cell_nvert(ll,jj) = 5
                    cell_types(ll,jj) = 14
                 endif
                 
                 ! Now go through 5 remaining faces to complete the cube
                 
                 if(split_face(2,ll,jj))then
                    ! Split face into 4 new cells
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices(       5)
                    vtkvertices(2,ll,ntempcells) = vtkvertices (13,ll,jj)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (26,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (16,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (13,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       6)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (14,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (26,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (14,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       7)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (15,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (26,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (15,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       8)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (16,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (26,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 else
                    ! Add a single face pyramid
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices( 5)
                    vtkvertices(2,ll,ntempcells) = old_vertices( 6)
                    vtkvertices(3,ll,ntempcells) = old_vertices( 7)
                    vtkvertices(4,ll,ntempcells) = old_vertices( 8)
                    vtkvertices(5,ll,ntempcells) = old_vertices(27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 endif
                 
                 if(split_face(3,ll,jj))then
                    ! Split face into 4 new cells
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices(       1)
                    vtkvertices(2,ll,ntempcells) = vtkvertices ( 9,ll,jj)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (21,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (17,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices ( 9,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       2)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (18,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (21,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (18,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       6)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (13,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (21,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (13,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       5)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (17,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (21,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 else
                    ! Add a single face pyramid
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices( 1)
                    vtkvertices(2,ll,ntempcells) = old_vertices( 2)
                    vtkvertices(3,ll,ntempcells) = old_vertices( 6)
                    vtkvertices(4,ll,ntempcells) = old_vertices( 5)
                    vtkvertices(5,ll,ntempcells) = old_vertices(27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 endif
                 
                 if(split_face(4,ll,jj))then
                    ! Split face into 4 new cells
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices(       2)
                    vtkvertices(2,ll,ntempcells) = vtkvertices (10,ll,jj)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (22,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (18,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (10,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       3)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (19,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (22,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (19,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       7)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (14,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (22,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (14,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       6)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (18,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (22,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 else
                    ! Add a single face pyramid
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices( 2)
                    vtkvertices(2,ll,ntempcells) = old_vertices( 3)
                    vtkvertices(3,ll,ntempcells) = old_vertices( 7)
                    vtkvertices(4,ll,ntempcells) = old_vertices( 6)
                    vtkvertices(5,ll,ntempcells) = old_vertices(27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 endif
                 
                 if(split_face(5,ll,jj))then
                    ! Split face into 4 new cells
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices(       3)
                    vtkvertices(2,ll,ntempcells) = vtkvertices (11,ll,jj)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (23,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (19,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (11,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       4)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (20,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (23,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (20,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       8)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (15,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (23,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (15,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       7)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (19,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (23,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 else
                    ! Add a single face pyramid
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices( 3)
                    vtkvertices(2,ll,ntempcells) = old_vertices( 4)
                    vtkvertices(3,ll,ntempcells) = old_vertices( 8)
                    vtkvertices(4,ll,ntempcells) = old_vertices( 7)
                    vtkvertices(5,ll,ntempcells) = old_vertices(27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 endif
                 
                 if(split_face(6,ll,jj))then
                    ! Split face into 4 new cells
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices(       4)
                    vtkvertices(2,ll,ntempcells) = vtkvertices (12,ll,jj)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (24,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (20,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (12,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       1)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (17,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (24,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (17,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       5)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (16,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (24,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = vtkvertices (16,ll,jj)
                    vtkvertices(2,ll,ntempcells) = old_vertices(       8)
                    vtkvertices(3,ll,ntempcells) = vtkvertices (20,ll,jj)
                    vtkvertices(4,ll,ntempcells) = vtkvertices (24,ll,jj)
                    vtkvertices(5,ll,ntempcells) = old_vertices(      27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 else
                    ! Add a single face pyramid
                    ntempcells = ntempcells + 1
                    vtkvertices(1,ll,ntempcells) = old_vertices( 4)
                    vtkvertices(2,ll,ntempcells) = old_vertices( 1)
                    vtkvertices(3,ll,ntempcells) = old_vertices( 5)
                    vtkvertices(4,ll,ntempcells) = old_vertices( 8)
                    vtkvertices(5,ll,ntempcells) = old_vertices(27)
                    cell_nvert(ll,ntempcells) = 5
                    cell_types(ll,ntempcells) = 14
                 endif
                 
              endif ! end if(split_cell)
              
           enddo ! end loop over cells for this level

           ncells_level(ll) = ntempcells
           
        enddo ! end loop over levels
     
        write(*,*) 'File '//TRIM(nomfich)//': Constructed',sum(ncells_level)-ncells_backup,&
                   &'new cells at AMR level interfaces'
     
     endif ! end if(quick_and_dirty)

          
     ! Calculate point data average
     do kk = 1,npoints
        do jj = 1,nvarmax
!         if(vtkPointDataCount(kk) < 0.5_dp)then
!            write(*,*) 'WARNING! PointDataCount is less than 1:',vtkPointDataCount(kk),vtkPointData(kk),kk
!         else
           vtkPointData(jj,kk) = vtkPointData(jj,kk) / vtkPointDataCount(kk)
!         endif
        enddo
     enddo
     
     
     if(.not.single_file)then
     
        ! Compute data byte sizes and offsets
        ncells = 0
        nvertices = 0
        do ll = 1,lmax
           ncells = ncells + ncells_level(ll)
           do ii = 1,ncells_level(ll)
              nvertices = nvertices + cell_nvert(ll,ii)
           enddo
        enddo
     
        nbytes_xyz   = 3 * npoints   * dp
        nbytes_cellc =     nvertices * 4
        nbytes_cello =     ncells    * 4
        nbytes_cellt =     ncells    * 4
        nbytes_rhop  =     npoints   * dp
        nbytes_velp  =     npoints   * dp
        nbytes_magp  =     npoints   * dp
        nbytes_prep  =     npoints   * dp
        
        ioff0 = 0                                   ! xyz coordinates
        ioff1 = ioff0 + sizeof(ip) + nbytes_xyz     ! cell connectivity
        ioff2 = ioff1 + sizeof(ip) + nbytes_cellc   ! cell offsets
        ioff3 = ioff2 + sizeof(ip) + nbytes_cello   ! cell types
        ioff4 = ioff3 + sizeof(ip) + nbytes_cellt   ! density points
        ioff5 = ioff4 + sizeof(ip) + nbytes_rhop    ! velocity points
        ioff6 = ioff5 + sizeof(ip) + nbytes_velp    ! B field points
        ioff7 = ioff6 + sizeof(ip) + nbytes_magp    ! pressure points
        
        ! Start writing VTK file
        call generate_vtk_filename(repository,icpu,vtkfile1,vtkfile2)
        funit3 = 200+rank
        write(*,*) 'Writing VTK file: '//trim(vtkfile1)
        open (funit3,file=trim(vtkfile1),form='unformatted',access='stream')
        write(funit3) '<?xml version="1.0"?>'//end_rec
        write(funit3) '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">'//end_rec
        write(funit3) '   <UnstructuredGrid>'//end_rec
        write(string,'(a,i9,a,i9,a)') '   <Piece NumberOfPoints="',npoints,'" NumberOfCells="',ncells,'">'
        write(funit3) trim(string)//end_rec
        write(funit3) '      <Points>'//end_rec
        write(offset,'(i9)') ioff0
        write(funit3) '         <DataArray type="Float'//prec_string//&
        &'" Name="Coordinates" NumberOfComponents="3" format="appended" offset="'//offset//'" />'//end_rec
        write(funit3) '      </Points>'//end_rec
        write(funit3) '      <Cells>'//end_rec
        write(offset,'(i9)') ioff1
        write(funit3) '         <DataArray type="Int32" Name="connectivity" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(offset,'(i9)') ioff2
        write(funit3) '         <DataArray type="Int32" Name="offsets" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(offset,'(i9)') ioff3
        write(funit3) '         <DataArray type="Int32" Name="types" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(funit3) '      </Cells>'//end_rec
        write(funit3) '      <PointData>'//end_rec
        write(offset,'(i9)') ioff4
        write(funit3) '         <DataArray type="Float'//prec_string//'" Name="Density" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(offset,'(i9)') ioff5
        write(funit3) '         <DataArray type="Float'//prec_string//'" Name="Velocity" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(offset,'(i9)') ioff6
        write(funit3) '         <DataArray type="Float'//prec_string//'" Name="B field" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(offset,'(i9)') ioff7
        write(funit3) '         <DataArray type="Float'//prec_string//'" Name="Pressure" format="appended" offset="'&
        &//offset//'" />'//end_rec
        write(funit3) '      </PointData>'//end_rec
        write(funit3) '   </Piece>'//end_rec
        write(funit3) '   </UnstructuredGrid>'//end_rec
        write(funit3) '   <AppendedData encoding="raw">'//end_rec
        write(funit3) '_'
        write(funit3) nbytes_xyz
        do jj = 1,npoints
           do ii = 1,3
              write(funit3) real(xyz(ii,jj),dp)/scaling
           enddo
        enddo
        write(funit3) nbytes_cellc
        do ll = 1,lmax
           do jj = 1,ncells_level(ll)
              do ii = 1,cell_nvert(ll,jj)
                 write(funit3) vtkvertices(ii,ll,jj)
              enddo
           enddo
        enddo
        write(funit3) nbytes_cello
        vert_offset_count = 0
        do ll = 1,lmax
           do jj = 1,ncells_level(ll)
              vert_offset_count = vert_offset_count + cell_nvert(ll,jj)
              write(funit3) vert_offset_count
           enddo
        enddo
        write(funit3) nbytes_cellt
        do ll = 1,lmax
           do jj = 1,ncells_level(ll)
              write(funit3) cell_types(ll,jj)
           enddo
        enddo
        write(funit3) nbytes_rhop
        do jj = 1,npoints
           write(funit3) vtkPointData(1,jj)
        enddo
        write(funit3) nbytes_velp
        do jj = 1,npoints
           write(funit3) vtkPointData(2,jj)
        enddo
        write(funit3) nbytes_magp
        do jj = 1,npoints
           write(funit3) vtkPointData(3,jj)
        enddo
        write(funit3) nbytes_prep
        do jj = 1,npoints
           write(funit3) vtkPointData(4,jj)
        enddo
        write(funit3) '   </AppendedData>'//end_rec
        write(funit3) '</VTKFile>'//end_rec
        close(funit3)
        
     endif

  end do
  ! End loop over cpus
  
  if(single_file)then
     ! Compute data byte sizes and offsets
     ncells = 0
     nvertices = 0
     do ll = 1,lmax
        ncells = ncells + ncells_level(ll)
        do ii = 1,ncells_level(ll)
           nvertices = nvertices + cell_nvert(ll,ii)
        enddo
     enddo
  
!      nbytes_xyz   = 3 * npoints   * dp
!      nbytes_cellc =     nvertices * 4
!      nbytes_cello =     ncells    * 4
!      nbytes_cellt =     ncells    * 4
!      nbytes_rhop  =     npoints   * dp
!      
!      ioff0 = 0                                   ! xyz coordinates
!      ioff1 = ioff0 + sizeof(ip) + nbytes_xyz     ! cell connectivity
!      ioff2 = ioff1 + sizeof(ip) + nbytes_cellc   ! cell offsets
!      ioff3 = ioff2 + sizeof(ip) + nbytes_cello   ! cell types
!      ioff4 = ioff3 + sizeof(ip) + nbytes_cellt   ! density points
     
     nbytes_xyz   = 3 * npoints   * dp
     nbytes_cellc =     nvertices * 4
     nbytes_cello =     ncells    * 4
     nbytes_cellt =     ncells    * 4
     nbytes_rhop  =     npoints   * dp
     nbytes_velp  =     npoints   * dp
     nbytes_magp  =     npoints   * dp
     nbytes_prep  =     npoints   * dp
     
     ioff0 = 0                                   ! xyz coordinates
     ioff1 = ioff0 + sizeof(ip) + nbytes_xyz     ! cell connectivity
     ioff2 = ioff1 + sizeof(ip) + nbytes_cellc   ! cell offsets
     ioff3 = ioff2 + sizeof(ip) + nbytes_cello   ! cell types
     ioff4 = ioff3 + sizeof(ip) + nbytes_cellt   ! density points
     ioff5 = ioff4 + sizeof(ip) + nbytes_rhop    ! velocity points
     ioff6 = ioff5 + sizeof(ip) + nbytes_velp    ! B field points
     ioff7 = ioff6 + sizeof(ip) + nbytes_magp    ! pressure points
     
     ! Start writing single VTK file
     vtkfile1 = trim(repository)//'/'//trim(repository)//'.vtu'
     funit3 = 200+rank
     write(*,*) 'Writing VTK file: '//trim(vtkfile1)
     open (funit3,file=trim(vtkfile1),form='unformatted',access='stream')
     write(funit3) '<?xml version="1.0"?>'//end_rec
     write(funit3) '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">'//end_rec
     write(funit3) '   <UnstructuredGrid>'//end_rec
     write(string,'(a,i9,a,i9,a)') '   <Piece NumberOfPoints="',npoints,'" NumberOfCells="',ncells,'">'
     write(funit3) trim(string)//end_rec
     write(funit3) '      <Points>'//end_rec
     write(offset,'(i9)') ioff0
     write(funit3) '         <DataArray type="Float'//prec_string//&
     &'" Name="Coordinates" NumberOfComponents="3" format="appended" offset="'//offset//'" />'//end_rec
     write(funit3) '      </Points>'//end_rec
     write(funit3) '      <Cells>'//end_rec
     write(offset,'(i9)') ioff1
     write(funit3) '         <DataArray type="Int32" Name="connectivity" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff2
     write(funit3) '         <DataArray type="Int32" Name="offsets" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff3
     write(funit3) '         <DataArray type="Int32" Name="types" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(funit3) '      </Cells>'//end_rec
     write(funit3) '      <PointData>'//end_rec
     write(offset,'(i9)') ioff4
     write(funit3) '         <DataArray type="Float'//prec_string//'" Name="Density Points" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff5
     write(funit3) '         <DataArray type="Float'//prec_string//'" Name="Velocity Points" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff6
     write(funit3) '         <DataArray type="Float'//prec_string//'" Name="B field Points" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(offset,'(i9)') ioff7
     write(funit3) '         <DataArray type="Float'//prec_string//'" Name="Pressure Points" format="appended" offset="'&
     &//offset//'" />'//end_rec
     write(funit3) '      </PointData>'//end_rec
     write(funit3) '   </Piece>'//end_rec
     write(funit3) '   </UnstructuredGrid>'//end_rec
     write(funit3) '   <AppendedData encoding="raw">'//end_rec
     write(funit3) '_'
     write(funit3) nbytes_xyz
     do jj = 1,npoints
        do ii = 1,3
           write(funit3) real(xyz(ii,jj),dp)/scaling
        enddo
     enddo
     write(funit3) nbytes_cellc
     do ll = 1,lmax
        do jj = 1,ncells_level(ll)
           do ii = 1,cell_nvert(ll,jj)
              write(funit3) vtkvertices(ii,ll,jj)
           enddo
        enddo
     enddo
     write(funit3) nbytes_cello
     vert_offset_count = 0
     do ll = 1,lmax
        do jj = 1,ncells_level(ll)
           vert_offset_count = vert_offset_count + cell_nvert(ll,jj)
           write(funit3) vert_offset_count
        enddo
     enddo
     write(funit3) nbytes_cellt
     do ll = 1,lmax
        do jj = 1,ncells_level(ll)
           write(funit3) cell_types(ll,jj)
        enddo
     enddo
     write(funit3) nbytes_rhop
     do jj = 1,npoints
        write(funit3) vtkPointData(1,jj)
     enddo
     write(funit3) nbytes_velp
     do jj = 1,npoints
        write(funit3) vtkPointData(2,jj)
     enddo
     write(funit3) nbytes_magp
     do jj = 1,npoints
        write(funit3) vtkPointData(3,jj)
     enddo
     write(funit3) nbytes_prep
     do jj = 1,npoints
        write(funit3) vtkPointData(4,jj)
     enddo
     write(funit3) '   </AppendedData>'//end_rec
     write(funit3) '</VTKFile>'//end_rec
     close(funit3)
  endif

  call deallocate_arrays
  
#ifdef MPI
  call mpi_barrier(world,ierror)
  call mpi_finalize(ierror)
#endif
  
  stop
  
contains

  !================================================================
  !================================================================
  !================================================================
  !================================================================
  subroutine read_params
    
    implicit none
    
    integer       :: i,n
    integer       :: iargc
    character(len=4)   :: opt
    character(len=128) :: arg
    LOGICAL       :: bad, ok
    
    n = iargc()
    if (n < 2) then
       print *, 'usage: amr2cell  -inp  input_dir'
       print *, '                 -out  output_file'
       print *, '                 [-xmi xmin] '
       print *, '                 [-xma xmax] '
       print *, '                 [-ymi ymin] '
       print *, '                 [-yma ymax] '
       print *, '                 [-zmi zmin] '
       print *, '                 [-zma zmax] '
       print *, '                 [-lma lmax] '
       print *, 'ex: amr2cell -inp output_00001 -out column.dat'// &
            &   ' -xmi 0.1 -xma 0.7 -lma 12'
       print *, ' '
       stop
    end if
    
    do i = 1,n,2
       call getarg(i,opt)
       if (i == n) then
          print '("option ",a2," has no argument")', opt
          stop 2
       end if
       call getarg(i+1,arg)
       select case (opt)
       case ('-inp')
          repository = trim(arg)
       case ('-out')
          outfich = trim(arg)
       case ('-xmi')
          read (arg,*) xmin
       case ('-xma')
          read (arg,*) xmax
       case ('-ymi')
          read (arg,*) ymin
       case ('-yma')
          read (arg,*) ymax
       case ('-zmi')
          read (arg,*) zmin
       case ('-zma')
          read (arg,*) zmax
       case ('-lma')
          read (arg,*) lmax
       case ('-nx')
          read (arg,*) nx_sample
       case ('-ny')
          read (arg,*) ny_sample
       case ('-nz')
          read (arg,*) nz_sample
       case default
          print '("unknown option ",a2," ignored")', opt
       end select
    end do
    
    return
    
  end subroutine read_params
  !================================================================
  !================================================================
  !================================================================
  !================================================================
  subroutine allocate_arrays
  
    implicit none
    
    npointsmax = nvtkmax * 10
    
    allocate(old_vertices(1:nverts_tot))
    
    allocate(cell_nvert  (1:nmaxlevels,1:nvtkmax))
    allocate(cell_types  (1:nmaxlevels,1:nvtkmax))
    allocate(ncells_level(1:nmaxlevels          ))
    
    allocate(split_face(1:6,1:nmaxlevels,1:nvtkmax))
    allocate(split_cell(    1:nmaxlevels,1:nvtkmax))
    
    allocate(vtkPointData(1:nvarmax,1:npointsmax))
    allocate(vtkPointDataCount     (1:npointsmax))
    
    allocate(is_center(1:npointsmax))
    allocate(is_corner(1:npointsmax))
    allocate(is_edge  (1:npointsmax))
    allocate(is_face  (1:npointsmax))
    
    allocate(vtkvertices(1:nverts_tot,1:nmaxlevels,1:nvtkmax))
    
    allocate(xyz(1:3,1:npointsmax))
    
    allocate(corner_shift(1:3,1:nverts_tot))
    
    return
  
  end subroutine allocate_arrays
  !================================================================
  !================================================================
  !================================================================
  !================================================================
  subroutine deallocate_arrays
  
    implicit none
    
    deallocate(old_vertices)
    
    deallocate(cell_nvert  )
    deallocate(cell_types  )
    deallocate(ncells_level)
    
    deallocate(split_face)
    deallocate(split_cell)
    
    deallocate(vtkPointData     )
    deallocate(vtkPointDataCount)
    
    deallocate(is_center)
    deallocate(is_corner)
    deallocate(is_edge  )
    deallocate(is_face  )
    
    deallocate(vtkvertices)
    
    deallocate(xyz)
    
    deallocate(corner_shift)
    
    return
  
  end subroutine deallocate_arrays
  
end program amr2vtk
!================================================================
!================================================================
!================================================================
!================================================================
subroutine title(n,nchar)

  implicit none
  integer::n
  character*5::nchar

  character*1::nchar1
  character*2::nchar2
  character*3::nchar3
  character*4::nchar4
  character*5::nchar5

  if(n.ge.10000)then
     write(nchar5,'(i5)') n
     nchar = nchar5
  elseif(n.ge.1000)then
     write(nchar4,'(i4)') n
     nchar = '0'//nchar4
  elseif(n.ge.100)then
     write(nchar3,'(i3)') n
     nchar = '00'//nchar3
  elseif(n.ge.10)then
     write(nchar2,'(i2)') n
     nchar = '000'//nchar2
  else
     write(nchar1,'(i1)') n
     nchar = '0000'//nchar1
  endif


end subroutine title
!================================================================
!================================================================
!================================================================
!================================================================
subroutine hilbert3d(x,y,z,order,bit_length,npoint)
  implicit none

  integer     ,INTENT(IN)                     ::bit_length,npoint
  integer     ,INTENT(IN) ,dimension(1:npoint)::x,y,z
  real(kind=8),INTENT(OUT),dimension(1:npoint)::order

  logical,dimension(0:3*bit_length-1)::i_bit_mask
  logical,dimension(0:1*bit_length-1)::x_bit_mask,y_bit_mask,z_bit_mask
  integer,dimension(0:7,0:1,0:11)::state_diagram
  integer::i,ip,cstate,nstate,b0,b1,b2,sdigit,hdigit

  if(bit_length>bit_size(bit_length))then
     write(*,*)'Maximum bit length=',bit_size(bit_length)
     write(*,*)'stop in hilbert3d'
     stop
  endif

  state_diagram = RESHAPE( (/   1, 2, 3, 2, 4, 5, 3, 5,&
                            &   0, 1, 3, 2, 7, 6, 4, 5,&
                            &   2, 6, 0, 7, 8, 8, 0, 7,&
                            &   0, 7, 1, 6, 3, 4, 2, 5,&
                            &   0, 9,10, 9, 1, 1,11,11,&
                            &   0, 3, 7, 4, 1, 2, 6, 5,&
                            &   6, 0, 6,11, 9, 0, 9, 8,&
                            &   2, 3, 1, 0, 5, 4, 6, 7,&
                            &  11,11, 0, 7, 5, 9, 0, 7,&
                            &   4, 3, 5, 2, 7, 0, 6, 1,&
                            &   4, 4, 8, 8, 0, 6,10, 6,&
                            &   6, 5, 1, 2, 7, 4, 0, 3,&
                            &   5, 7, 5, 3, 1, 1,11,11,&
                            &   4, 7, 3, 0, 5, 6, 2, 1,&
                            &   6, 1, 6,10, 9, 4, 9,10,&
                            &   6, 7, 5, 4, 1, 0, 2, 3,&
                            &  10, 3, 1, 1,10, 3, 5, 9,&
                            &   2, 5, 3, 4, 1, 6, 0, 7,&
                            &   4, 4, 8, 8, 2, 7, 2, 3,&
                            &   2, 1, 5, 6, 3, 0, 4, 7,&
                            &   7, 2,11, 2, 7, 5, 8, 5,&
                            &   4, 5, 7, 6, 3, 2, 0, 1,&
                            &  10, 3, 2, 6,10, 3, 4, 4,&
                            &   6, 1, 7, 0, 5, 2, 4, 3 /), &
                            & (/8 ,2, 12 /) )

  do ip=1,npoint

     ! convert to binary
     do i=0,bit_length-1
        x_bit_mask(i)=btest(x(ip),i)
        y_bit_mask(i)=btest(y(ip),i)
        z_bit_mask(i)=btest(z(ip),i)
     enddo

     ! interleave bits
     do i=0,bit_length-1
        i_bit_mask(3*i+2)=x_bit_mask(i)
        i_bit_mask(3*i+1)=y_bit_mask(i)
        i_bit_mask(3*i  )=z_bit_mask(i)
     end do

     ! build Hilbert ordering using state diagram
     cstate=0
     do i=bit_length-1,0,-1
        b2=0 ; if(i_bit_mask(3*i+2))b2=1
        b1=0 ; if(i_bit_mask(3*i+1))b1=1
        b0=0 ; if(i_bit_mask(3*i  ))b0=1
        sdigit=b2*4+b1*2+b0
        nstate=state_diagram(sdigit,0,cstate)
        hdigit=state_diagram(sdigit,1,cstate)
        i_bit_mask(3*i+2)=btest(hdigit,2)
        i_bit_mask(3*i+1)=btest(hdigit,1)
        i_bit_mask(3*i  )=btest(hdigit,0)
        cstate=nstate
     enddo

     ! save Hilbert key as double precision real
     order(ip)=0.
     do i=0,3*bit_length-1
        b0=0 ; if(i_bit_mask(i))b0=1
        order(ip)=order(ip)+dble(b0)*dble(2)**i
     end do

  end do

end subroutine hilbert3d
!================================================================
!================================================================
!================================================================
!================================================================
subroutine generate_vtk_filename(infile,icpu,vtkfile1,vtkfile2)

  implicit none
  
  character(len=128), intent(in ) :: infile
  integer           , intent(in ) :: icpu
  character(len=128), intent(out) :: vtkfile1,vtkfile2
  character(len=  5)              :: scpu
  
  vtkfile1 = trim(infile)//'/'//trim(infile)//'_'
  vtkfile2 = trim(infile)//'_'
  
  if( icpu <  10)                 write(scpu,'(a,i1)') '0000',icpu
  if((icpu >= 10).and.(icpu < 100)) write(scpu,'(a,i2)') '000',icpu
  if((icpu >= 100).and.(icpu < 1000)) write(scpu,'(a,i3)') '00',icpu
  if((icpu >= 1000).and.(icpu < 10000)) write(scpu,'(a,i4)') '0',icpu
  if((icpu >= 10000).and.(icpu < 100000)) write(scpu,'(i5)') icpu
  
  vtkfile1 = trim(vtkfile1)//scpu//'.vtu'
  vtkfile2 = trim(vtkfile2)//scpu//'.vtu'
  
  return

end subroutine generate_vtk_filename
