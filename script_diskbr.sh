#!/bin/bash

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

function processdata {

    file=$1;
    z="$(echo $file | rev |cut -d'_' -f1 | rev)";
    echo "0.5 0.5 0.5" > center.dat;
    echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat;
    if [ $z -ne 00001 ] ; then
        ${VISUDIR}/findcenter;
    fi
    ${VISUDIR}/angmom;
    echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > br.dat;
    ${VISUDIR}/br_disk;
    gunplot ${VISUDIR}/plothistodiskbr.gp;
    gnuplot ${VISUDIR}/plothistozrb.gp;
    mv br.ps movies/br_disk/'brdisk_'$z'.ps';
    for zrb in zrb*.ps ; do
        zrbb="$(echo $zrb | cut -d'.' -f1)";
        mv $zrb movies/br_disk/$zrbb'_'$z'.ps';
    done

};

###################################################################

last=false;
output_selected=false;
pattern_specified=false;
while getopts "eo:p:" OPTION; do
   case $OPTION in
      e)
         last=true;
      ;;
      o)
         output_selected=true;
         nout=$OPTARG;
      ;;
      p)
         pattern_specified=true;
         pattern=$OPTARG;
      ;;
   esac
done

mkdir -p movies;
mkdir -p movies/br_disk;
dir=.

if $last ; then

    for file in $dir/output*; do
        echo $file;
    done
    processdata $file;

elif $output_selected ; then

    if [ $nout -lt 10 ]; then
        file="${dir}/output_0000${nout}";
    elif [ $nout -lt 100 ]; then
        file="${dir}/output_000${nout}";
    elif [ $nout -lt 1000 ]; then
        file="${dir}/output_00${nout}";
    elif [ $nout -lt 10000 ]; then
        file="${dir}/output_0${nout}";
    else
        file="${dir}/output_${nout}";
    fi
    processdata $file;

elif $pattern_specified ; then

    for file in $dir/output_$pattern ; do
        echo "file is" $file;
        processdata $file;
    done

else

    for file in $dir/output_* ; do
        echo $file;
        processdata $file;
    done

fi
