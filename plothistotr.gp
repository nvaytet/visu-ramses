set view map
#set logscale x
set logscale y
set palette model RGB defined (0.25 "orange", 0.55 "red", 1 "violet")

load "< awk '/t/ {print $0}' tr.txt"
#set label front "time = %4.3f",t," Kyears" at -2,0.03
#set title "time = %4.3f Kyears",t
set title sprintf("Time = %4.3f Kyrs",t)
set ylabel 'T (K)'
set xlabel 'log(R) (a.u.)'
set xrange [-2:4]
#set yrange [0.4:500]
set format cb "%1.1f"
set palette maxcolors 100

#set term png enh size 1800,1500 19
set term post enh color 17
#set output 'rhor.png'
set output 'tr.ps'
splot 'tr.txt' index 1 u 2:(10**($1)):4 w p pt 7 ps 0.3 palette not
unset label
#set term wxt

