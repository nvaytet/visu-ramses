#############################################################################
# Makefile for visualisation software
#############################################################################
COMP = "GNU"

QUADHILBERT = 0

# GFORTRAN
ifeq ($(COMP),"GNU")
   F90 = gfortran
endif

# INTEL
ifeq ($(COMP),"INTEL")
   F90 = ifort
endif

LIST =	amrleg angmom brho coldens compabrho compbrhomap djdt findcenter \
	plandiskB plandisk tempdisk rhor tr trho ortho plandiskBdetails  \
	amr2vtk amr2vtk_delaunay br amr2cell findcenter_sink massdisk massoutflow \
	plandiskB_bt planorthoverif plandiskverif massstardisk

FLAGS = -cpp -Dr16p -DQUADHILBERT=$(QUADHILBERT)

#############################################################################
all:	$(LIST)
#############################################################################
bin2vtk:	IR_Precision.o Lib_VTK_IO.o bin2vtk.f90
		$(F90) IR_Precision.o Lib_VTK_IO.o bin2vtk.f90 -o bin2vtk
#############################################################################
amr2vtk_delaunay:	delaunay3d.f amr2vtk_delaunay.f90
		$(F90) delaunay3d.f amr2vtk_delaunay.f90 -o amr2vtk_delaunay
#############################################################################
%.o:	%.f90
	$(F90) $(FLAGS) -c $< -o $@
#############################################################################
%:	%.f90
	$(F90) $(FLAGS) $< -o $@
#############################################################################
clean :
	rm -f $(LIST) *.mod *.o
#############################################################################

