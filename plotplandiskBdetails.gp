reset
set view map
set palette model RGB defined (-1 "white" , -0.75 "yellow", -0.05 "red", 0 "black", 0.05 "blue", 0.75 "cyan", 1 "white")


load "< awk '/t/ {print $0}' br.txt"


load "< awk '/hmax/ {print $0}' plandisk.dat"



set xlabel 'R (a.u.)'
set ylabel 'h (a.u.)'

range=hmax*0.4
#range =350 pour fenetre 5 dans mu2 5

set yrange [-range:range]
set xrange [0:range]
set label front "time = %4.3f",t," Kyears" at screen 0.3,0.05 # 0.1


set term post enh color 13 landscape
set output 'plandiskB.ps'

set multiplot
set lmargin 0
set rmargin 0
set origin -0.1,0
set cblabel 'log(|B|_{/Symbol Q}/||B||)' rotate by 0 offset screen 0,0.15
set size ratio 2
set size 0.5,1
set cbrange [-1.5:1.5]
set format cb "{%1.1f}"
set colorbox horiz user origin  0.04,0.90 size 0.19,0.02
splot 'plandisk.dat' index 1 u ($2):($1):($4*$7) w image not

unset ylabel
set format y ""

set cblabel 'log(|B_r|/||B||)' rotate by 0 offset screen 0,0.15
set origin 0.132,0
set size 0.5,1
set colorbox horiz user origin  0.272,0.90 size 0.19,0.02
splot 'plandisk.dat' index 1 u 2:1:($5*$8) w image not

set cblabel 'log(|B_z|/||B||)' rotate by 0 offset screen 0,0.15
set origin 0.364,0
set size 0.5,1
set colorbox horiz user origin  0.504,0.90 size 0.19,0.02
splot 'plandisk.dat' index 1 u 2:1:($6*$9) w image not


#set palette model RGB defined (0.2 "white", 0.45 "cyan", 1 "blue", 1.1 "black")
set palette model RGB defined (-0.5 "red", -0.3 "orange", -0.05 "yellow", 0 "white", 0.05 "cyan", 0.3 "blue", 0.5 "violet",0.5000001 "white")
#set cblabel '||B|| with disk criteria (G)' rotate by 0 offset screen 0,0.15
set cblabel 'log({/Symbol b}) (P_ {therm}/P_B)' rotate by 0 offset screen 0,0.15
set origin 0.596,0
set size 0.5,1
set logscale cb
set cbrange [1.e-3:1.e3]
set format cb "{%L}"
set colorbox horiz user origin  0.736,0.90 size 0.19,0.02
splot 'plandisk.dat' index 1 u 2:1:($3) w image not
unset logscale cb

unset multiplot

unset output
