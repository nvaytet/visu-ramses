#!/bin/bash
i=23
j=26
y="$(printf "%05d" $i)"
z="$(printf "%05d" $j)"
mkdir movies
mkdir movies/brho_comp
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#for file in $dir/output*
file1=$dir'/output_'$y
file2=$dir'/output_'$z
#do
    echo -e 'inp' '"'$file1'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 10.8\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
    ./brhomap
    mv brho.txt brho1.txt

    echo -e 'inp' '"'$file2'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 10.8\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat
    ./brhomap
    mv brho.txt brho2.txt
    ./compa
    echo "load 'plotcompabrho.gp'" | gnuplot
    #echo "load 'plothistobrho.gp'" | gnuplot
    mv comp_brho.ps movies/brho_comp/'comp_brho_'$y'_'$z'.ps'
    #mv brho.png movies/brho/'brho_'$y'.png'
    #done
rm brho1.txt
rm brho2.txt
rm rescompabrho.txt
rm brho.dat





~                                     
