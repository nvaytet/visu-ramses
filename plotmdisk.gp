set logscale y
set xlabel 'Time (kyears)'
set ylabel 'Disk mass'
set term post enh color 17
set output 'mdisk.ps'
plot 'tdisk.txt' u 1:2 w l lw 3 tit 'Mass of disk'
