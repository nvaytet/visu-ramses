reset
set size ratio 2
set view map
#set logscale cb
#set logscale x
#set logscale y
set palette model RGB defined (0.2 "white", 0.45 "cyan", 1 "blue", 1.1 "black")
load "< awk '/t/ {print $0}' br.txt"


load "< awk '/hmax/ {print $0}' plandisk.dat"



set cblabel 'V_a (km.s^{-1})' rotate by 0 offset screen -0.06, screen -0.23
set xlabel 'R (a.u.)'
set ylabel 'h (a.u.)'

range=hmax*0.6
print hmax
#range =350 pour fenetre 5 dans mu2 5
#range =180
#range =360
#range=15000

set yrange [-range:range]
set xrange [0:range]
set palette maxcolors 100
#set label front "time = %4.3f",t," Kyears" at screen 0.58,0.11 # 0.1


#set term png enh size 1800,1500 19
set term post enh color 17 portrait
set output 'plandiskB_bt.ps'

set multiplot
set origin 0,0
set size 1,1
#set logscale cb
set cbrange [0:50]
#set cbrange [0:4]
#set format cb "{%L}"
set format cb "{%1.1f}"
set colorbox vert user origin 0.76,0.39 size 0.05,0.4
unset xtics
splot 'plandisk.dat' index 1 u 2:1:(($4/$3)/100.e3) w image not

#set origin 0,0
#set size 1,1
#set contour
#set cntrparam levels 15
#unset surface
#unset clabel
#splot 'plandisk.dat' index 1 u 2:1:($8) w l lt 1 lc 7 lw 2 not
#
#set surface
#unset contour

set cbtics out
set cbtics offset 0,screen 0.08
set cblabel "log(|B|_{/Symbol Q}/||B||)" offset screen 0.02,screen 0.05 # 0.07
#set cblabel "magnetic field (G)" offset 0,screen 0.05 # 0.07
set xlabel ''
set ylabel ''
set format x ""
set format y ""
set format cb "{%L}"
set cbrange [1.e-3:5.e-1]
set origin 0,0
set size 1,1
#set palette model RGB defined (0 "violet", 0.2 "blue", 0.6 "black")
set palette model RGB defined (0 "white", 0.3 "yellow", 0.5 "orange",  0.6 "red", 0.8 "purple")
#set palette model RGB defined (0 "white", 0.3 "yellow", 0.5 "orange",  0.6 "red")         utilise pour DA1
set palette model RGB defined (-0.2 "white", 0 "white", 0.1 "yellow", 0.25 "orange",  0.4 "red")
set colorbox horiz user origin 0.26,0.89 size 0.5,0.02 
set palette maxcolors 150
set logscale cb
set cbrange [9.e-2:1.e0]
set palette model RGB defined (-0.2 "white", 0 "white", 0.1 "yellow", 0.25 "orange",  0.4 "red", 0.4 "red", 0.5 "red")
set cbrange [4.e-2:1.e0]
set xtics ('0' 0, '50' 50, '100' 100, '150' 150) offset screen 0.0,0.01 
#set xtics ('0' 0, '10000' 10000, '25000' 25000, '40000' 40000) offset screen 0.0,0.01 
norm=range/21
splot 'plandisk.dat' every 9:9 index 1 u 2:1:($9/$3):($5/$7*norm):($6/$7*norm):(0) w vectors nohead lw 8 palette not
#
#set cbrange [*:*]
#
#set cbtics in
#set cbtics nooffset 
#set cblabel 'log(-J_z)' rotate by 0 offset screen -0.1,screen 0.07
#set origin 0.521,0.521
#set size 0.4,0.4
#set format x ""
#set format y ""
#set cbrange [1.e3:*]
#set palette model RGB defined (0.2 "white", 0.45 "blue", 1 "black")
#set colorbox vert user origin 0.79,0.66 size 0.015,0.15 
#
#splot 'djdt.dat' index 1 u 2:1:(-$4/$3) w image not

unset multiplot

