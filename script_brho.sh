#!/bin/bash

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

function processdata {

    file=$1;
    z="$(echo $file | rev |cut -d'_' -f1 | rev)";
    echo -e 'inp' '"'$file'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 1.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat;
    ${VISUDIR}/brho;
    gnuplot ${VISUDIR}/plothistobrho.gp;
    mv brho.ps movies/brho/'brho_'$z'.ps';
    if $2 ; then
       gs movies/brho/'brho_'$z'.ps';
    fi

};

##################################################################

last=false;
output_selected=false;
pattern_specified=false;
view_file=false;
while getopts "eo:p:x" OPTION; do
   case $OPTION in
      e)
         last=true;
      ;;
      o)
         output_selected=true;
         nout=$OPTARG;
      ;;
      p)
         pattern_specified=true;
         pattern=$OPTARG;
      ;;
      x)
         view_file=true;
      ;;
   esac
done

mkdir -p movies;
mkdir -p movies/brho;
dir=.

if $last ; then

    for file in $dir/output*; do
        echo $file;
    done
    processdata $file $view_file;

elif $output_selected ; then

    if [ $nout -lt 10 ]; then
        file="${dir}/output_0000${nout}";
    elif [ $nout -lt 100 ]; then
        file="${dir}/output_000${nout}";
    elif [ $nout -lt 1000 ]; then
        file="${dir}/output_00${nout}";
    elif [ $nout -lt 10000 ]; then
        file="${dir}/output_0${nout}";
    else
        file="${dir}/output_${nout}";
    fi
    processdata $file $view_file;

elif $pattern_specified ; then

    for file in $dir/output_$pattern ; do
        echo "file is" $file;
        processdata $file $view_file;
    done

else

    for file in $dir/output_* ; do
        echo $file;
        processdata $file $view_file;
    done

fi
