#!/bin/bash
i=1

VISUDIR=$(dirname "${BASH_SOURCE[0]}");

mkdir movies
dirmovie="movies/disk_verif"
mkdir $dirmovie
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dir=.
#for file in $dir/output_*0
   #for file in $dir/output_00065
   file=$dir/output_00700
#do
#      echo $file
#   done
   z="$(echo $file | rev |cut -d'_' -f1 | rev)"
   filee="$(echo $file | rev |cut -d'/' -f1 | rev)"
   if [ -f $dirmovie'/disk_'$z'.ps' ]
   then
      echo file $file already processed
   else
      echo processing file $file ....
      #   let i=$i+1
      #   y="$(printf "%03d" $i)"
      echo "0.5 0.5 0.5" > center.dat
      echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.65''\n''ymin 0.5''\n''ymax 0.65''\n''zmin 0.5''\n''zmax 0.65\nlmax 0\nfile vtk\ntype 18\nfenetre 3.\ncenter 0.5 0.5 0.5\nnorm 10000\nold 1' > brho.dat

      ${VISUDIR}/findcenter_sink
      ${VISUDIR}/angmom
 
      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.65''\n''ymin 0.5''\n''ymax 0.65''\n''zmin 0.5''\n''zmax 0.65\nlmax 0\nfile vtk\ntype 18\nfenetre 8.5\ncenter 0.5 0.5 0.5\nnorm 600\nold 1' > br.dat
      ${VISUDIR}/plandiskverif
      mv "plandisk_${z}.txt" plandisk_plan.dat

      echo -e 'inp' '"'$filee'"''\n''out cube.dat''\n''direction 4''\n''xmin 0.5''\n''xmax 0.55''\n''ymin 0.5''\n''ymax 0.55''\n''zmin 0.5''\n''zmax 0.55\nlmax 0\nfile vtk\ntype 18\nfenetre 7.\ncenter 0.5 0.5 0.5\nnorm 50\nold 1' > br.dat
      ${VISUDIR}/planorthoverif
      #   echo "load '/gpfs/data1/jmasson/analyse_folder/plothistodiskbr.gp'" | gnuplot
      mv "plandisk_${z}.txt" plandisk_ortho.dat
      gnuplot ${VISUDIR}/plotdisk_verif.gp
      mv plandisk.ps $dirmovie'/disk_'$z'.ps'
#      rm -rf plandisk.dat angmom.dat 'angmom_'$z'.txt' angmom.res 
   fi
#gs $dirmovie/'plandisk_'$z'.ps'
done
#rm brho.txt
#rm brho.dat

