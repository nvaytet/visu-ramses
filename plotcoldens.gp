#set pm3d
#set term wxt
unset label
set view map
set border 4095
set logscale cb
set palette model RGB defined ( 0 '#3366ff', 1 '#99ffcc', 2 '#339900', 3 '#66ff33', 4 '#996633', 5 '#ff9900', 6 '#ffff33' )
f(x) = x < 5./6 ? 2./3 - 4./5*x : 1 - (x-5./6)
f(x) = x <= 5./6 ? 0.6 - x*(0.6)/(5./6) :  1-(x-5./6)*(0.6)/(5./6) 
#f(x) = 0.85-0.85*x
g(x) = x < 2./10 ? 0.75-x*(0.75-( 5./9 - 6./9*2./10))/(2./10.): x < 5./6 ? 5./9 - 6./9*x : 1 - (x-5./6)
set palette model HSV functions f(gray),1,1
set palette maxcolors 100
set format cb "10^{%L}"
set autoscale fix
#set cbrange [*:1e-10]
set size 3,1
set xlabel 'u.a.' rotate by 0
load "< awk '/t/ {print $0}' cube.dat_1"
set label  "t= %4.3f",t," Kyears" at screen 0.65, screen 0.015
set term post enh color 17 portrait
set output 'rhov.ps'

set multiplot
set origin 0.,-0.2
set size 1,1

set tmargin 0
set size square
#set cbrange [5.e-1:1.e4]
#set colorbox vert user orig 0.86,0.3 size .03,.65
set cblabel "Column density in g/cm^2" rotate by -90 offset screen 0.02,0

splot 'cube.dat_1' index 1 u 2:3:4 w image not
unset colorbox
unset cblabel
#unset cbtics

set bmargin 0
set tmargin 1
set rmargin 0
set lmargin 0

set format x ""
#set format y ""
unset xlabel
unset ylabel

set origin 0.11,0.631
set origin 0.11,0.431
set size 0.5,0.5
set colorbox horiz user origin .18,1.02 size .3,.01
set colorbox horiz user origin .18,.82 size .3,.01
set cblabel "Column density (g/cm^2)" offset screen 0, screen 0.05
set cbtics offset screen 0, screen 0.05
splot 'cube.dat_2' index 1 u 3:2:4 w image not

set format y ""

set rmargin 0
set lmargin 0

set origin 0.445,0.631
set origin 0.445,0.431
set size 0.5,0.5
set colorbox horiz user origin .535,1.02 size .3,.01
set colorbox horiz user origin .535,.82 size .3,.01
set cblabel "Column density (g/cm^2)" offset screen 0, screen 0.05
set cbtics offset screen 0, screen 0.05
splot 'cube.dat_0' index 1 u 2:3:4 w image not

unset multiplot
